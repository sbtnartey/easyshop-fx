package entities;


import list.ListBrands;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class Brands {

    public String id;//1
    public String brandName;//2
    public String brandComment;//3
    public String supplierId;//4
    public String createdBy;//5
    public String createdDate;//6
    public String lastModifiedDate;//7
    public String lastModifiedBy;//8
    
    public String supplierName;
    public String creatorName;

    public ObservableList<ListBrands> brandDetails = FXCollections.observableArrayList();

}