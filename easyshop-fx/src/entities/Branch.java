/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import list.ListBranches;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ICSGH-BILLY
 */
public class Branch extends EntityCommon
{
    public String branchName;
    public String branchContactNumber;
    public String branchAddress;

    public ObservableList<ListBranches> branchDetails = FXCollections.observableArrayList();
}
