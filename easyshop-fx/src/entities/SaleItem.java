/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import list.ListSaleItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ICSGH-BILLY
 */
public class SaleItem
{

    public String id;
    public String quantity;
    public String sellPrice;
    public String amount;
    public String productId;
    public String saleId;
    public String createdBy;
    public String createdDate;
    public String lastModifiedDate;
    public String lastModifiedBy;

    public String productName;
    public String creatorName;
    public String salesReceipt;

    public ObservableList<ListSaleItem> saleItemDetails = FXCollections.observableArrayList();

}
