/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import list.ListStock;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ICSGH-BILLY
 */
public class Stock
{
    public String id;//1
    public String invoiceNumber;//2
    public String invoiceDate;//3
    public String invoiceTotalCost;//4
    public String totalItems;//5
    public String createdBy;//6
    public String createdDate;//7
    public String lastModifiedDate;//8
    public String lastModifiedBy;//9
    
    public ObservableList<ListStock> stockDetailsList = FXCollections.observableArrayList();
}
