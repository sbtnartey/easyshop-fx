package entities;

import list.ListPreSell;
import list.ListSold;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;


public class SellCart {
    
    public String Id;
    public String sellId;
    public String customerId;
    public String productId;
    public String unitPrice;
    public String sellPrice;
    public String quantity;
    public String totalPrice;
    public String purchaseDate;
    public String warrentyVoidDate;
    public String sellerID;
    public String sellDate;
    public String oldQuantity;
    public String totalItems;
    public String totalCost;
    public String deliveryStatus;
    
    public String customerName;
    public String sellerName;
    public String givenProductId;
    public String productName;
    
    
    
    public ObservableList<ListPreSell> carts = FXCollections.observableArrayList();
    public ObservableList<ListSold> soldList = FXCollections.observableArrayList();

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }
    
    

}
