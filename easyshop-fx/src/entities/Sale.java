/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import list.ListSale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ICSGH-BILLY
 */
public class Sale
{

    public String id;//1
    public String receiptNo;//2
    public String totalAmount;//3
    public String amountPaid;//4
    public String customerId;//5
    public String deliveryStatus;//6
    public String createdBy;//8
    public String createdDate;//9
    public String lastModifiedDate;//9
    public String lastModifiedBy;//10
    public String totalItems;

    public String sellerName;
    public String customerName;

    public ObservableList<ListSale> saleDetails = FXCollections.observableArrayList();

}
