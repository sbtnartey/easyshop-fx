package entities;


import list.ListCustomer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class Customer {

    public String id;//1
    public String customerName;//2
    public String customerConNo;//3
    public String customerAddress;//4
    public String totalBuy;//5
    public String createdDate;//6
    public String createdBy;//7
    public String lastModifiedBy;//8
    public String lastModifiedDate;//9
    
    public String userName;

    public ObservableList<ListCustomer> customerList = FXCollections.observableArrayList();

}