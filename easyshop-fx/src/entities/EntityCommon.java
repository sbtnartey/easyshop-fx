/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author ICSGH-BILLY
 */
public class EntityCommon
{
    public String id;
    public boolean updated;
    public boolean deleted;
    public Date createdDate;
    public Date lastModifiedDate;
    public String lastModifiedBy;
    public String createdBy;
}
