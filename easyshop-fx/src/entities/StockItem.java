/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import list.ListStockItems;

/**
 *
 * @author ICSGH-BILLY
 */
public class StockItem
{
    public String id;//1
    public String qty;//2
    public String sellingPrice;//3
    public String purchasePrice;//4
    public String productId;//5
    public String stockId;//6
    public String expirationDate;//7
    public String createdDate;//8
    public String createdBy;//9
    public String lastModifiedDate;//10
    public String lastModifiedBy;//11
    public String totalCost;
    
    
    public String creatorName;
    public String productName;
    public String invoiceNumber;
    
    public ObservableList<ListStockItems> stockItemsDetailsList = FXCollections.observableArrayList();
}
