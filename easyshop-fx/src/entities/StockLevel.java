/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import list.ListStockLevel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ICSGH-BILLY
 */
public class StockLevel
{
    public String id;
    public String quantityAvailable;
    public String sellingPrice;
    public String productId;
    public String purchasePrice;
    public String createdDate;
    public String createdBy;
    public String lastModifiedDate;
    public String lastModifiedBy;
    public String latestStock;
    public String lastAddQty;
    
    public String creatorName;
    public String productName;
    
    public ObservableList<ListStockLevel> stockLevelDetails = FXCollections.observableArrayList();
}
