/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import main.EasyShop;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ServerController implements Initializable {

    @FXML
    private TextField tfHost;
    @FXML
    private TextField thPort;
    @FXML
    private PasswordField pfPassword;
    @FXML
    private Button btnConnect;
    @FXML
    private Button btnReset;
    @FXML
    private Label lablServerStatus;
    @FXML
    private TextField tfDBName;
    @FXML
    private TextField tfUserName;

    Properties properties = new Properties();
    InputStream inputStream;
    OutputStream output = null;

    Connection con = null;

    String url;
    String user;
    String pass;
    String unicode = "?useUnicode=yes&characterEncoding=UTF-8";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        checkSQLStatus();
        getDataFromFile();
        
    }

    @FXML
    private void btnConnectOnAction(ActionEvent event) {
        mkDbProperties();

    }

    @FXML
    private void btnResetOnAction(ActionEvent event) {
    }

    public void getDataFromFile() {
        try {
            inputStream = new FileInputStream("database.properties");

            properties.load(inputStream);
            System.err.println("Host : " + properties.getProperty("host"));
            tfHost.setText(properties.getProperty("host"));
            tfDBName.setText(properties.getProperty("db"));
            tfUserName.setText(properties.getProperty("user"));
            pfPassword.setText(properties.getProperty("password"));
            thPort.setText(properties.getProperty("port"));
            inputStream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mkDbProperties() {
        try {

            if (isNotNull()) {
                output = new FileOutputStream("database.properties");

                properties.setProperty("host", tfHost.getText().trim());
                properties.setProperty("port", thPort.getText().trim());
                properties.setProperty("db", tfDBName.getText().trim());
                properties.setProperty("user", tfUserName.getText().trim());
                properties.setProperty("password", pfPassword.getText().trim());
                properties.store(output, null);
                output.close();
                if (dbConnect()) {
                    con.close();
                    
                    DBModel model = new DBModel();
                    model.createDataBase();
                    
                    
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Server connect successfully");
                    alert.setHeaderText("Login now");
                    alert.setContentText("Server has been connected sucessfully \n to login now click ok");
                    alert.initStyle(StageStyle.UNDECORATED);
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.isPresent() && result.get() == ButtonType.OK) {
                        Stage stage = (Stage) thPort.getScene().getWindow();
                        stage.close();
                    }
                } else {
                    Alert error_alert = new Alert(Alert.AlertType.ERROR);
                    error_alert.setTitle("Can't connect with mysql");
                    error_alert.setHeaderText("Can't connect to mysql server");
                    error_alert.setContentText("Mysql server parameters not correct. Try Again!");
                    error_alert.initStyle(StageStyle.UNDECORATED);
                    error_alert.show();
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EasyShop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EasyShop.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void checkSQLStatus() {
        try {
            inputStream = new FileInputStream("database.properties");
            String host = properties.getProperty("host");
            int port = 3306;
            Socket socket = new Socket(host, port);

            lablServerStatus.setText("Server is running");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ServerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean dbConnect() {
        DBConnection bConnection = new DBConnection();
        try {
            con = bConnection.getConnection();
            if (con != null) {
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Too Many Connection");
            ex.printStackTrace();
        }
        return false;
    }

    public boolean isNotNull() {
        boolean isNotNull;
        if (tfHost.getText().trim().isEmpty() || tfDBName.getText().trim().isEmpty()
                || tfUserName.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error");
            alert.setContentText("Please fill all required field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            isNotNull = false;

        } else {
            isNotNull = true;
        }
        return isNotNull;
    }
}
