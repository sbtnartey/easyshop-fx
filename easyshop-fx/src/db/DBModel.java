/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DBModel
{

    Properties properties = new Properties();
    InputStream inputStream;
    String db;

    public void loadPropertiesFile()
    {
        try
        {
            inputStream = new FileInputStream("database.properties");
            properties.load(inputStream);
            db = properties.getProperty("db");
        } catch (IOException e)
        {
            System.out.println("DDDD");
        }
    }

    PreparedStatement pst;

    public void createDataBase()
    {
        loadPropertiesFile();
        DBConnection con = new DBConnection();
        try
        {

            if (con.dbConnect())
            {
                pst = con.mkDataBase().prepareStatement("create database if not exists " + db + " DEFAULT CHARACTER SET utf8 \n"
                        + "  DEFAULT COLLATE utf8_general_ci");
                pst.execute();
                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`User` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `username` VARCHAR(20) NOT NULL,\n"
                        + "  `full_name` VARCHAR(100) ,\n"
                        + "  `email_address` VARCHAR(100) ,\n"
                        + "  `contact_number` VARCHAR(100) ,\n"
                        + "  `address` text,\n"
                        + "  `password` VARCHAR(45),\n"
                        + "  `status` tinyint(1) NOT NULL DEFAULT '0',\n"
                        + "  `user_image` mediumblob,\n"
                        + "  `date_joined` date DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`UserPermission` (\n"
                        + "  `id` int(11) NOT NULL AUTO_INCREMENT,\n"
                        + "  `add_product` tinyint(1) DEFAULT NULL,\n"
                        + "  `add_supplier` tinyint(1) DEFAULT NULL,\n"
                        + "  `add_brand` tinyint(1) DEFAULT NULL,\n"
                        + "  `add_category` tinyint(1) DEFAULT NULL,\n"
                        + "  `add_unit` tinyint(1) DEFAULT NULL,\n"
                        + "  `add_customer` tinyint(1) DEFAULT NULL,\n"
                        + "  `update_product` tinyint(1) DEFAULT NULL,\n"
                        + "  `update_customer` tinyint(1) DEFAULT NULL,\n"
                        + "  `update_supplier` tinyint(1) DEFAULT NULL,\n"
                        + "  `update_brand` tinyint(1) DEFAULT NULL,\n"
                        + "  `update_catagory` tinyint(1) DEFAULT NULL,\n"
                        + "  `update_unit` tinyint(1) DEFAULT NULL,\n"
                        + "  `rma_manage` tinyint(1) DEFAULT NULL,\n"
                        + "  `sell_product` tinyint(1) DEFAULT NULL,\n"
                        + "  `provide_discount` tinyint(1) DEFAULT NULL,\n"
                        + "  `employee_manage` tinyint(1) DEFAULT NULL,\n"
                        + "  `org_manage` tinyint(1) DEFAULT NULL,\n"
                        + "  `change_own_pass` tinyint(1) DEFAULT NULL,\n"
                        + "  `user_id` varchar(255) NOT NULL, \n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`Shop` (\n"
                        + "  `id` int(1) NOT NULL ,\n"
                        + "  `shop_name` varchar(100) DEFAULT NULL,\n"
                        + "  `shop_web_address` varchar(100) DEFAULT NULL,\n"
                        + "  `shop_contact_numbers` text DEFAULT NULL,\n"
                        + "  `shop_contact_address` text DEFAULT NULL,\n"
                        + "  `shop_logo` mediumblob,\n"
                        + "  `shop_email_address` varchar(100) DEFAULT NULL,\n"
                        + "  `vat` double DEFAULT 0,\n"
                        + "  `nhis` double DEFAULT 0,\n"
                        + "  `user_id` varchar(255) DEFAULT NULL,\n"
                        + "  `start_date` date DEFAULT NULL,\n"
                        + "  `end_date` date DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`Supplier` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `supplier_name` varchar(100) DEFAULT NULL,\n"
                        + "  `supplier_phone_number` text DEFAULT NULL,\n"
                        + "  `supplier_address` text DEFAULT NULL,\n"
                        + "  `supplier_description` text DEFAULT NULL,\n"
                        + "  `supplying_since` date DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`Brands` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `brand_name` varchar(70) DEFAULT NULL,\n"
                        + "  `description` text DEFAULT NULL,\n"
                        + "  `supplier_id` varchar(255)  DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");

                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`Category` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `category_name` varchar(70) NOT NULL,\n"
                        + "  `category_description` text DEFAULT NULL,\n"
                        + "  `created_id` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");

                pst.execute();
                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`Unit` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `unit_name` varchar(50) NOT NULL,\n"
                        + "  `unit_description` text DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");

                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE if not exists " + db + ".`RMA` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `rma_name` varchar(100) NOT NULL,\n"
                        + "  `rma_days` varchar(11) NOT NULL,\n"
                        + "  `comment` text DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");

                pst.execute();
                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`Products` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `product_name` varchar(150) NOT NULL,\n"
                        + "  `description` text ,\n"
                        + "  `supplier_id` varchar(255) NOT NULL,\n"
                        + "  `brand_id` varchar(255) NOT NULL,\n"
                        + "  `category_id` varchar(255) NOT NULL,\n"
                        + "  `unit_id` varchar(255) DEFAULT NULL,\n"
                        + "  `rma_id` varchar(255) DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  `product_image` mediumblob DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`Customer` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `customer_name` varchar(200) NOT NULL,\n"
                        + "  `customer_contact_no` varchar(200) DEFAULT NULL,\n"
                        + "  `customer_address` text,\n"
                        + "  `total_buy` double,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`Stock` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `invoice_number` varchar(100) NOT NULL,\n"
                        + "  `invoice_date` date,\n"
                        + "  `total_cost` double,\n"
                        + "  `total_items` int(10) DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`StockItem` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `item_qty` int(10) DEFAULT NULL,\n"
                        + "  `selling_price` double,\n"
                        + "  `purchase_price` double,\n"
                        + "  `product_id` varchar(100) NOT NULL,\n"
                        + "  `stock_id` varchar(100) NOT NULL,\n"
                        + "  `expiration_date` date,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  `total_cost` double DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`StockLevel` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `quantity_available` integer DEFAULT NULL,\n"
                        + "  `selling_price` double NOT NULL,\n"
                        + "  `product_id` varchar(255)DEFAULT NULL,\n"
                        + "  `purchase_price` double NOT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  `latest_stock` varchar(255) DEFAULT NULL,\n"
                        + "  `last_added_quantity` int(10) DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`Sale` (\n"
                        + "  `id` varchar(255) NOT NULL,\n"
                        + "  `receipt_no` varchar(50) NOT NULL,\n"
                        + "  `total_amount` double DEFAULT NULL,\n"
                        + "  `total_items` int(10) DEFAULT NULL,\n"
                        + "  `amount_paid` double DEFAULT NULL,\n"
                        + "  `customer_id` varchar(255) DEFAULT NULL,\n"
                        + "  `delivery_status` varchar(50) DEFAULT NULL,\n"
                        + "  `created_by` varchar(255) DEFAULT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  `returned_amount` double DEFAULT NULL,\n"
                        + "  `returned_qty` int(10) DEFAULT NULL,\n"
                        + "  `sale_returned` tinyint(1),\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS " + db + ".`SaleItem` (\n"
                        + "  `id` varchar(255) NOT NULL ,\n"
                        + "  `quantity` integer DEFAULT NULL,\n"
                        + "  `sell_price` double NOT NULL,\n"
                        + "  `amount` double NOT NULL,\n"
                        + "  `product_id` varchar(255) DEFAULT NULL,\n"
                        + "  `sale_id` varchar(255) NOT NULL,\n"
                        + "  `created_by` varchar(255) NOT NULL,\n"
                        + "  `created_date` timestamp,\n"
                        + "  `last_modified_date` timestamp,\n"
                        + "  `last_modified_by` varchar(255) DEFAULT NULL,\n"
                        + "  PRIMARY KEY (`id`),\n"
                        + "  UNIQUE INDEX `id` (`id` ASC));");
                pst.execute();

                pst = con.mkDataBase().prepareStatement("CREATE PROCEDURE upgrade_database_1_0_to_2_0()\n"
                        + " BEGIN\n"   
                        + " IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`total_cost` AND TABLE_NAME=`StockItem`) ) THEN\n"
                        + " ALTER TABLE `StockItem` ADD `total_cost` double DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`latest_stock` AND TABLE_NAME=`StockLevel`) ) THEN \n"
                        + " ALTER TABLE `StockLevel` ADD `latest_stock` varchar(255) DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`start_date` AND TABLE_NAME=`Shop`) ) THEN \n"
                        + " ALTER TABLE `Shop` ADD `start_date` date DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`end_date` AND TABLE_NAME=`Shop`) ) THEN \n"
                        + " ALTER TABLE `Shop` ADD `end_date` date DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`product_image` AND TABLE_NAME=`Products`) ) THEN \n"
                        + " ALTER TABLE `Products` ADD `product_image` mediumblob DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`returned_amount` AND TABLE_NAME=`Sale`) ) THEN \n"
                        + " ALTER TABLE `Sale` ADD `returned_amount` double DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`returned_qty` AND TABLE_NAME=`Sale`) ) THEN \n"
                        + " ALTER TABLE `Sale` ADD `returned_qty` int(10) DEFAULT NULL;\n"
                        + " END IF;\n"
                        + " IF NOT EXISTS( (SELECT * FROM  information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()\n"
                        + " AND COLUMN_NAME=`sale_returned` AND TABLE_NAME=`Sale`) ) THEN \n"
                        + " ALTER TABLE `Sale` ADD `sale_returned` tinyint(1);\n"
                        + " END IF;\n"
                        + " END;");
                pst.execute();


//            pst = con.mkDataBase().prepareStatement("CREATE TABLE IF NOT EXISTS "+db+".`Sell` (\n"
//                    + "  `Id` int(11) NOT NULL AUTO_INCREMENT,\n"
//                    + "  `SellId` varchar(10) NOT NULL,\n"
//                    + "  `CustomerId` varchar(11) NOT NULL,\n"
//                    + "  `ProductId` varchar(11) NOT NULL,\n"
//                    + "  `PursesPrice` double NOT NULL,\n"
//                    + "  `SellPrice` double NOT NULL,\n"
//                    + "  `Quantity` int(10) NOT NULL,\n"
//                    + "  `TotalPrice` double NOT NULL,\n"
//                    + "  `WarrentyVoidDate` varchar(20) NOT NULL,\n"
//                    + "  `SellerId` int(11) NOT NULL,\n"
//                    + "  `SellDate` timestamp NOT NULL,\n"
//                    + "  PRIMARY KEY (`Id`)\n"
//                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
//            pst.execute();
                System.out.println("Create Database Sucessfuly");
            } else
            {
                try
                {
                    Parent root = FXMLLoader.load(getClass().getResource("/view/Server.fxml"));
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    stage.setScene(scene);
                    stage.setTitle("Server Configure");
                    stage.showAndWait();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        } catch (SQLException ex)
        {
            System.err.println(ex);
            try
            {
                if (!con.dbExists() || con.getConnection() == null)
                {
                    Parent root = FXMLLoader.load(getClass().getResource("/view/Server.fxml"));
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    stage.setScene(scene);
                    stage.setTitle("Server Configure");
                    stage.showAndWait();
                }

            } catch (IOException ex1)
            {
                Logger.getLogger(DBModel.class.getName()).log(Level.SEVERE, null, ex1);
                try
                {
                    if (!con.dbExists() || con.getConnection() == null)
                    {
                        Parent root = FXMLLoader.load(getClass().getResource("/view/Server.fxml"));
                        Scene scene = new Scene(root);
                        Stage stage = new Stage();
                        stage.setScene(scene);
                        stage.setTitle("Server Configure");
                        stage.showAndWait();
                    }
                } catch (Exception e)
                {
                }

            }
        }
    }
}
