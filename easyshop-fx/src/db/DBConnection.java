/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    Properties properties = new Properties();
    InputStream inputStream;

    Connection con = null;

    String url;
    String user;
    String pass;
    String db;
    String unicode = "?useUnicode=yes&characterEncoding=UTF-8";
    static String driverName = "com.mysql.jdbc.Driver"; 

    public void loadPropertiesFile() {
        try {
            inputStream = new FileInputStream("database.properties");
            properties.load(inputStream);
            url = "jdbc:mysql://" + properties.getProperty("host") + ":" + properties.getProperty("port") + "/";
            user = properties.getProperty("user");
            pass = properties.getProperty("password");
            db = properties.getProperty("db");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("DDDD");
        }
    }

    public Connection mkDataBase() throws SQLException {
        return getConnection();
    }

    public Connection getConnection() {
        loadPropertiesFile();
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(url + unicode, user, pass);
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return con;
    }
    
    public boolean dbExists(){
        try {
            Connection connection = getConnection();
            try (ResultSet rs = connection.getMetaData().getCatalogs()) {
                while(rs.next()){
                    String dbName = rs.getString(1);
                    if(dbName.equalsIgnoreCase(db)){
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean dbConnect() {
        DBConnection bConnection = new DBConnection();
        try {
            con = bConnection.getConnection();
            if (con != null) {
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            System.out.println("Too Many Connection");
            ex.printStackTrace();
        }
        return false;
    }
}
