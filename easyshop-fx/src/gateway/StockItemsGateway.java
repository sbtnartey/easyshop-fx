/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import com.latlab.common.utils.DateTimeUtils;
import entities.StockLevel;
import entities.Supplier;
import list.ListStockLevel;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import details.StockUpdateModel;
import entities.StockItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;
import list.ListStockItems;

/**
 *
 * @author ICSGH-BILLY
 */
public class StockItemsGateway
{

    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public boolean save(StockItem stockItem)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("insert into " + db + ".StockItem(id, item_qty, selling_price, purchase_price, product_id, stock_id, expiration_date, created_by, created_date, last_modified_date) values(?,?,?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setInt(2, Integer.parseInt(stockItem.qty));
            pst.setDouble(3, Double.parseDouble(stockItem.sellingPrice));
            pst.setDouble(4, Double.parseDouble(stockItem.purchasePrice));
            pst.setString(5, stockItem.productId);
            pst.setString(6, stockItem.stockId);
            pst.setDate(7, sql.getDate(stockItem.expirationDate));
            pst.setString(8, stockItem.createdBy);
            pst.setTimestamp(9, sql.getCurrentTimeStamp());
            pst.setTimestamp(10, sql.getCurrentTimeStamp());
            pst.executeUpdate();
            con.close();
            pst.close();

            return true;

        } catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }

    }

    public void view(StockLevel stockLevel)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareCall("select * from " + db + ".StockItems");
            rs = pst.executeQuery();
            while (rs.next())
            {
                stockLevel.id = rs.getString(1);
                stockLevel.quantityAvailable = String.valueOf(rs.getInt(2));
                stockLevel.sellingPrice = String.valueOf(rs.getDouble(3));
                stockLevel.productId = rs.getString(4);
                stockLevel.purchasePrice = String.valueOf(rs.getDouble(5));
                stockLevel.createdBy = rs.getString(7);

                stockLevel.productName = sql.getName(stockLevel.productId, stockLevel.productName, "Product");
                stockLevel.creatorName = sql.getName(stockLevel.createdBy, stockLevel.creatorName, "User");
                stockLevel.stockLevelDetails.addAll(new ListStockLevel(stockLevel.id, stockLevel.productId, stockLevel.productName, stockLevel.quantityAvailable,
                        stockLevel.purchasePrice, stockLevel.sellingPrice));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selectedView(StockItem stockItem)
    {
        con = dbCon.getConnection();

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".StockItem where id=?");
            pst.setString(1, stockItem.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                stockItem.id = rs.getString(1);
                stockItem.qty = String.valueOf(rs.getInt(2));
                stockItem.sellingPrice = String.valueOf(rs.getDouble(3));
                stockItem.purchasePrice = String.valueOf(rs.getDouble(4));
                stockItem.productId = rs.getString(5);
                stockItem.stockId = rs.getString(6);
                stockItem.totalCost = String.valueOf(rs.getDouble(12));
                
                stockItem.productName = sql.getName(stockItem.productId, stockItem.productName, "Products");
                stockItem.invoiceNumber = sql.getField("Stock", "invoice_number", stockItem.stockId, "id");
                
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchView(StockItem stockItem)
    {
        con = dbCon.getConnection();

        stockItem.stockItemsDetailsList.clear();
        System.out.println("name :" + stockItem.productName);

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select s.id, s.quantity_available, s.selling_price, s.unit_price, s.product_id, p.id, p.product_name  from " + db + ".StockLevel AS s, Product As p "
                    + "where s.product_id = p.id AND  p.Product = ? ORDER BY p.product_name");
            pst.setString(1, "%" + stockItem.productName + "%");

            rs = pst.executeQuery();
            while (rs.next())
            {
                stockItem.id = rs.getString(1);
                stockItem.qty = String.valueOf(rs.getInt(2));
                stockItem.sellingPrice = String.valueOf(rs.getDouble(3));
                stockItem.purchasePrice = String.valueOf(rs.getDouble(4));
                stockItem.productId = rs.getString(5);
                stockItem.stockId = rs.getString(10);

                stockItem.productName = sql.getName(stockItem.productId, stockItem.productName, "Product");

//                stockItem.stockLevelDetails.addAll(new ListStockLevel(stockItem.id, stockItem.productId, stockItem.productName, stockItem.quantityAvailable, stockItem.purchasePrice, 
//                        stockItem.sellingPrice, stockItem.stockId));
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(StockItem stockItem)
    {
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareStatement("delete from " + db + ".StockItem where id=?");
            pst.setString(1, stockItem.id);
            pst.executeUpdate();
            
            StockUpdateModel stockUpdateModel = new StockUpdateModel();
            stockUpdateModel.productId = stockItem.productId;
            stockUpdateModel.isDelete = true;
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void update(StockItem stockItem)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("update " + db + ".StockItem set item_qty=? , selling_price=?, purchase_price=?, last_modified_date=? where id=?");
            pst.setInt(1, Integer.parseInt(stockItem.qty));
            pst.setDouble(2, Double.parseDouble(stockItem.sellingPrice));
            pst.setDouble(3, Double.parseDouble(stockItem.purchasePrice));
            pst.setTimestamp(4, sql.getCurrentTimeStamp());
            pst.setString(5, stockItem.id);
            pst.executeUpdate();
            con.close();
            pst.close();
            
            //update stocklevel
            StockUpdateModel stockUpdateModel = processStockLevelUpdate(stockItem);
            updateStockLevel(stockUpdateModel);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update");
            alert.setContentText("Stock Level Update successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    
    public StockUpdateModel processStockLevelUpdate(StockItem stockItem){
        con = dbCon.getConnection();
        StockUpdateModel stockUpdateModel = new StockUpdateModel();
        try {
            //get stockItem qty
            pst = con.prepareStatement("select * from " +db+ ".StockLevel where product_id=? and latest_stock=?");
            pst.setString(1, stockItem.productId);
            pst.setString(2, stockItem.stockId);
            stockUpdateModel.newQty = Integer.parseInt(stockItem.qty);
            rs = pst.executeQuery();
            while (rs.next())
            {
                stockUpdateModel.productId = stockItem.productId;
                stockUpdateModel.availableQty = rs.getInt(2);
                stockUpdateModel.lastAddedQty = rs.getInt(11);
                stockUpdateModel.newSellingPrice = Double.parseDouble(stockItem.sellingPrice);
                stockUpdateModel.newPurchasePrice = Double.parseDouble(stockItem.purchasePrice);
            }
            con.close();
            pst.close();
            rs.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stockUpdateModel;
    }
    
    public void updateStockLevel(StockUpdateModel stockUpdateModel){
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("update " +db+ ".StockLevel set quantity_available =?,selling_price=?,purchase_price=?,last_modified_date=? where product_id=?");
            pst.setInt(1, stockUpdateModel.availableQty);
            pst.setDouble(2, stockUpdateModel.newSellingPrice);
            pst.setDouble(3, stockUpdateModel.newPurchasePrice);
            pst.setTimestamp(4, sql.getCurrentTimeStamp());
            pst.setString(5, stockUpdateModel.productId);
            rs = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public void viewStockItems(StockItem stockItem)
    {
        con = dbCon.getConnection();

        stockItem.stockItemsDetailsList.clear();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".StockItem ORDER BY created_date DESC");
            rs = pst.executeQuery();
            while (rs.next())
            {

                stockItem.id = rs.getString(1);
                stockItem.qty = String.valueOf(rs.getInt(2));
                stockItem.sellingPrice = String.valueOf(rs.getDouble(3));
                stockItem.purchasePrice = String.valueOf(rs.getDouble(4));
                stockItem.productId = rs.getString(5);
                stockItem.stockId = rs.getString(6);
                stockItem.expirationDate = DateTimeUtils.formatDate(rs.getDate(7), DateTimeUtils.SIMPLE_PATTERN);
                stockItem.createdBy = rs.getString(8);
                stockItem.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(9), DateTimeUtils.SIMPLE_PATTERN);

                stockItem.productName = sql.getName(stockItem.productId, stockItem.productName, "Products");
                stockItem.creatorName = sql.getName(stockItem.createdBy, stockItem.creatorName, "User");
                stockItem.invoiceNumber = sql.getName(stockItem.stockId, stockItem.invoiceNumber, "Stock");
                stockItem.stockItemsDetailsList.addAll(new ListStockItems(stockItem.id, stockItem.qty, stockItem.productName, stockItem.invoiceNumber, 
                        stockItem.sellingPrice, stockItem.purchasePrice, stockItem.expirationDate, stockItem.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    
    public int productCurrentStockLevel(String productId) {
        int currentProductLevel = 0;
        con = dbCon.getConnection();
        
        try {
            pst = con.prepareStatement("select quantity_available from " + db + ".StockLevel where product_id = ?");
            pst.setString(1, productId);
            rs = pst.executeQuery();
            while(rs.next()) {
                currentProductLevel = rs.getInt(1);
            }
            pst.close();
            con.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentProductLevel;
    }
    
}
