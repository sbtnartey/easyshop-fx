/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import entities.Sale;
import entities.Supplier;
import list.ListSale;
import com.latlab.common.formating.ObjectValue;
import com.latlab.common.utils.DateTimeUtils;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import entities.SaleItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 *
 * @author ICSGH-BILLY
 */
public class SalesGateway
{
    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(Sale sale) {
        con = dbCon.getConnection();
        
        try {
            pst = con.prepareStatement("insert into "+db+".Sale(id, receipt_no,total_amount,amount_paid,created_by,created_date,customer_id,delivery_status) values(?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setString(2, sale.receiptNo);
            pst.setDouble(3, Double.parseDouble(sale.totalAmount));
            pst.setDouble(4, Double.parseDouble(sale.amountPaid));
            pst.setString(5, sale.createdBy);
            pst.setDate(6, new java.sql.Date(new Date().getTime()));
            pst.setString(7, sale.customerId);
            pst.setString(6, sale.deliveryStatus);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Sucess : save sucess");
            alert.setContentText("Sale" + "  '" + sale.receiptNo + "' " + "Added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void view(Sale sale) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareCall("select * from "+db+".Sale where DATE(created_date)= CURDATE()");
            rs = pst.executeQuery();
            while (rs.next()) {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = String.valueOf(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = String.valueOf(rs.getDouble(5));
                sale.customerId = rs.getString(5);
                sale.createdBy = rs.getString(7);
                sale.createdDate = DateTimeUtils.formatDate(rs.getDate(8), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");
                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdBy , sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selectedView(Sale sale) {
        con = dbCon.getConnection();

        try {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from "+db+".Sale where id=?");
            pst.setString(1, sale.id);
            rs = pst.executeQuery();
            while (rs.next()) {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = String.valueOf(rs.getDouble(3));
                sale.amountPaid = String.valueOf(rs.getDouble(4));
                sale.customerId = rs.getString(5);
                sale.createdBy = rs.getString(7);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");
               
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex) {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Sale findSale(String saleId){
        Sale sale = new Sale();
        try {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from "+db+".Sale where id=?");
            pst.setString(1, sale.id);
            rs = pst.executeQuery();
            while (rs.next()) {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = String.valueOf(rs.getDouble(3));
                sale.amountPaid = String.valueOf(rs.getDouble(4));
                sale.customerId = rs.getString(5);
                sale.createdBy = rs.getString(7);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");
               
            }
            con.close();
            pst.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sale;
    }

    public void searchView(Sale sale) {
        con = dbCon.getConnection();

        sale.saleDetails.clear();

        try {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from "+db+".Sale where receipt_no like ? ORDER DESC BY created_date");
            pst.setString(1, "%" + sale.receiptNo + "%");

            rs = pst.executeQuery();
            while (rs.next()) {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = String.valueOf(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = String.valueOf(rs.getDouble(5));
                sale.customerId = rs.getString(5);
                sale.createdBy = rs.getString(7);
                sale.createdDate = DateTimeUtils.formatDate(rs.getDate(8), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");
                  sale.saleDetails.addAll(new ListSale(sale.id, sale.createdBy , sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex) {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    public void delete(Sale sale) {
        con = dbCon.getConnection();

        try {
            pst = con.prepareStatement("delete from "+db+".Sale where id=?");
            pst.setString(1, sale.id);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Sale sale) {
        con = dbCon.getConnection();

        try {
            pst = con.prepareStatement("update "+db+".Sale set total_amount=? , amount_paid=? where id=?");
            pst.setDouble(1, ObjectValue.get_doubleValue(sale.totalAmount));
            pst.setDouble(2, ObjectValue.get_doubleValue(sale.amountPaid));
            pst.setTimestamp(9, sql.getCurrentTimeStamp());
            pst.setString(10, sale.lastModifiedBy);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : update sucess ");
            alert.setContentText("Update" + "  '" + sale.receiptNo + "' " + "Added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public int updateReturnedSale(SaleItem saleItem){
        int updated = 0;
        double itemAmount = Double.parseDouble(saleItem.amount);
        int itemQty = Integer.parseInt(saleItem.quantity);
        Sale sale = findSale(saleItem.saleId);
       
        int newSaleTotalItems = Integer.parseInt(sale.totalItems) - itemQty;
        double newSaleTotalAmount = Double.parseDouble(sale.totalAmount) - itemAmount;
        try {
            con = dbCon.getConnection();
            pst = con.prepareStatement("update " + db + ".Sale set returned_amount = ?, returned_qty = ?, sale_returned = ? where id = ?");
            pst.setDouble(1, newSaleTotalAmount);
            pst.setInt(2, newSaleTotalItems);
            pst.setBoolean(3, true);
            pst.setString(4, saleItem.saleId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return updated;
    }
    
//    public boolean isNotUsed(Sale sale){
//        con = dbCon.getConnection();
//        boolean inNotUse = false;
//        try {
//            pst = con.prepareStatement("select * from "+db+".Sale where BrandId=?");
//            pst.setString(1, brands.id);
//            rs = pst.executeQuery();
//            while(rs.next()){
//               Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setTitle("Error");
//            alert.setHeaderText("ERROE : Already exist ");
//            alert.setContentText("Brand" + "  '" + brands.brandName + "' " + "Already exist");
//            alert.initStyle(StageStyle.UNDECORATED);
//            alert.showAndWait();
//                return inNotUse;
//            }rs.close();
//            pst.close();
//            con.close();
//            inNotUse = true;
//        } catch (SQLException ex) {
//            Logger.getLogger(BrandsGetway.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return inNotUse;
//    }
}
