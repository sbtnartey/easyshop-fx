/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import entities.Stock;
import entities.Supplier;
import list.ListStock;
import com.latlab.common.formating.NumberFormattingUtils;
import com.latlab.common.formating.ObjectValue;
import com.latlab.common.utils.DateTimeUtils;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 *
 * @author ICSGH-BILLY
 */
public class StocksGateway
{

    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(Stock stock)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("insert into " + db + ".Stock values(?,?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setString(2, stock.invoiceNumber);
            pst.setDate(3, sql.getDate(stock.invoiceDate));
            pst.setDouble(4, ObjectValue.get_doubleValue(stock.invoiceTotalCost));
            pst.setInt(5, ObjectValue.get_intValue(stock.totalItems));
            pst.setString(6, stock.createdBy);
            pst.setDate(7, new java.sql.Date(new Date().getTime()));
            pst.setDate(8, new java.sql.Date(new Date().getTime()));
            pst.setString(9, stock.lastModifiedBy);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Sucess : save sucess");
            alert.setContentText("Stock" + "  '" + stock.invoiceNumber + "' " + "Added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public void view(Stock stock)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareCall("select * from " + db + ".Stock ORDER BY invoice_date DESC");
            rs = pst.executeQuery();
            while (rs.next())
            {
                stock.id = rs.getString(1);
                stock.invoiceNumber = rs.getString(2);
                stock.invoiceDate = DateTimeUtils.formatDate(rs.getDate(3), DateTimeUtils.SIMPLE_PATTERN);
                stock.invoiceTotalCost = String.valueOf(rs.getDouble(4));
                stock.totalItems = String.valueOf(rs.getInt(5));
                stock.createdBy = rs.getString(6);

                stock.createdBy = sql.getName(stock.createdBy, stock.createdBy, "User");
                stock.stockDetailsList.addAll(new ListStock(stock.id, stock.invoiceNumber, stock.invoiceTotalCost, stock.invoiceDate, stock.createdBy, stock.totalItems));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selectedView(Stock stock)
    {
        con = dbCon.getConnection();

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Stock where id=?");
            pst.setString(1, stock.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                stock.id = rs.getString(1);
                stock.invoiceNumber = rs.getString(2);
                stock.invoiceDate = DateTimeUtils.formatDate(rs.getDate(3), DateTimeUtils.SIMPLE_PATTERN);
                stock.invoiceTotalCost = NumberFormattingUtils.getFormatedAmount(rs.getDouble(4));
                stock.totalItems = String.valueOf(rs.getInt(5));
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchView(Stock stock)
    {
        con = dbCon.getConnection();

        stock.stockDetailsList.clear();
        System.out.println("invoice number :" + stock.invoiceNumber);
        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Stock where invoice_number like ? ORDER BY invoice_date DESC");
            pst.setString(1, "%" + stock.invoiceNumber + "%");

            rs = pst.executeQuery();
            while (rs.next())
            {
                stock.id = rs.getString(1);
                stock.invoiceNumber = rs.getString(2);
                stock.invoiceDate = DateTimeUtils.formatDate(rs.getDate(3), DateTimeUtils.SIMPLE_PATTERN);
                stock.invoiceTotalCost = rs.getString(4);
                stock.totalItems = String.valueOf(rs.getInt(5));
                stock.createdBy = rs.getString(5);

                stock.createdBy = sql.getName(stock.createdBy, stock.createdBy, "User");
                stock.stockDetailsList.addAll(new ListStock(stock.id, stock.invoiceNumber, stock.invoiceTotalCost, stock.invoiceDate, stock.createdBy, stock.totalItems));
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(Stock stock)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("delete from " + db + ".Stock where id=?");
            pst.setString(1, stock.id);
            pst.executeUpdate();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void update(Stock stock)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("update " + db + ".Stock set "
                    + "invoice_number=?,invoice_date=?,total_cost=?,total_items=?,last_modified_date=?,last_modified_by=? where id=?");
            pst.setString(1, stock.invoiceNumber);
            pst.setDate(2, sql.getDate(stock.invoiceDate));
            pst.setDouble(3, ObjectValue.get_doubleValue(stock.invoiceTotalCost));
            pst.setInt(4, ObjectValue.get_intValue(stock.totalItems));
            pst.setTimestamp(5, sql.getCurrentTimeStamp());
            pst.setString(6, stock.lastModifiedBy);
            pst.setString(7, stock.id);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : update sucess ");
            alert.setContentText("Invoice values Update successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean isHavingItem(Stock stock)
    {
        con = dbCon.getConnection();
        boolean isHavingItem = true;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".StockItem where stock_id=?");
            pst.setString(1, stock.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                return isHavingItem;
            }
            rs.close();
            pst.close();
            con.close();
            isHavingItem = false;
        } catch (SQLException ex)
        {
            Logger.getLogger(StocksGateway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isHavingItem;
    }
}
