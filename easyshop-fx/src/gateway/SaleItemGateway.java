/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import com.latlab.common.formating.NumberFormattingUtils;
import com.latlab.common.formating.ObjectValue;
import entities.SaleItem;
import list.ListSaleItem;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import entities.SellCart;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ICSGH-BILLY
 */
public class SaleItemGateway
{
    SQL sql = new SQL();
    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();
    StockItemsGateway stockItemsGateway = new StockItemsGateway();

    public void save(SaleItem saleItem) {
        con = dbCon.getConnection();
        
        try {
            double sellPrice = ObjectValue.get_doubleValue(saleItem.sellPrice);
            int qty = ObjectValue.get_intValue(saleItem.quantity);
            double total = sellPrice * qty;
            
            pst = con.prepareStatement("insert into "+db+".SaleItem values(?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setInt(2, qty);
            pst.setDouble(3, sellPrice);
            pst.setDouble(4, total);
            pst.setString(5, saleItem.productId);
            pst.setString(6, saleItem.saleId);
            pst.setString(7, saleItem.createdBy);
            pst.setTimestamp(8, sql.getCurrentTimeStamp());
            pst.executeUpdate();
            con.close();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
     public void save(SellCart sellCart) {
        con = dbCon.getConnection();
        
        try {
            pst = con.prepareStatement("insert into "+db+".SaleItem(id, quantity, sell_price, amount, product_id, "
                    + "sale_id, created_by, created_date, last_modified_date) values(?,?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setInt(2, ObjectValue.getIntegerValue(sellCart.quantity));
            pst.setDouble(3, ObjectValue.get_doubleValue(sellCart.sellPrice));
            pst.setDouble(4, ObjectValue.get_doubleValue(sellCart.totalPrice));
            pst.setString(5, sellCart.productId);
            pst.setString(6, sellCart.Id);
            pst.setString(7, sellCart.sellerID);
            pst.setTimestamp(8, sql.getCurrentTimeStamp());
            pst.setTimestamp(9, sql.getCurrentTimeStamp());
            pst.executeUpdate();
            con.close();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
     
     public void viewSaleDetail(SaleItem saleItem)
    {
        con = dbCon.getConnection();

        saleItem.saleItemDetails.clear();
        System.out.println("saleItem.saleId in saleitemgateway " + saleItem.saleId);
        try
        {
            pst = con.prepareStatement("select * from " + db + ".SaleItem where sale_id=?");
            pst.setString(1, saleItem.saleId);
            rs = pst.executeQuery();
            while (rs.next())
            {
                saleItem.id = rs.getString(1);
                saleItem.quantity = String.valueOf(rs.getInt(2));
                saleItem.sellPrice = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                saleItem.amount = NumberFormattingUtils.getFormatedAmount(rs.getDouble(4));
                saleItem.productId = rs.getString(5);

                saleItem.productName = sql.getName(saleItem.productId, saleItem.productName, "Products");
                
                saleItem.saleItemDetails.addAll(new ListSaleItem(saleItem.id, saleItem.productId, saleItem.productName, saleItem.quantity, saleItem.sellPrice,
                        saleItem.amount));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
     
     public int updateReturnedGoods(SaleItem  saleItem) {
        int update = 0;
        con = dbCon.getConnection();
        int productLevel = stockItemsGateway.productCurrentStockLevel(saleItem.productId);
        int returnedQty = Integer.parseInt(saleItem.quantity);
        
        int newQty = returnedQty + productLevel;
        
        try {
            pst = con.prepareStatement("update " + db + ".StockLevel set quantity_available = ? where product_id = ?");
            pst.setInt(1, newQty);
            pst.setString(2, saleItem.productId);
            update = pst.executeUpdate();
            
            pst.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         System.out.println("update " + update);
        return update;
    }
     
    public int returnSaleItem(SaleItem saleItem) {
        int update = 0;
        try {
            con = dbCon.getConnection();
            pst = con.prepareStatement("update " + db + ".SaleItem set item_returned = ? where product_id =? and sale_id = ?");
            pst.setBoolean(1, true);
            pst.setString(2, saleItem.productId);
            pst.setString(3, saleItem.saleId);
            update = pst.executeUpdate();
            pst.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return update;
    }
     
     
     
//
//    public void view(SaleItem saleItem) {
//        con = dbCon.getConnection();
//
//        try {
//            pst = con.prepareCall("select * from "+db+".SaleItem");
//            rs = pst.executeQuery();
//            while (rs.next()) {
//                saleItem.id = rs.getString(1);
//                saleItem.quantity = rs.getInt(2);
//                saleItem.sellPrice = rs.getDouble(3);
//                saleItem.amount = rs.getDouble(4);
//                saleItem.createdBy = rs.getString(5);
//               
//                saleItem.creatorName = sql.getName(saleItem.createdBy, saleItem.creatorName, "User");
//                saleItem.productName = sql.getName(saleItem.productId, saleItem.productName, "Product");
//                saleItem.saleItemDetails.addAll(new ListSaleItem(saleItem.id, saleItem.productId, saleItem.productName, saleItem.quantity, saleItem.sellPrice, saleItem.amount));
//            }
//            con.close();
//            pst.close();
//            rs.close();
//        } catch (SQLException ex) {
//            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
//
//    public void selectedView(Sale sale) {
//        con = dbCon.getConnection();
//
//        try {
//            con = dbCon.getConnection();
//            pst = con.prepareCall("select * from "+db+".Sale where id=?");
//            pst.setString(1, sale.id);
//            rs = pst.executeQuery();
//            while (rs.next()) {
//                sale.id = rs.getString(1);
//                sale.receiptNo = rs.getString(2);
//                sale.totalAmount = rs.getDouble(3);
//                sale.amountPaid = rs.getDouble(4);
//                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
//            }
//            con.close();
//            pst.close();
//            rs.close();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public void searchView(Sale sale) {
//        con = dbCon.getConnection();
//
//        sale.saleDetails.clear();
//
//        try {
//            con = dbCon.getConnection();
//            pst = con.prepareCall("select * from "+db+".Sale where ReceiptNo like ? ORDER DESC BY CreatedDate");
//            pst.setString(1, "%" + sale.receiptNo + "%");
//
//            rs = pst.executeQuery();
//            while (rs.next()) {
//                sale.id = rs.getString(1);
//                sale.receiptNo = rs.getString(2);
//                sale.totalAmount = rs.getDouble(3);
//                sale.amountPaid = rs.getDouble(4);
//                sale.createdBy = rs.getString(5);
//                sale.createdDate = rs.getDate(6);
//                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
//                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdDate, sale.receiptNo, sale.totalAmount, sale.amountPaid, sale.sellerName));
//            }
//            con.close();
//            pst.close();
//            rs.close();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public void delete(Sale sale) {
//        con = dbCon.getConnection();
//
//        try {
//            pst = con.prepareStatement("delete from "+db+".Sale where Id=?");
//            pst.setString(1, sale.id);
//            pst.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void update(SaleItem saleItem) {
//        con = dbCon.getConnection();
//
//        try {
//            pst = con.prepareStatement("update "+db+".SaleItem set Quantity=? , where Id=?");
//            pst.setDouble(1, sale.totalAmount);
//            pst.setDouble(2, sale.amountPaid);
//            pst.setDate(7, new java.sql.Date(saleItem.lastModifiedDate.getTime()));
//            pst.setString(8, saleItem.lastModifiedBy);
//            pst.executeUpdate();
//            con.close();
//            pst.close();
//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setTitle("Sucess");
//            alert.setHeaderText("Update : update sucess ");
//            alert.setContentText("Update" + "  '" + sale.receiptNo + "' " + "Added successfully");
//            alert.initStyle(StageStyle.UNDECORATED);
//            alert.showAndWait();
//            
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
}
