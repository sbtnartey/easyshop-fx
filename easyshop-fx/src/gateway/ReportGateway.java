/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import db.DBConnection;
import db.DBProperties;
import db.SQL;
import details.EodSales;
import details.EodSalesList;
import details.StockLevelDetail;
import details.StockLevelDetailList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author seth
 */
public class ReportGateway {

    SQL sql = new SQL();
    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void getEodSales(EodSales eodSales) {
        con = dbCon.getConnection();
        eodSales.eodSalesLists.clear();
        try {
            pst = con.prepareCall("SELECT si.sale_id,si.quantity,si.product_id,si.sell_price,si.amount,s.id,s.receipt_no,s.created_by,s.created_date FROM " + db + ".SaleItem AS si, " + db + ".Sale AS s "
                    + "WHERE si.sale_id=s.id AND DATE(s.created_date)= CURDATE() GROUP BY s.receipt_no ");
            rs = pst.executeQuery();
            while (rs.next()) {
               
                
                eodSales.quantity = rs.getInt(2);
                eodSales.productId = rs.getString(3);
                eodSales.productName = sql.getName(eodSales.productId, eodSales.productName, "Products");
                eodSales.sellPrice = rs.getDouble(4);
                eodSales.amount = rs.getDouble(5);
                eodSales.receiptNumber = rs.getString(7);
                eodSales.soledBy = sql.getField("User", "full_name", rs.getString(8), "id");
                
                eodSales.eodSalesLists.add(new EodSalesList(eodSales.receiptNumber, eodSales.soledBy, eodSales.productName, 
                        eodSales.quantity, eodSales.sellPrice, eodSales.amount));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void getStockLevel(StockLevelDetail stockLevelDetail){
        
        
        con = dbCon.getConnection();
        stockLevelDetail.stockLevelDetailLists.clear();
        try {
            pst = con.prepareStatement("select quantity_available, selling_price, product_id from " + db + ".StockLevel");
            rs = pst.executeQuery();
            while(rs.next()){
                stockLevelDetail.qtyAvailable = rs.getInt(1);
                stockLevelDetail.sellingPrice = rs.getDouble(2);
                stockLevelDetail.productId = rs.getString(3);
                
                stockLevelDetail.productName = sql.getName(stockLevelDetail.productId, stockLevelDetail.productName, "Products");
                
                stockLevelDetail.totalCost = stockLevelDetail.qtyAvailable * stockLevelDetail.sellingPrice;
                
                stockLevelDetail.stockLevelDetailLists.add(new StockLevelDetailList(stockLevelDetail.productName, stockLevelDetail.qtyAvailable, stockLevelDetail.sellingPrice, stockLevelDetail.totalCost));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
}
