/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import com.latlab.common.utils.DateTimeUtils;
import entities.Supplier;
import list.ListSupplier;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 * @author rifat
 */
public class SupplierGetway
{

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();
    SQL sql = new SQL();

    public void save(Supplier supplier)
    {
        con = dbCon.getConnection();
        if (isUniqSupplyerName(supplier))
        {
            try
            {
                con = dbCon.getConnection();
                pst = con.prepareCall("insert into " + db + ".Supplier(id,supplier_name,supplier_phone_number,supplier_address,supplier_description,supplying_since,created_by)"
                        + "values(?,?,?,?,?,?,?)");
                pst.setString(1, sql.generateId());
                pst.setString(2, supplier.supplierName);
                pst.setString(3, supplier.supplierContactNumber);
                pst.setString(4, supplier.supplierAddress);
                pst.setString(5, supplier.supplierDescription);
                pst.setDate(6, sql.getDate(supplier.supplyingSince));
                pst.setString(7, supplier.creatorId);
                pst.executeUpdate();
                con.close();
                pst.close();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sucess");
                alert.setHeaderText("Sucess : save sucess");
                alert.setContentText("Supplier" + "  '" + supplier.supplierName + "' " + "Added successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

            } catch (SQLException ex)
            {
                Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void view(Supplier supplier)
    {
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareCall("select * from " + db + ".Supplier ORDER BY supplier_name ASC");
            rs = pst.executeQuery();
            while (rs.next())
            {
                supplier.id = rs.getString(1);
                supplier.supplierName = rs.getString(2);
                supplier.supplierContactNumber = rs.getString(3);
                supplier.supplierAddress = rs.getString(4);
                //supplier.supplierDescription = rs.getString(5);
                supplier.supplyingSince = DateTimeUtils.formatDate(rs.getDate(6), DateTimeUtils.SIMPLE_PATTERN);
                supplier.creatorId = rs.getString(7);

                supplier.supplierDetails.addAll(new ListSupplier(supplier.id, supplier.supplierName, supplier.supplierContactNumber, supplier.supplierAddress, supplier.supplyingSince));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (SQLException ex)
        {
//            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Exception");
        }
    }

    public void searchView(Supplier supplier)
    {
        supplier.supplierDetails.clear();
        con = dbCon.getConnection();
        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Supplier where supplier_name like ? or supplier_phone_number like ? ORDER BY supplier_name");
            pst.setString(1, "%" + supplier.supplierName + "%");
            pst.setString(2, "%" + supplier.supplierContactNumber + "%");
            rs = pst.executeQuery();
            while (rs.next())
            {
                supplier.id = rs.getString(1);
                supplier.supplierName = rs.getString(2);
                supplier.supplierContactNumber = rs.getString(3);
                supplier.supplierAddress = rs.getString(4);
                supplier.supplyingSince = DateTimeUtils.formatDate(rs.getDate(6), DateTimeUtils.SIMPLE_PATTERN);

                supplier.supplierDetails.addAll(new ListSupplier(supplier.id, supplier.supplierName, supplier.supplierContactNumber, supplier.supplierAddress, supplier.supplyingSince));
            }
            rs.close();
            con.close();
            pst.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selectedView(Supplier supplier)
    {
        System.out.println("name :" + supplier.supplierName);
        con = dbCon.getConnection();
        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Supplier where id=?");
            pst.setString(1, supplier.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                supplier.id = rs.getString(1);
                supplier.supplierName = rs.getString(2);
                supplier.supplierContactNumber = rs.getString(3);
                supplier.supplierAddress = rs.getString(4);
                supplier.supplierDescription = rs.getString(5);
                supplier.supplyingSince = DateTimeUtils.formatDate(rs.getDate(6), DateTimeUtils.SIMPLE_PATTERN);
                supplier.creatorId = rs.getString(7);

            }
            rs.close();
            con.close();
            pst.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update(Supplier supplier)
    {
        System.out.println("we are in update");
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Supplier where id=? and supplier_name=?");
            pst.setString(1, supplier.id);
            pst.setString(2, supplier.supplierName);
            rs = pst.executeQuery();
            while (rs.next())
            {
                System.out.println("Into the loop");
                updateNow(supplier);
                rs.close();
                pst.close();
                con.close();
                return;
            }
            rs.close();
            con.close();
            pst.close();
            if (isUniqSupplyerName(supplier))
            {
                System.out.println("Out of the loop");
                updateNow(supplier);
                rs.close();
                con.close();
                pst.close();
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public void delete(Supplier supplier)
    {
        con = dbCon.getConnection();
        try
        {

            con = dbCon.getConnection();
            pst = con.prepareCall("SELECT * FROM " + db + ".Brands WHERE supplier_id=?");
            pst.setString(1, supplier.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Sucess");
                alert.setHeaderText("ERROR : Action Denied");
                alert.setContentText("This supplier provide some brands, please delete these brand first! Is that nessary to delete this supplyer ? \nif not you can update supplyer as much you can");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

                return;
            }
            rs.close();
            con.close();
            pst.close();
            deleteParmanently(supplier);
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean isUniqSupplyerName(Supplier supplyer)
    {
        con = dbCon.getConnection();
        boolean uniqSupplyer = false;
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareCall("select supplier_name from " + db + ".Supplier where supplier_name=?");
            pst.setString(1, supplyer.supplierName);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Sucess");
                alert.setHeaderText("ERROR : Action Denied");
                alert.setContentText("Supplier" + "  '" + supplyer.supplierName + "' " + "Already exist");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

                return uniqSupplyer;
            }
            rs.close();
            con.close();
            pst.close();
            uniqSupplyer = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uniqSupplyer;
    }

    public void updateNow(Supplier supplier)
    {
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareStatement("update " + db + ".Supplier set supplier_name=?, supplier_phone_number=?,supplier_address=? ,supplier_description=?, supplying_since=? where id=?");
            pst.setString(1, supplier.supplierName);
            pst.setString(2, supplier.supplierContactNumber);
            pst.setString(3, supplier.supplierAddress);
            pst.setString(4, supplier.supplierDescription);
            pst.setDate(5, sql.getDate(supplier.supplyingSince));
            pst.setString(6, supplier.id);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Updated : Updated sucess");
            alert.setContentText("Supplier" + "  '" + supplier.supplierName + "' " + "Updated successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void deleteParmanently(Supplier supplier)
    {
        con = dbCon.getConnection();
        try
        {
            System.out.println("and i am hear");
            con = dbCon.getConnection();
            pst = con.prepareCall("delete from " + db + ".Supplier where id=?");
            pst.setString(1, supplier.id);
            pst.executeUpdate();
            con.close();
            pst.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private boolean isUpdate(Supplier supplyer)
    {
        con = dbCon.getConnection();
        boolean isUpdate = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Supplier where id=?");
            pst.setString(1, supplyer.id);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isUpdate;
    }

    public boolean isNotUse(Supplier supplyer)
    {
        con = dbCon.getConnection();
        boolean isNotUse = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Brands where supplier_id=?");
            pst.setString(1, supplyer.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("WARNING");
                alert.setHeaderText("WARNING : ");
                alert.setContentText("This Supplier supplied  '" + rs.getString(2) + "' brand \n delete brand first");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                return isNotUse;
            }
            rs.close();
            pst.close();
            con.close();
            isNotUse = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(SupplierGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isNotUse;
    }
}
