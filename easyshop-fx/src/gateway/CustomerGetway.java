/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import entities.Customer;
import list.ListCustomer;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 *
 * @author rifat
 */
public class CustomerGetway {

    SQL sql = new SQL();
    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    
    
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(Customer customer) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("insert into "+db+".Customer(id,customer_name,customer_contact_no,customer_address,created_by,created_date) values(?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setString(2, customer.customerName);
            pst.setString(3, customer.customerConNo);
            pst.setString(4, customer.customerAddress);
            pst.setString(5, customer.createdBy);
            pst.setTimestamp(6, sql.getCurrentTimeStamp());
            pst.executeUpdate();
            pst.close();
            con.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Sucess : save sucess");
            alert.setContentText("Customer" + "  '" + customer.customerName + "' " + "Added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void view(Customer customer) {
        con = dbCon.getConnection();
        customer.customerList.clear();
        try {
            pst = con.prepareStatement("select * from "+db+".Customer");
            rs = pst.executeQuery();
            while (rs.next()) {
                customer.id = rs.getString(1);
                customer.customerName = rs.getString(2);
                customer.customerConNo = rs.getString(3);
                customer.customerAddress = rs.getString(4);
                customer.totalBuy = rs.getString(5);
                customer.createdBy = rs.getString(6);
                customer.createdDate = rs.getString(7);
                
                customer.userName = sql.getName(customer.createdBy, customer.userName, "User");
                customer.customerList.addAll(new ListCustomer(customer.id, customer.customerName, customer.customerConNo, customer.customerAddress, 
                        customer.totalBuy, customer.userName, customer.createdDate));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void selectedView(Customer customer) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("select * from "+db+".Customer where id=?");
            pst.setString(1, customer.id);
            rs = pst.executeQuery();
            while (rs.next()) {
                customer.customerName = rs.getString(2);
                customer.customerConNo = rs.getString(3);
                customer.customerAddress = rs.getString(4);
                customer.totalBuy = rs.getString(5);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void searchView(Customer customer) {
        con = dbCon.getConnection();
        customer.customerList.clear();
        try {
            pst = con.prepareStatement("select * from "+db+".Customer where customer_name like ? or customer_contact_no like ?");
            pst.setString(1, "%" + customer.customerName + "%");
            pst.setString(2, "%" + customer.customerConNo + "%");
            rs = pst.executeQuery();
            while (rs.next()) {
                customer.id = rs.getString(1);
                customer.customerName = rs.getString(2);
                customer.customerConNo = rs.getString(3);
                customer.customerAddress = rs.getString(4);
                customer.totalBuy = rs.getString(5);
                customer.createdBy = rs.getString(6);
                customer.createdDate = rs.getString(7);
                customer.userName = sql.getName(customer.createdBy, customer.userName, "User");
                customer.customerList.addAll(new ListCustomer(customer.id, customer.customerName, customer.customerConNo, customer.customerAddress, customer.totalBuy, customer.userName, customer.createdDate));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update(Customer customer) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("UPDATE "+db+".Customer set customer_name=?,customer_contact_no=?,customer_address=? where id=?");
            pst.setString(1, customer.customerName);
            pst.setString(2, customer.customerConNo);
            pst.setString(3, customer.customerAddress);
            pst.setString(4, customer.id);
            pst.executeUpdate();
            pst.close();
            con.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : Update sucess");
            alert.setContentText("Customer" + "  '" + customer.customerName + "' " + "update successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Customer customer) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("delete from "+db+".Customer where id=?");
            pst.setString(1, customer.id);
            pst.executeUpdate();
            pst.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isNotUsed(Customer customer) {
        con = dbCon.getConnection();
        boolean isNotUsed = false;
        try {
            pst = con.prepareStatement("select * from "+db+".Sale where customer_id=?");
            pst.setString(1, customer.id);
            rs = pst.executeQuery();
            while (rs.next()) {
               
                return isNotUsed;
            }
            rs.close();
            pst.close();
            con.close();
            isNotUsed = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isNotUsed;
    }

}
