package gateway;

import com.latlab.common.formating.NumberFormattingUtils;
import com.latlab.common.formating.ObjectValue;
import com.latlab.common.utils.DateTimeUtils;
import entities.SellCart;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import entities.Sale;
import entities.SaleItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import list.ListPreSell;
import list.ListSale;

public class SellCartGetway
{

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    SQL sql = new SQL();

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();
    StockItemsGateway stockItemsGateway = new StockItemsGateway();

    public String save(SellCart sellCart)
    {
        con = dbCon.getConnection();
        String sellCartId = sql.generateId();
        try
        {
            pst = con.prepareStatement("insert into " + db + ".Sale(id, receipt_no, total_amount, total_items, created_by, created_date, "
                    + "last_modified_date,customer_id, delivery_status) values(?,?,?,?,?,?,?,?,?)");
            pst.setString(1, sellCartId);
            pst.setString(2, sellCart.sellId);
            pst.setDouble(3, ObjectValue.get_doubleValue(sellCart.totalCost));
            pst.setInt(4, ObjectValue.get_intValue(sellCart.totalItems));
            pst.setString(5, sellCart.sellerID);
            pst.setTimestamp(6, sql.getCurrentTimeStamp());
            pst.setTimestamp(7, sql.getCurrentTimeStamp());
            pst.setString(8, sellCart.customerId);
            pst.setString(9, sellCart.deliveryStatus);

            pst.executeUpdate();
            pst.close();
            con.close();

            return sellCartId;

        } catch (SQLException ex)
        {
            Logger.getLogger(SellCartGetway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public boolean update(Sale sale)
    {
        try
        {
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean updateSalePayment(Sale sale)
    {
        con = dbCon.getConnection();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("update " + db + ".Sale set amount_paid=? where id=?");
            pst.setDouble(1, NumberFormattingUtils.ObjectToDouble(sale.amountPaid));
            pst.setString(2, sale.id);
            pst.executeUpdate();
            pst.close();
            con.close();
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public void view(Sale sale)
    {
        con = dbCon.getConnection();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Sale where customer_id IS NULL AND DATE(created_date)= CURDATE() ORDER BY created_date DESC ");
            rs = pst.executeQuery();
            while (rs.next())
            {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = NumberFormattingUtils.getFormatedAmount(rs.getDouble(5));
                sale.customerId = rs.getString(6);
                sale.createdBy = rs.getString(8);
                sale.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(9), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);

                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");

                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdDate, sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(SellCartGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  
    public void firstTenView(Sale sale)
    {
        con = dbCon.getConnection();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Sale limit 0,15");
            rs = pst.executeQuery();
            while (rs.next())
            {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = NumberFormattingUtils.getFormatedAmount(rs.getDouble(5));
                sale.customerId = rs.getString(5);
                sale.createdBy = rs.getString(8);
                sale.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(9), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");

                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdBy, sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(SellCartGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void viewDeliveries(Sale sale)
    {
        con = dbCon.getConnection();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Sale where customer_id IS NOT NULL OR customer_id <> '' AND DATE(created_date) = CURDATE() "
                    + " AND sale_returned = 0 ORDER BY created_date DESC");
            rs = pst.executeQuery();
            while (rs.next())
            {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = NumberFormattingUtils.getFormatedAmount(rs.getDouble(5));
                sale.customerId = rs.getString(6);
                sale.createdBy = rs.getString(8);
                sale.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(9), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");

                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdDate, sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(SellCartGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchView(Sale sale)
    {
        con = dbCon.getConnection();
        sale.saleDetails.clear();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Sale where id like ? or receipt_no like ? AND customer_id IS NULL AND sale_returnd = 0");
            pst.setString(1, "%" + sale.id + "%");
            pst.setString(2, "%" + sale.receiptNo + "%");
            rs = pst.executeQuery();
            while (rs.next())
            {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = NumberFormattingUtils.getFormatedAmount(rs.getDouble(5));
                sale.customerId = rs.getString(6);
                sale.createdBy = rs.getString(8);
                sale.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(9), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");

                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdDate, sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(SellCartGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void searchViewDelivery(Sale sale)
    {
        con = dbCon.getConnection();
        sale.saleDetails.clear();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Sale where id like ? or receipt_no like ? and customer_id IS NOT NULL");
            pst.setString(1, "%" + sale.id + "%");
            pst.setString(2, "%" + sale.receiptNo + "%");
            rs = pst.executeQuery();
            while (rs.next())
            {
                sale.id = rs.getString(1);
                sale.receiptNo = rs.getString(2);
                sale.totalAmount = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                sale.totalItems = String.valueOf(rs.getInt(4));
                sale.amountPaid = NumberFormattingUtils.getFormatedAmount(rs.getDouble(5));
                sale.customerId = rs.getString(6);
                sale.createdBy = rs.getString(8);
                sale.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(9), DateTimeUtils.SIMPLE_PATTERN + " " + DateTimeUtils.TIME);
                sale.sellerName = sql.getName(sale.createdBy, sale.sellerName, "User");
                sale.customerName = sql.getName(sale.customerId, sale.customerName, "Customer");

                sale.saleDetails.addAll(new ListSale(sale.id, sale.createdDate, sale.receiptNo, sale.totalItems, sale.totalAmount, sale.amountPaid, sale.sellerName, sale.customerName));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(SellCartGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchProductForCart(SellCart cart)
    {
        cart.carts.clear();
        con = dbCon.getConnection();
        SQL sql = new SQL();
        try
        {
            pst = con.prepareStatement("select sl.product_id,sl.quantity_available,sl.selling_price,p.product_name,p.id from " + db + ".StockLevel AS sl, " + db + ".Products AS p where p.product_name like ? "
                    + "AND sl.product_id = p.id");
            pst.setString(1, "%" + cart.productName + "%");
//            pst.setString(1, cart.productName);
            rs = pst.executeQuery();

            while (rs.next())
            {

                cart.productId = rs.getString(1);
                cart.oldQuantity = String.valueOf(rs.getInt(2));
                cart.sellPrice = NumberFormattingUtils.getFormatedAmount(rs.getDouble(3));
                cart.productName = sql.getName(cart.productId, cart.productName, "Products");
                
                cart.carts.addAll(new ListPreSell(cart.Id, cart.productId, cart.productName, cart.customerId, cart.sellPrice, cart.quantity, 
                        cart.oldQuantity, cart.totalPrice));
            }
            pst.close();
            con.close();
            rs.close();

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    
}
