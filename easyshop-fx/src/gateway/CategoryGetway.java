/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import entities.Category;
import entities.Supplier;
import list.ListCategory;
import db.DBConnection;
import db.DBProperties;
import db.SQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 * @author rifat
 */
public class CategoryGetway
{

    SQL sql = new SQL();
    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(Category catagory)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("insert into " + db + ".Category(id,category_name,category_description,created_id,created_date) values(?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setString(2, catagory.catagoryName);
            pst.setString(3, catagory.catagoryDescription);
            pst.setString(4, catagory.createdBy);
            pst.setTimestamp(5, sql.getCurrentTimeStamp());
            pst.executeUpdate();
            pst.close();
            con.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Sucess : save sucess");
            alert.setContentText("Category" + "  '" + catagory.catagoryName + "' " + "Added Sucessfuly");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public void view(Category catagory)
    {
        con = dbCon.getConnection();
        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Category ORDER BY category_name ASC");
            rs = pst.executeQuery();
            while (rs.next())
            {
                catagory.id = rs.getString(1);
                catagory.catagoryName = rs.getString(2);
                catagory.catagoryDescription = rs.getString(3);
                catagory.createdDate = rs.getString(4);
                catagory.createdBy = rs.getString(5);

                catagory.creatorName = sql.getName(catagory.createdBy, catagory.catagoryName, "User");
                catagory.catagoryDetails.addAll(new ListCategory(catagory.id, catagory.catagoryName, catagory.catagoryDescription, catagory.creatorName, catagory.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selectedView(Category catagory)
    {
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Category where id=?");
            pst.setString(1, catagory.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                catagory.id = rs.getString(1);
                catagory.catagoryName = rs.getString(2);
                catagory.catagoryDescription = rs.getString(3);

            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

//    public void brandView(Category catagory) {
//        con = dbCon.getConnection();
//
//        try {
//            pst = con.prepareStatement("select * from "+db+".Brands where SupplyerId=?");
//            pst.setString(1, catagory.supplierId);
//            rs = pst.executeQuery();
//            while (rs.next()) {
//                catagory.brandName = rs.getString(2);
//            }
//            pst.close();
//            con.close();
//            rs.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
    public void searchView(Category catagory)
    {
        con = dbCon.getConnection();
        catagory.catagoryDetails.clear();

        try
        {
            pst = con.prepareStatement("select * from " + db + ".Category where category_name like ? ORDER BY category_name");

            pst.setString(1, "%" + catagory.catagoryName + "%");
            rs = pst.executeQuery();
            while (rs.next())
            {
                catagory.id = rs.getString(1);
                catagory.catagoryName = rs.getString(2);
                catagory.catagoryDescription = rs.getString(3);
                catagory.createdDate = rs.getString(4);
                catagory.createdBy = rs.getString(5);

                catagory.creatorName = sql.getName(catagory.createdBy, catagory.catagoryName, "User");

                catagory.catagoryDetails.addAll(new ListCategory(catagory.id, catagory.catagoryName, catagory.catagoryDescription, catagory.creatorName, catagory.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void update(Category catagory)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("update " + db + ".Category set category_name=?,category_description=?,last_modified_date=?,last_modified_by=? where id=?");
            pst.setString(1, catagory.catagoryName);
            pst.setString(2, catagory.catagoryDescription);
            pst.setTimestamp(3, sql.getCurrentTimeStamp());
            pst.setString(4, catagory.lastModifiedBy);
            pst.setString(5, catagory.id);
            pst.executeUpdate();
            pst.close();
            con.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : update sucess");
            alert.setContentText("Category" + "  '" + catagory.catagoryName + "' " + "Update Sucessfuly");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public void delete(Category catagory)
    {
        con = dbCon.getConnection();
        try
        {

            pst = con.prepareStatement("delete from " + db + ".Category where id=?");
            pst.setString(1, catagory.id);
            pst.executeUpdate();
            pst.close();
            con.close();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean isNotUse(Category catagory)
    {
        con = dbCon.getConnection();
        boolean isNotUse = false;
        try
        {
            pst = con.prepareCall("select * from " + db + ".Products where category_id=?");
            pst.setString(1, catagory.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("ERROR");
                alert.setContentText("Sorry cannot" + "  '" + rs.getString(2) + "' " + "has products saved under");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                return isNotUse;
            }
            pst.close();
            rs.close();
            con.close();
            isNotUse = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(CategoryGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isNotUse;
    }

}
