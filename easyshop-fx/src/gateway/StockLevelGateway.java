/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import entities.Brands;
import entities.StockLevel;
import entities.Supplier;
import list.ListStockLevel;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 *
 * @author ICSGH-BILLY
 */
public class StockLevelGateway
{

    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(StockLevel stockLevel)
    {
        con = dbCon.getConnection();
        stockLevel.productId = sql.getIdNo(stockLevel.productName, stockLevel.productId, "Products", "product_name");

        try
        {
            pst = con.prepareStatement("insert into " + db + ".StockLevel(id, quantity_available, selling_price, product_id,purchase_price,created_by,created_date,last_modified_date) values(?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setInt(2, Integer.parseInt(stockLevel.quantityAvailable));
            pst.setDouble(3, Double.parseDouble(stockLevel.sellingPrice));
            pst.setString(4, stockLevel.productId);
            pst.setDouble(5, Double.parseDouble(stockLevel.purchasePrice));
            pst.setString(6, stockLevel.createdBy);
            pst.setTimestamp(7, sql.getCurrentTimeStamp());
            pst.setTimestamp(8, sql.getCurrentTimeStamp());

            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Sucess : save sucess");
            alert.setContentText("Product Level Updated successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public void view(StockLevel stockLevel)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareCall("select * from " + db + ".StockLevel");
            rs = pst.executeQuery();
            while (rs.next())
            {
                stockLevel.id = rs.getString(1);
                stockLevel.quantityAvailable = String.valueOf(rs.getInt(2));
                stockLevel.sellingPrice = String.valueOf(rs.getDouble(3));
                stockLevel.productId = rs.getString(4);
                stockLevel.purchasePrice = String.valueOf(rs.getDouble(5));
                stockLevel.createdBy = rs.getString(7);

                stockLevel.productName = sql.getName(stockLevel.productId, stockLevel.productName, "Products");
                stockLevel.creatorName = sql.getName(stockLevel.createdBy, stockLevel.creatorName, "User");
                stockLevel.stockLevelDetails.addAll(new ListStockLevel(stockLevel.id, stockLevel.productId, stockLevel.productName, stockLevel.quantityAvailable,
                        stockLevel.purchasePrice, stockLevel.sellingPrice));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selectedView(Brands brands)
    {
        con = dbCon.getConnection();

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Brands where id=?");
            pst.setString(1, brands.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                brands.id = rs.getString(1);
                brands.brandName = rs.getString(2);
                brands.brandComment = rs.getString(3);
                brands.supplierId = rs.getString(4);
                brands.supplierName = sql.getName(brands.supplierId, brands.supplierName, "Supplier");
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchView(StockLevel stockLevel)
    {
        con = dbCon.getConnection();

        stockLevel.stockLevelDetails.clear();
        System.out.println("name :" + stockLevel.productName);

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select s.id, s.quantity_available, s.selling_price, s.purchase_price, s.product_id, p.id, p.product_name  from " + db + ".StockLevel AS s, Product As p "
                    + "where s.product_id = p.id AND  p.Product = ? ORDER BY p.product_name");
            System.out.println("Brand name in Brand Object");
            pst.setString(1, "%" + stockLevel.productName + "%");

            rs = pst.executeQuery();
            while (rs.next())
            {
                stockLevel.id = rs.getString(1);
                stockLevel.quantityAvailable = String.valueOf(rs.getInt(2));
                stockLevel.sellingPrice = String.valueOf(rs.getDouble(3));
                stockLevel.purchasePrice = String.valueOf(rs.getDouble(4));
                stockLevel.productId = rs.getString(5);

                stockLevel.productName = sql.getName(stockLevel.productId, stockLevel.productName, "Products");

                stockLevel.stockLevelDetails.addAll(new ListStockLevel(stockLevel.id, stockLevel.productId, stockLevel.productName, stockLevel.quantityAvailable, stockLevel.purchasePrice,
                        stockLevel.sellingPrice));
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(StockLevel stockLevel)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("delete from " + db + ".StockLevel where id=?");
            pst.setString(1, stockLevel.id);
            pst.executeUpdate();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void update(StockLevel stockLevel)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("update " + db + ".StockLevel set quantity_available=?,selling_price=?,purchase_price=?,last_modified_date=?,last_modified_by=?,latest_stock=?, last_added_quantity=? where id=?");
            pst.setInt(1, Integer.parseInt(stockLevel.quantityAvailable));
            pst.setDouble(2, Double.parseDouble(stockLevel.sellingPrice));
            pst.setDouble(3, Double.parseDouble(stockLevel.purchasePrice));
            pst.setTimestamp(4, sql.getCurrentTimeStamp());
            pst.setString(5, stockLevel.lastModifiedBy);
            pst.setString(6, stockLevel.latestStock);
            pst.setInt(7, Integer.parseInt(stockLevel.lastAddQty));
            pst.setString(8, stockLevel.id);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : update sucess ");
            alert.setContentText("Stock Level Update successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean isNotUsed(StockLevel stockLevel)
    {
        con = dbCon.getConnection();
        boolean inNotUse = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".StockLevel where product_id=?");
            pst.setString(1, stockLevel.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("ERROE : Already exist ");
                alert.setContentText("Stock For Product Already exist");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                return inNotUse;
            }
            rs.close();
            pst.close();
            con.close();
            inNotUse = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(BrandsGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inNotUse;
    }

    public boolean isSoled(StockLevel stockLevel)
    {
        boolean isSoled = true;
        return isSoled;
    }

    public void viewFistTen(StockLevel stockLevel)
    {
        con = dbCon.getConnection();

        stockLevel.stockLevelDetails.clear();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".StockLevel");
            rs = pst.executeQuery();
            while (rs.next())
            {

                stockLevel.id = rs.getString(1);
                stockLevel.quantityAvailable = String.valueOf(rs.getInt(2));
                stockLevel.sellingPrice = String.valueOf(rs.getDouble(3));
                stockLevel.productId = rs.getString(4);
                stockLevel.purchasePrice = String.valueOf(rs.getDouble(5));

                stockLevel.productName = sql.getName(stockLevel.productId, stockLevel.productName, "Products");
                stockLevel.creatorName = sql.getName(stockLevel.createdBy, stockLevel.creatorName, "User");
                stockLevel.stockLevelDetails.addAll(new ListStockLevel(stockLevel.id, stockLevel.productId, stockLevel.productName, stockLevel.quantityAvailable, stockLevel.purchasePrice,
                        stockLevel.sellingPrice));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public StockLevel getStockLevel(StockLevel stockLevel)
    {
        con = dbCon.getConnection();
        StockLevel newStockLevel = new StockLevel();
        try
        {
            pst = con.prepareStatement("select quantity_available, purchase_price, selling_price from " + db + ".StockLevel where product_id=?");
            pst.setString(1, stockLevel.productId);
            rs = pst.executeQuery();
            while (rs.next())
            {
                newStockLevel.quantityAvailable = String.valueOf(rs.getInt(1));
                newStockLevel.purchasePrice = String.valueOf(rs.getDouble(2));
                newStockLevel.sellingPrice = String.valueOf(rs.getDouble(3));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return newStockLevel;
    }
}
