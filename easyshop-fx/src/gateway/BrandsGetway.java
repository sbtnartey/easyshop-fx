/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import entities.Brands;
import entities.Supplier;
import list.ListBrands;
import com.latlab.common.utils.DateTimeUtils;
import db.DBConnection;
import db.DBProperties;
import db.SQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 * @author rifat
 */
public class BrandsGetway
{

    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(Brands brands)
    {
        con = dbCon.getConnection();
        brands.supplierId = sql.getIdNo(brands.supplierName, brands.supplierId, "Supplier", "supplier_name");

        try
        {
            pst = con.prepareStatement("insert into " + db + ".Brands(id,brand_name,description,supplier_id,created_by,created_date) values(?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setString(2, brands.brandName);
            pst.setString(3, brands.brandComment);
            pst.setString(4, brands.supplierId);
            pst.setString(5, brands.createdBy);
            pst.setTimestamp(6, sql.getCurrentTimeStamp());
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Sucess : save sucess");
            alert.setContentText("Brand" + "  '" + brands.brandName + "' " + "Added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public void view(Brands brands)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareCall("select * from " + db + ".Brands");
            rs = pst.executeQuery();
            while (rs.next())
            {
                brands.id = rs.getString(1);
                brands.brandName = rs.getString(2);
                brands.brandComment = rs.getString(3);
                brands.supplierId = rs.getString(4);
                brands.createdBy = rs.getString(5);
                brands.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(6), DateTimeUtils.SIMPLE_PATTERN);

                brands.supplierName = sql.getName(brands.supplierId, brands.supplierName, "Supplier");
                brands.creatorName = sql.getName(brands.createdBy, brands.creatorName, "User");
                brands.brandDetails.addAll(new ListBrands(brands.id, brands.brandName, brands.brandComment, brands.supplierName, brands.creatorName, brands.createdDate));
            }
            con.close();
            pst.close();
            rs.close();
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selectedView(Brands brands)
    {
        con = dbCon.getConnection();

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Brands where id=?");
            pst.setString(1, brands.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                brands.id = rs.getString(1);
                brands.brandName = rs.getString(2);
                brands.brandComment = rs.getString(3);
                brands.supplierId = rs.getString(4);
                brands.supplierName = sql.getName(brands.supplierId, brands.supplierName, "Supplier");
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchView(Brands brands)
    {
        con = dbCon.getConnection();

        brands.brandDetails.clear();
        System.out.println("name :" + brands.brandName);

        try
        {
            con = dbCon.getConnection();
            pst = con.prepareCall("select * from " + db + ".Brands where brand_name like ? ORDER BY brand_name");
            System.out.println("Brand name in Brand Object");
            pst.setString(1, "%" + brands.brandName + "%");

            rs = pst.executeQuery();
            while (rs.next())
            {
                brands.id = rs.getString(1);
                brands.brandName = rs.getString(2);
                brands.brandComment = rs.getString(3);
                brands.supplierId = rs.getString(4);
                brands.createdBy = rs.getString(5);
                brands.createdDate = DateTimeUtils.formatDate(rs.getTimestamp(6), DateTimeUtils.SIMPLE_PATTERN);

                brands.supplierName = sql.getName(brands.supplierId, brands.supplierName, "Supplier");
                System.out.println("supplier name " + brands.supplierName);
                brands.creatorName = sql.getName(brands.createdBy, brands.creatorName, "User");
                brands.brandDetails.addAll(new ListBrands(brands.id, brands.brandName, brands.brandComment, brands.supplierName, brands.creatorName, brands.createdDate));
            }
            con.close();
            pst.close();
            rs.close();

        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(Brands brands)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("delete from " + db + ".Brands where id=?");
            pst.setString(1, brands.id);
            pst.executeUpdate();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void update(Brands brands)
    {
        con = dbCon.getConnection();

        try
        {
            pst = con.prepareStatement("update " + db + ".Brands set brand_name=?,description=?,supplier_id=?,last_modified_date=?,last_modified_by=? where id=?");
            pst.setString(1, brands.brandName);
            pst.setString(2, brands.brandComment);
            pst.setString(3, brands.supplierId);
            pst.setTimestamp(4, sql.getCurrentTimeStamp());
            pst.setString(5, brands.lastModifiedBy);
            pst.setString(6, brands.id);
            pst.executeUpdate();
            con.close();
            pst.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : update success ");
            alert.setContentText("Update" + "  '" + brands.brandName + "' " + "Added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean isNotUsed(Brands brands)
    {
        con = dbCon.getConnection();
        boolean inNotUse = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Products where brand_id=?");
            pst.setString(1, brands.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("ERROR");
                alert.setContentText("Sorry cannot delete " + "  '" + brands.brandName + "' " + "has products saved under");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                return inNotUse;
            }
            rs.close();
            pst.close();
            con.close();
            inNotUse = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(BrandsGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inNotUse;
    }

}
