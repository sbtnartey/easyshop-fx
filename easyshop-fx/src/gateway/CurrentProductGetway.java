/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gateway;

import controller.application.settings.OrgSettingController;
import entities.CurrentProduct;
import list.ListProduct;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 * @author rifat
 */
public class CurrentProductGetway {

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;
    
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    SQL sql = new SQL();

    public void save(CurrentProduct currentProduct) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("insert into "+db+".Products(id,product_name,description,supplier_id,brand_id,category_id,created_by,created_date,last_modified_date, product_image) "
                    + "values(?,?,?,?,?,?,?,?,?,?)");
            pst.setString(1, sql.generateId());
            pst.setString(2, currentProduct.productName);
            pst.setString(3, currentProduct.description);
            pst.setString(4, currentProduct.supplierId);
            pst.setString(5, currentProduct.brandId);
            pst.setString(6, currentProduct.catagoryId);
//            pst.setString(7, currentProduct.unitId);
//            pst.setString(8, currentProduct.rmaId);
            pst.setString(7, currentProduct.createdBy);
            pst.setDate(8, new java.sql.Date(new Date().getTime()));
            pst.setDate(9, new java.sql.Date(new Date().getTime()));
//            pst.setString(12, currentProduct.lastModifiedBy);
            if (currentProduct.imagePath != null) {
                try {
                    InputStream is = new FileInputStream(new File(currentProduct.imagePath));
                    pst.setBlob(10, is);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                pst.setBlob(10, (Blob) null);
            }
            pst.executeUpdate();
            pst.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Too Many Connection");
        }

    }

    public void view(CurrentProduct currentProduct) {
        currentProduct.currentProductList.clear();
        con = dbCon.getConnection();

        try {
            pst = con.prepareStatement("SELECT * FROM "+db+".Products ORDER BY product_name ASC");
            rs = pst.executeQuery();
            while (rs.next()) {

                currentProduct.id = rs.getString(1);
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName, currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void selectedView(CurrentProduct currentProduct) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("select * from "+db+".Products where id=?");
            pst.setString(1, currentProduct.id);
            rs = pst.executeQuery();
            while (rs.next()) {
                currentProduct.id = rs.getString(1);
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void viewFistTen(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        currentProduct.currentProductList.clear();
        try {
            pst = con.prepareStatement("select * from "+db+".Products ORDER BY product_name ASC");
            rs = pst.executeQuery();
            while (rs.next()) {

                currentProduct.id = rs.getString(1);
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName, currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void searchView(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        currentProduct.currentProductList.clear();
        try {
            pst = con.prepareStatement("select * from "+db+".Products where id like ? or product_name like ?");
            pst.setString(1, "%" + currentProduct.id + "%");
            pst.setString(2, "%" + currentProduct.productName + "%");
            rs = pst.executeQuery();
            while (rs.next()) {

                currentProduct.id = rs.getString(1);
               
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName,currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void searchBySupplier(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        currentProduct.currentProductList.clear();
        currentProduct.supplierId = sql.getIdNo(currentProduct.supplierName, currentProduct.supplierId, "Supplier", "supplier_name");
        try {
            pst = con.prepareStatement("select * from "+db+".Products where supplier_id=?");
            pst.setString(1, currentProduct.supplierId);
            rs = pst.executeQuery();
            while (rs.next()) {
               currentProduct.id = rs.getString(1);
                 currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName, currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchByBrand(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        currentProduct.currentProductList.clear();
        currentProduct.supplierId = sql.getIdNo(currentProduct.supplierName, currentProduct.supplierId, "Supplier", "supplier_name");
        currentProduct.brandId = sql.getBrandID(currentProduct.supplierId, currentProduct.brandId, currentProduct.brandName);
        System.out.println("Brand ID: " + currentProduct.brandId);

        try {
            pst = con.prepareStatement("select * from "+db+".Products where brand_id=?");
            pst.setString(1, currentProduct.brandId);
            rs = pst.executeQuery();
            while (rs.next()) {
               currentProduct.id = rs.getString(1);
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName, currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchByCatagory(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        currentProduct.currentProductList.clear();
        currentProduct.supplierId = sql.getIdNo(currentProduct.supplierName, currentProduct.supplierId, "Supplier", "supplier_name");
        currentProduct.brandId = sql.getBrandID(currentProduct.supplierId, currentProduct.brandId, currentProduct.brandName);
        currentProduct.catagoryId = sql.getCatagoryId(currentProduct.supplierId, currentProduct.brandId, currentProduct.catagoryId, currentProduct.catagoryName);
        try {
            pst = con.prepareStatement("select * from "+db+".Products where CategoryId=?");
            pst.setString(1, currentProduct.catagoryId);
            rs = pst.executeQuery();
            while (rs.next()) {
                currentProduct.id = rs.getString(1);
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName, currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchByRMA(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        currentProduct.currentProductList.clear();

        try {
            pst = con.prepareStatement("select * from "+db+".Products where rma_id=?");
            pst.setString(1, currentProduct.rmaId);
            rs = pst.executeQuery();
            while (rs.next()) {
                currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.currentProductList.addAll(new ListProduct(currentProduct.id, currentProduct.productName, currentProduct.description, currentProduct.supplierName, currentProduct.brandName, currentProduct.catagoryName, currentProduct.unitName, currentProduct.rmaName, currentProduct.userName, currentProduct.createdDate));
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sView(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        try {
            pst = con.prepareStatement("select * from "+db+".Products where id=?");
            pst.setString(1, currentProduct.id);
            rs = pst.executeQuery();
            while (rs.next()) {
               currentProduct.id = rs.getString(1);
                 currentProduct.productName = rs.getString(2);
                currentProduct.description = rs.getString(3);
                currentProduct.supplierId = rs.getString(4);
                currentProduct.brandId = rs.getString(5);
                currentProduct.catagoryId = rs.getString(6);
                currentProduct.unitId = rs.getString(7);
                currentProduct.rmaId = rs.getString(8);
                currentProduct.createdBy = rs.getString(9);
                currentProduct.createdDate = rs.getString(10);
                
                currentProduct.supplierName = sql.getName(currentProduct.supplierId, currentProduct.supplierName, "Supplier");
                currentProduct.brandName = sql.getName(currentProduct.brandId, currentProduct.brandName, "Brands");
                currentProduct.catagoryName = sql.getName(currentProduct.catagoryId, currentProduct.catagoryName, "Category");
                currentProduct.unitName = sql.getName(currentProduct.unitId, currentProduct.unitName, "Unit");
                currentProduct.rmaName = sql.getName(currentProduct.rmaId, currentProduct.rmaName, "RMA");
                currentProduct.userName = sql.getName(currentProduct.createdBy, currentProduct.userName, "User");
                currentProduct.rmaDayesss = sql.getDayes(currentProduct.rmaDayesss, currentProduct.rmaId);
                long dateRMA = Long.parseLong(currentProduct.rmaDayesss);

                currentProduct.warrentyVoidDate = LocalDate.now().plusDays(dateRMA).toString();

            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void cbSupplier(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        try {
            pst = con.prepareStatement("select * from "+db+".Supplier");
            rs = pst.executeQuery();
            while (rs.next()) {
                currentProduct.supplierList = rs.getString(2);
            }
            pst.close();
            con.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(CurrentProduct currentProduct) {
        con = dbCon.getConnection();

        try {
            pst = con.prepareStatement("update "+db+".Products set product_name=?,description=?, "
                    + "supplier_id=?,brand_id=?,category_id=?, product_image = ? where id=?");
            pst.setString(1, currentProduct.productName);
            pst.setString(2, currentProduct.description);
            pst.setString(3, currentProduct.supplierId);
            pst.setString(4, currentProduct.brandId);
            pst.setString(5, currentProduct.catagoryId);
            if (currentProduct.imagePath != null) {
                try {
                    InputStream is = new FileInputStream(new File(currentProduct.imagePath));
                    pst.setBlob(6, is);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                pst.setBlob(6, (Blob) null);
            }
            pst.setString(7, currentProduct.id);
            pst.executeUpdate();
            pst.close();
            con.close();
            rs.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("Update : update sucess");
            alert.setContentText("Product" + "  '" + currentProduct.productName + "' " + "Updated Sucessfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(CurrentProduct currentProduct) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("delete from "+db+".Products where id=?");
            pst.setString(1, currentProduct.id);
            pst.executeUpdate();
            pst.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isNotSoled(CurrentProduct currentProduct) {
        con = dbCon.getConnection();
        boolean isNotSoled = false;
        try {
            pst = con.prepareStatement("select * from "+db+".Sell where id=?");
            pst.setString(1, currentProduct.id);
            rs = pst.executeQuery();
            while (rs.next()) {
                return isNotSoled;
            }
            rs.close();
            pst.close();
            con.close();
            isNotSoled = true;
        } catch (SQLException ex) {
            Logger.getLogger(CurrentProductGetway.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isNotSoled;
    }

}
