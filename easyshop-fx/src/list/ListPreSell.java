/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

import javafx.scene.control.TextField;

public class ListPreSell
{
    String Id;
    String productId;
    String customerId;
    String unitPrice;
    String sellPrice;
    String quantity;
    String totalPrice;
    String purchaseDate;
    String warrentyVoidDate;
    String sellerId;
    String sellDate;
    String oldQuantity;
    
    String productName;

    public ListPreSell()
    {
    }
    
    public ListPreSell(String Id, String productId, String productName,String customerId, String unitPrice, String sellPrice, String quantity, String oldQuantity, String totalPrice)
    {
        this.Id = Id;
        this.productId = productId;
        this.customerId = customerId;
        this.unitPrice = unitPrice;
        this.sellPrice = sellPrice;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.productName = productName;
        this.oldQuantity = oldQuantity;
    }
    
    public ListPreSell(String Id, String productId, String productName,String customerId, String sellPrice, String quantity, String oldQuantity, String totalPrice)
    {
        this.Id = Id;
        this.productId = productId;
        this.customerId = customerId;
        this.unitPrice = unitPrice;
        this.sellPrice = sellPrice;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.productName = productName;
        this.oldQuantity = oldQuantity;
    }

    public String getId()
    {
        return Id;
    }

    public void setId(String Id)
    {
        this.Id = Id;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(String customerId)
    {
        this.customerId = customerId;
    }

    public String getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public String getSellPrice()
    {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice)
    {
        this.sellPrice = sellPrice;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getTotalPrice()
    {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice)
    {
        this.totalPrice = totalPrice;
    }

    public String getPurchaseDate()
    {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate)
    {
        this.purchaseDate = purchaseDate;
    }

    public String getWarrentyVoidDate()
    {
        return warrentyVoidDate;
    }

    public void setWarrentyVoidDate(String warrentyVoidDate)
    {
        this.warrentyVoidDate = warrentyVoidDate;
    }

    public String getSellerId()
    {
        return sellerId;
    }

    public void setSellerId(String sellerId)
    {
        this.sellerId = sellerId;
    }

    public String getSellDate()
    {
        return sellDate;
    }

    public void setSellDate(String sellDate)
    {
        this.sellDate = sellDate;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getOldQuantity()
    {
        return oldQuantity;
    }

    public void setOldQuantity(String oldQuantity)
    {
        this.oldQuantity = oldQuantity;
    }
    
}
