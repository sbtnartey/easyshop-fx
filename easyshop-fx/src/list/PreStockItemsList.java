/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author ICSGH-BILLY
 */
public class PreStockItemsList
{
    public String itemQty;
    public String productId;
    public String stockId;
    public String sellingPrice;
    public String purchasePrice;
    public String expirationDate;
    
    public String invoiceNumber;
    public String productName;

    public PreStockItemsList(String itemQty, String productId, String stockId, String invoiceNumber, String productName, String sellingPrice, String purchasePrice, String expirationDate)
    {
        this.itemQty = itemQty;
        this.productId = productId;
        this.stockId = stockId;
        this.sellingPrice = sellingPrice;
        this.purchasePrice = purchasePrice;
        this.expirationDate = expirationDate;
        this.productName = productName;
    }

    public String getItemQty()
    {
        return itemQty;
    }

    public void setItemQty(String itemQty)
    {
        this.itemQty = itemQty;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getStockId()
    {
        return stockId;
    }

    public void setStockId(String stockId)
    {
        this.stockId = stockId;
    }

    public String getSellingPrice()
    {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice)
    {
        this.sellingPrice = sellingPrice;
    }

    public String getPurchasePrice()
    {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice)
    {
        this.purchasePrice = purchasePrice;
    }

    public String getExpirationDate()
    {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate)
    {
        this.expirationDate = expirationDate;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }
    
}
