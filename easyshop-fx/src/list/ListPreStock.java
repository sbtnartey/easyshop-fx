/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author ICSGH-BILLY
 */
public class ListPreStock
{
    public String productId;
    public String productName;
    public String quantityAvailable;
    public String sellingPrice;
    public String unitPrice;

    public ListPreStock(String productId, String productName, String quantityAvailable, String sellingPrice, String unitPrice)
    {
        this.quantityAvailable = quantityAvailable;
        this.sellingPrice = sellingPrice;
        this.productId = productId;
        this.unitPrice = unitPrice;
        this.productName = productName;
    }

    public String getQuantityAvailable()
    {
        return quantityAvailable;
    }

    public void setQuantityAvailable(String quantityAvailable)
    {
        this.quantityAvailable = quantityAvailable;
    }

    public String getSellingPrice()
    {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice)
    {
        this.sellingPrice = sellingPrice;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }
    
    
    
    
    
}
