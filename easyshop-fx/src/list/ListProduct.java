package list;

public class ListProduct {
    public String id;
    public String productName;
    public String description;
    public String supplierName;
    public String brand;
    public String catagory;
    public String unit;
    public String pursesPrice;
    public String sellPrice;
    public String discountInCash;
    public String discountInPersent;
    public String rma;
    public String user;
    public String date;

    public ListProduct(String id, String productName, String description, String supplierName, String brand, String catagory, String unit, String rma, String user, String date) {
        this.id = id;
        this.productName = productName;
        this.description = description;
        this.supplierName = supplierName;
        this.brand = brand;
        this.catagory = catagory;
        this.unit = unit;
        this.rma = rma;
        this.user = user;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCatagory() {
        return catagory;
    }

    public void setCatagory(String catagory) {
        this.catagory = catagory;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    public String getDiscountInCash() {
        return discountInCash;
    }

    public void setDiscountInCash(String discountInCash) {
        this.discountInCash = discountInCash;
    }

    public String getDiscountInPersent() {
        return discountInPersent;
    }

    public void setDiscountInPersent(String discountInPersent) {
        this.discountInPersent = discountInPersent;
    }

    public String getRma() {
        return rma;
    }

    public void setRma(String rma) {
        this.rma = rma;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
