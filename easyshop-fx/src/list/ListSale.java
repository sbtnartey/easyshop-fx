/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

public class ListSale
{
    public String id;
    public String createdDate;
    public String receiptNo;
    public String totalItems;
    public String totalAmount;
    public String amountPaid;
    public String sellerName;
    public String customerName;

    public ListSale(String id, String createdDate, String receiptNo, String totalItems, String totalAmount, String amountPaid, String sellerName, String customerName)
    {
        this.id = id;
        this.createdDate = createdDate;
        this.receiptNo = receiptNo;
        this.totalAmount = totalAmount;
        this.amountPaid = amountPaid;
        this.sellerName = sellerName;
        this.totalItems = totalItems;
        this.customerName = customerName;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getReceiptNo()
    {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo)
    {
        this.receiptNo = receiptNo;
    }

    public String getTotalAmount()
    {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    public String getAmountPaid()
    {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid)
    {
        this.amountPaid = amountPaid;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getSellerName()
    {
        return sellerName;
    }

    public void setSellerName(String sellerName)
    {
        this.sellerName = sellerName;
    }

    public String getTotalItems()
    {
        return totalItems;
    }

    public void setTotalItems(String totalItems)
    {
        this.totalItems = totalItems;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }
    
}
