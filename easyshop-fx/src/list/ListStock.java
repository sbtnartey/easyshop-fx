/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author ICSGH-BILLY
 */
public class ListStock
{

    public String id;//1
    public String invoiceNumber;//2
    public String invoiceDate;//3
    public String invoiceTotalCost;//4
    public String createdBy;//5
    public String totalItems;
    
    
    public ListStock(String id, String invoiceNumber, String invoiceTotalCost, String invoiceDate, String createdBy, String totalItems)
    {
        this.id = id;
        this.invoiceNumber = invoiceNumber;
        this.invoiceTotalCost = invoiceTotalCost;
        this.invoiceDate = invoiceDate;
        this.createdBy = createdBy;
        this.totalItems = totalItems;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getInvoiceNumber()
    {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber)
    {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate()
    {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate)
    {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceTotalCost()
    {
        return invoiceTotalCost;
    }

    public void setInvoiceTotalCost(String invoiceTotalCost)
    {
        this.invoiceTotalCost = invoiceTotalCost;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getTotalItems()
    {
        return totalItems;
    }

    public void setTotalItems(String totalItems)
    {
        this.totalItems = totalItems;
    }
    
}
