/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

import entities.SaleItem;

/**
 *
 * @author ICSGH-BILLY
 */
public class ListSaleItem
{
    public String id;
    public String productId;
    public String productName;
    public String quantity;
    public String sellPrice;
    public String amount;

    public ListSaleItem(String id, String productId, String productName, String quantity, String sellPrice, String amount)
    {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.sellPrice = sellPrice;
        this.amount = amount;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getSellPrice()
    {
        return sellPrice;
    }

    public void setSellPrice(String sellPrice)
    {
        this.sellPrice = sellPrice;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    
}
