/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author ICSGH-BILLY
 */
public class ListStockItems
{
    public String id;
    public String itemQty;
    public String productId;
    public String stockId;
    public String sellingPrice;
    public String purchasePrice;
    public String expirationDate;
    
    public String invoiceNumber;
    public String productName;
    public String totalCost;
    public String createdDate;

    public ListStockItems(String id, String itemQty, String productName, String invoiceNumber, String sellingPrice, String purchasePrice, String expirationDate, String createdDate)
    {
        this.id = id;
        this.itemQty = itemQty;
        this.productName = productName;
        this.invoiceNumber = invoiceNumber;
        this.sellingPrice = sellingPrice;
        this.purchasePrice = purchasePrice;
        this.expirationDate = expirationDate;
        this.createdDate = createdDate;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    
    public String getItemQty()
    {
        return itemQty;
    }

    public void setItemQty(String itemQty)
    {
        this.itemQty = itemQty;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getStockId()
    {
        return stockId;
    }

    public void setStockId(String stockId)
    {
        this.stockId = stockId;
    }

    public String getSellingPrice()
    {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice)
    {
        this.sellingPrice = sellingPrice;
    }

    public String getPurchasePrice()
    {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice)
    {
        this.purchasePrice = purchasePrice;
    }

    public String getExpirationDate()
    {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate)
    {
        this.expirationDate = expirationDate;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    
    
}
