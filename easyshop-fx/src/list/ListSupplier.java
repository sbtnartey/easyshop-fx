/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author rifat
 */
public class ListSupplier
{

    public String supplierId;
    public String supplierName;
    public String supplierPhoneNumber;
    public String supplierAddress;
    public String supplierDescription;
    public String creatorId;
    public String supplyingSince;

    public ListSupplier(String supplyerId, String supplierName)
    {
        this.supplierId = supplyerId;
        this.supplierName = supplierName;
    }

    public ListSupplier(String supplierId, String supplierName, String supplierPhoneNumber, String supplierAddress, String supplyingSince)
    {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.supplierPhoneNumber = supplierPhoneNumber;
        this.supplierAddress = supplierAddress;
        this.supplyingSince = supplyingSince;
    }

    public void setSupplierAddress(String supplierAddress)
    {
        this.supplierAddress = supplierAddress;
    }

    public String getSupplierPhoneNumber()
    {
        return supplierPhoneNumber;
    }

    public String getSupplierAddress()
    {
        return supplierAddress;
    }

    public String getSupplierDescription()
    {
        return supplierDescription;
    }

    public String getSupplyingSince()
    {
        return supplyingSince;
    }

    public String getSupplierId()
    {
        return supplierId;
    }

    public void setSupplierId(String supplierId)
    {
        this.supplierId = supplierId;
    }

    public String getSupplierName()
    {
        return supplierName;
    }

    public void setSupplierName(String supplierName)
    {
        this.supplierName = supplierName;
    }

}
