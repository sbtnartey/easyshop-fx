package list;

public class ListBranches
{

    public String id;
    public String branchName;
    public String branchContactNumber;
    public String branchAddress;
    public String creatorId;
    public String date;

    public ListBranches(String id, String brandName, String brandComment, String supplyerName, String creatorId, String date)
    {
        this.id = id;
        this.branchName = brandName;
        this.branchContactNumber = brandComment;
        this.branchAddress = supplyerName;
        this.creatorId = creatorId;
        this.date = date;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getBranchName()
    {
        return branchName;
    }

    public void setBranchName(String branchName)
    {
        this.branchName = branchName;
    }

    public String getBranchContactNumber()
    {
        return branchContactNumber;
    }

    public void setBranchContactNumber(String branchContactNumber)
    {
        this.branchContactNumber = branchContactNumber;
    }

    public String getBranchAddress()
    {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress)
    {
        this.branchAddress = branchAddress;
    }

    public String getCreatorId()
    {
        return creatorId;
    }

    public void setCreatorId(String creatorId)
    {
        this.creatorId = creatorId;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }
}
