/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author rifat
 */
public class ListCategory {
    public String id;
    public String catagoryName;
    public String catagoryDescription;
    public String creatorId;
    public String createdDate;

    public ListCategory(String id, String catagoryName, String catagoryDescription, String creatorId, String date) {
        this.id = id;
        this.catagoryName = catagoryName;
        this.catagoryDescription = catagoryDescription;
        this.creatorId = creatorId;
        this.createdDate = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatagoryName() {
        return catagoryName;
    }

    public void setCatagoryName(String catagoryName) {
        this.catagoryName = catagoryName;
    }

    public String getCatagoryDescription() {
        return catagoryDescription;
    }

    public void setCatagoryDescription(String catagoryDescription) {
        this.catagoryDescription = catagoryDescription;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}