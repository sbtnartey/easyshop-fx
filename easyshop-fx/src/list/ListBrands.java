package list;

import entities.Brands;


public class ListBrands extends Brands
{

    public ListBrands(String id, String brandName, String brandComment, String supplierName, String createdBy, String createdDate)
    {
        this.id = id;
        this.brandName = brandName;
        this.brandComment = brandComment;
        this.supplierName = supplierName;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getBrandName()
    {
        return brandName;
    }

    public void setBrandName(String brandName)
    {
        this.brandName = brandName;
    }

    public String getBrandComment()
    {
        return brandComment;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }
    
    public void setBrandComment(String brandComment)
    {
        this.brandComment = brandComment;
    }

    public String getSupplierName()
    {
        return supplierName;
    }

    public void setSupplierName(String supplierName)
    {
        this.supplierName = supplierName;
    }
    
}
