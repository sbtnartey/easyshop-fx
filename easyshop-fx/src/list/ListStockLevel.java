/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package list;

/**
 *
 * @author ICSGH-BILLY
 */
public class ListStockLevel
{
    public String id;
    public String productId;
    public String productName;
    public String quantityAvailable;
    public String purchasePrice;
    public String sellingPrice;

    public ListStockLevel(String id, String productId, String productName, String quantityAvailable, String purchasePrice, String sellingPrice)
    {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.quantityAvailable = quantityAvailable;
        this.purchasePrice = purchasePrice;
        this.sellingPrice = sellingPrice;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getQuantityAvailable()
    {
        return quantityAvailable;
    }

    public void setQuantityAvailable(String quantityAvailable)
    {
        this.quantityAvailable = quantityAvailable;
    }

    public String getSellingPrice()
    {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice)
    {
        this.sellingPrice = sellingPrice;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getPurchasePrice()
    {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice)
    {
        this.purchasePrice = purchasePrice;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }
    
}
