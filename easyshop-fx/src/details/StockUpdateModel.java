/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package details;

/**
 *
 * @author seth
 */
public class StockUpdateModel {

    public int lastAddedQty;
    public int availableQty;
    public int newQty;
    public int newUpdatedQty;
    public double newPurchasePrice;
    public double newSellingPrice;
    public String productId;
    public boolean isDelete = true;

    public int getLastAddedQty() {
        return lastAddedQty;
    }

    public void setLastAddedQty(int lastAddedQty) {
        this.lastAddedQty = lastAddedQty;
    }

    public int getAvailableQty() {
        if(isIsDelete()){
            availableQty =-lastAddedQty;
        }else{
            availableQty = +newUpdatedQty;
        }
        return availableQty;
    }

    public void setAvailableQty(int availableQty) {
        this.availableQty = availableQty;
    }

    public int getNewUpdatedQty() {
        newUpdatedQty = newQty - lastAddedQty;
        return newUpdatedQty;
    }

    public void setNewUpdatedQty(int newUpdatedQty) {
        this.newUpdatedQty = newUpdatedQty;
    }

    public double getNewPurchasePrice() {
        return newPurchasePrice;
    }

    public void setNewPurchasePrice(double newPurchasePrice) {
        this.newPurchasePrice = newPurchasePrice;
    }

    public double getNewSellingPrice() {
        return newSellingPrice;
    }

    public void setNewSellingPrice(double newSellingPrice) {
        this.newSellingPrice = newSellingPrice;
    }

    public int getNewQty() {
        return newQty;
    }

    public void setNewQty(int newQty) {
        this.newQty = newQty;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

}
