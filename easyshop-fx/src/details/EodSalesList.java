/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package details;

/**
 *
 * @author seth
 */
public class EodSalesList extends EodSales{

    public EodSalesList(String receiptNumber, String soledBy, String productName, int quantity, double sellPrice, double amount) {
        this.receiptNumber = receiptNumber;
        this.soledBy = soledBy;
        this.productName = productName;
        this.quantity = quantity;
        this.sellPrice = sellPrice;
        this.amount = amount;
    }

    
    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getSoledBy() {
        return soledBy;
    }

    public void setSoledBy(String soledBy) {
        this.soledBy = soledBy;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
    
    
}
