/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package details;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author seth
 */
public class EodSales implements Serializable{
    
    public String receiptNumber;
    public String soledBy;
    public String productName;
    public int quantity;
    public double sellPrice;
    public double amount;
    public String productId;
    
    public List<EodSalesList> eodSalesLists = new ArrayList<>();
}
