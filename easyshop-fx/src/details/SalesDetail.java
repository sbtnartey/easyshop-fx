/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package details;

import com.latlab.common.formating.ObjectValue;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import list.ListSaleItem;

/**
 *
 * @author ICSGH-BILLY
 */
public class SalesDetail implements Serializable
{
    
    private String productName;
    private double unitPrice;
    private int qty;
    private double itemAmount;
    
    
    
    public static List<SalesDetail> createSalesReceipt(List<ListSaleItem> listSaleItems)
    {
        List<SalesDetail> salesDetailsList = new ArrayList<>();
        try
        {
           for(ListSaleItem saleItem : listSaleItems)
           {
               SalesDetail salesDetail = new SalesDetail();
               salesDetail.setItemAmount(ObjectValue.get_doubleValue(saleItem.getAmount()));
               salesDetail.setProductName(saleItem.getProductName());
               salesDetail.setUnitPrice(ObjectValue.get_doubleValue(saleItem.getSellPrice()));
               salesDetail.setQty(ObjectValue.get_intValue(saleItem.getQuantity()));
               salesDetailsList.add(salesDetail);
           }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return salesDetailsList;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public double getUnitPrice()
    {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice)
    {
        this.unitPrice = unitPrice;
    }

    public int getQty()
    {
        return qty;
    }

    public void setQty(int qty)
    {
        this.qty = qty;
    }

    public double getItemAmount()
    {
        return itemAmount;
    }

    public void setItemAmount(double itemAmount)
    {
        this.itemAmount = itemAmount;
    }
    
    
}
