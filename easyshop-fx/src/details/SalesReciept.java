/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package details;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author ICSGH-BILLY
 */
public class SalesReciept
{
    private String receiptNumber;
    private double totalCost;
    private double cashHanded;
    private double balance;
    private String printedBy;
    private String shopName;
    private String shopAddress;
    private String shopPhoneNo;
    private String shopEmailAddress;
    private double amountToPay;
    
    
    
    private List<SalesDetail> saleItemsList = new LinkedList<>();

    public String getReceiptNumber()
    {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber)
    {
        this.receiptNumber = receiptNumber;
    }

    public double getTotalCost()
    {
        return totalCost;
    }

    public void setTotalCost(double totalCost)
    {
        this.totalCost = totalCost;
    }

    public double getCashHanded()
    {
        return cashHanded;
    }

    public void setCashHanded(double cashHanded)
    {
        this.cashHanded = cashHanded;
    }

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public List<SalesDetail> getSaleItemsList()
    {
        return saleItemsList;
    }

    public void setSaleItemsList(List<SalesDetail> saleItemsList)
    {
        this.saleItemsList = saleItemsList;
    }

    public String getPrintedBy()
    {
        
        return printedBy;
    }

    public void setPrintedBy(String printedBy)
    {
        this.printedBy = printedBy;
    }

    public String getShopName()
    {
        return shopName;
    }

    public void setShopName(String shopName)
    {
        this.shopName = shopName;
    }

    public String getShopAddress()
    {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress)
    {
        this.shopAddress = shopAddress;
    }

    public String getShopPhoneNo()
    {
        return shopPhoneNo;
    }

    public void setShopPhoneNo(String shopPhoneNo)
    {
        this.shopPhoneNo = shopPhoneNo;
    }

    public String getShopEmailAddress()
    {
        return shopEmailAddress;
    }

    public void setShopEmailAddress(String shopEmailAddress)
    {
        this.shopEmailAddress = shopEmailAddress;
    }

    public double getAmountToPay()
    {
        return amountToPay;
    }

    public void setAmountToPay(double amountToPay)
    {
        this.amountToPay = amountToPay;
    }
    
    
}
