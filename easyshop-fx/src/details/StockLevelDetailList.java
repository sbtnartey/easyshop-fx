/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package details;

/**
 *
 * @author Billy
 */
public class StockLevelDetailList extends StockLevelDetail{

    public StockLevelDetailList(String productName, int qtyAvailable, double sellingPrice, double totalCost) {
        this.productName = productName;
        this.qtyAvailable = qtyAvailable;
        this.totalCost = totalCost;
        this.sellingPrice = sellingPrice;
    }
    
    

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQtyAvailable() {
        return qtyAvailable;
    }

    public void setQtyAvailable(int qtyAvailable) {
        this.qtyAvailable = qtyAvailable;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
    
    
}
