package dao;

import entities.CurrentProduct;
import entities.Stock;
import gateway.StocksGateway;
import com.latlab.common.formating.ObjectValue;
import com.latlab.common.utils.DateTimeUtils;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

public class StockBLL
{

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    SQL sql = new SQL();
    StocksGateway stockGateway = new StocksGateway();

    public void save(Stock stock)
    {
        if (isUniqueNumber(stock))
        {
            stockGateway.save(stock);
        }

    }

    public void update(Stock stock)
    {
        if (isNotNull(stock))
        {

            if (checkUpdateCondition(stock))
            {
                stockGateway.update(stock);
            } else if (isUniqueNumber(stock))
            {
                stockGateway.update(stock);
            }

        }
    }

    public boolean isUniqueNumber(Stock stock)
    {
        boolean isUniqname = false;
        try
        {
            pst = con.prepareStatement("select id from " + db + ".Stock where invoice_number=?");
            pst.setString(1, stock.invoiceDate);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sucess");
                alert.setHeaderText("ERROR : Not Uniq");
                alert.setContentText("Stock invoice number" + "  '" + stock.invoiceNumber + "' " + "not Unique");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

                return isUniqname;
            }
            isUniqname = true;
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isUniqname;
    }

    public boolean checkUpdateCondition(Stock stock)
    {
        boolean isTrueUpdate = false;

        try
        {
            pst = con.prepareStatement("select * from " + db + ".Stock where id=? ");
            pst.setString(1, stock.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                return isTrueUpdate = true;
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        return isTrueUpdate;
    }

    public boolean isNotNull(Stock stock)
    {

        boolean isNotNull = false;
        if (stock.invoiceNumber.isEmpty() || stock.invoiceNumber.isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("ERROR : Null Found");
            alert.setContentText("Please fill requrer field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            return isNotNull;
        }
        return isNotNull;
    }

    public Object delete(Stock stock)
    {
        if (!stockGateway.isHavingItem(stock))
        {
            stockGateway.delete(stock);
        } else
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("ERROR");
            alert.setContentText("Invoice" + "  '" + stock.invoiceNumber + "' " + "has items");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        }
        return stock;
    }

}
