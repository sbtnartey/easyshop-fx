package dao;

import entities.Category;
import entities.Supplier;
import gateway.CategoryGetway;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

public class CategoryBLL
{

    SQL sql = new SQL();
    CategoryGetway catagoryGetway = new CategoryGetway();
    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public void save(Category catagory)
    {
        if (isUniqName(catagory))
        {
            catagoryGetway.save(catagory);
        }
    }

    public void update(Category catagory)
    {
        if (checkUpdate(catagory))
        {
            catagoryGetway.update(catagory);
        } else if (isUniqName(catagory))
        {
            catagoryGetway.update(catagory);
        }
    }

    public void delete(Category catagory)
    {
        if (catagoryGetway.isNotUse(catagory))
        {
            catagoryGetway.delete(catagory);
        } else
        {
            //noting
        }
    }

    public boolean checkUpdate(Category catagory)
    {
        boolean isTrueUpdate = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Category where category_name=? and id=?");
            pst.setString(1, catagory.catagoryName);
            pst.setString(2, catagory.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                return isTrueUpdate = true;
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isTrueUpdate;
    }

    public boolean isUniqName(Category catagory)
    {

        boolean uniqSupplyer = false;
        try
        {
            pst = con.prepareCall("select * from " + db + ".Category where category_name=?");
            pst.setString(1, catagory.catagoryName);
            rs = pst.executeQuery();
            while (rs.next())
            {
                System.out.println("in not uniq");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sucess");
                alert.setHeaderText("ERROR : used");
                alert.setContentText("Catagory" + "  '" + catagory.catagoryName + "' " + "Already exist");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

                return uniqSupplyer;
            }
            uniqSupplyer = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uniqSupplyer;
    }
}
