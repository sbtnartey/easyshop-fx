package dao;

import entities.SellCart;
import gateway.SellCartGetway;
import db.DBConnection;
import db.DBProperties;
import entities.SaleItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SellCartBLL {

    SellCartGetway sellCartGerway = new SellCartGetway();

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    public String sell(SellCart sellCart) {

        return sellCartGerway.save(sellCart);

    }

    public void updateCurrentQuantity(SellCart sellCart) {
        int oQ = Integer.parseInt(sellCart.oldQuantity);
        int nQ = Integer.parseInt(sellCart.quantity);
        int uQ = (oQ - nQ);
        //String updatedQuantity = String.valueOf(uQ);
        try {
            pst = con.prepareStatement("update "+db+".StockLevel set quantity_available=? where product_id=?");
            pst.setInt(1, uQ);
            pst.setString(2, sellCart.productId);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SellCartBLL.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Update Complate");
    }
    
    

}
