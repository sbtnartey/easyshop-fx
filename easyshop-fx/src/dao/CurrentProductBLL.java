package dao;

import entities.CurrentProduct;
import entities.StockLevel;
import gateway.CurrentProductGetway;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

/**
 * Created by rifat on 8/15/15.
 */
public class CurrentProductBLL
{

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    SQL sql = new SQL();
    CurrentProductGetway currentProductGetway = new CurrentProductGetway();

    public void save(CurrentProduct currentProduct)
    {
        if (isUniqName(currentProduct))
        {
            currentProductGetway.save(currentProduct);
        }

    }

    public void update(CurrentProduct currentProduct)
    {
        if (isNotNull(currentProduct))
        {
            if (isUpdate(currentProduct))
            {
                if (checkUpdateCondition(currentProduct))
                {
                    currentProductGetway.update(currentProduct);
                } else if (isUniqName(currentProduct))
                {
                    currentProductGetway.update(currentProduct);
                }
            }
        }
    }

    public boolean isUniqName(CurrentProduct currentProduct)
    {
        System.out.println("WE ARE IS IS UNIT NAME");
        boolean isUniqname = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Products where id=?");
            pst.setString(1, currentProduct.id);
            rs = pst.executeQuery();
            while (rs.next())
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sucess");
                alert.setHeaderText("ERROR : Not Uniq");
                alert.setContentText("Product id" + "  '" + currentProduct.id + "' " + "id not Uniq");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

                return isUniqname;
            }
            isUniqname = true;
        } catch (SQLException e)
        {

        }
        return isUniqname;
    }

    public boolean isUpdate(CurrentProduct currentProduct)
    {
        System.out.println("WE ARE IS IS UPDTE");
        boolean isUpdate = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".Products where id=? and product_name=? and quantity=? and description=? and supplier_id=? and brand_id=? "
                    + "and catagory_id=? and unit_id=? and rma_id=? and date=?");
            pst.setString(1, currentProduct.id);
            pst.setString(2, currentProduct.productName);
            pst.setString(3, currentProduct.description);
            pst.setString(4, currentProduct.supplierId);
            pst.setString(5, currentProduct.brandId);
            pst.setString(6, currentProduct.catagoryId);
            pst.setString(7, currentProduct.unitId);
            pst.setString(8, currentProduct.rmaId);
            pst.setString(11, currentProduct.createdDate);
            rs = pst.executeQuery();
            while (rs.next())
            {
                return isUpdate;
            }
            isUpdate = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isUpdate;
    }

    public boolean checkUpdateCondition(CurrentProduct currentProduct)
    {
        boolean isTrueUpdate = false;
        if (isUpdate(currentProduct))
        {
            try
            {
                pst = con.prepareStatement("select * from " + db + ".Products where id=?");
                pst.setString(1, currentProduct.id);
                rs = pst.executeQuery();
                while (rs.next())
                {

                    return isTrueUpdate = true;
                }
            } catch (SQLException ex)
            {
                Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return isTrueUpdate;
    }

    public boolean isNotNull(CurrentProduct currentProduct)
    {

        boolean isNotNull = false;
        if (currentProduct.productName.isEmpty() || currentProduct.supplierId.isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("ERROR : Null Found");
            alert.setContentText("Please fill requrer field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            return isNotNull;
        }
        return isNotNull;
    }

    public Object delete(CurrentProduct currentProduct)
    {
        if (currentProductGetway.isNotSoled(currentProduct))
        {
            currentProductGetway.delete(currentProduct);
        } else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sucess");
            alert.setHeaderText("WARNING : ");
            alert.setContentText("Sorry cannot this product " + currentProduct.productName + " has sales.");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        }
        return currentProduct;
    }

}
