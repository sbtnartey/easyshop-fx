package dao;

import entities.Brands;
import entities.Supplier;
import gateway.BrandsGetway;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

public class BrandBLL {

    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    BrandsGetway brandsGetway = new BrandsGetway();

    public void save(Brands brands) {
        if (isUniqName(brands)) {
            brandsGetway.save(brands);
        }
    }

    public void update(Brands brands) {
        if (isTrueUpdate(brands)) {
            brandsGetway.update(brands);
        } else if (isUniqName(brands)) {
            brandsGetway.update(brands);
        }

    }

    public void delete(Brands brands){
        if(brandsGetway.isNotUsed(brands)){
            brandsGetway.delete(brands);
        }else{
            //noting
        }
    }

    public boolean isTrueUpdate(Brands brands) {
        boolean isTrue = false;
        brands.supplierId = sql.getIdNo(brands.supplierName, brands.supplierId, "Supplier", "supplier_name");
        System.out.println("we are in update");

        try {
            pst = con.prepareStatement("SELECT * FROM "+db+".Brands WHERE brand_name =? AND supplier_id =? AND id =?");
            pst.setString(1, brands.brandName);
            pst.setString(2, brands.supplierId);
            pst.setString(3, brands.id);
            rs = pst.executeQuery();
            while (rs.next()) {
                return isTrue;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isTrue;
    }


    public boolean isUniqName(Brands brands) {
        boolean uniqSupplier = false;
        try {
            pst = con.prepareCall("select * from "+db+".Brands where brand_name=? and supplier_id=?");
            brands.supplierId = sql.getIdNo(brands.supplierName, brands.supplierId, "Supplier", "supplier_name");
            pst.setString(1, brands.brandName);
            pst.setString(2, brands.supplierId);
            rs = pst.executeQuery();
            while (rs.next()) {
                System.out.println("in not uniq");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sucess");
                alert.setHeaderText("ERROR : used");
                alert.setContentText("Brand" + "  '" + brands.brandName + "' " + "Already exist");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
               
                return uniqSupplier;
            }
            uniqSupplier = true;
        } catch (SQLException ex) {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uniqSupplier;
    }

}
