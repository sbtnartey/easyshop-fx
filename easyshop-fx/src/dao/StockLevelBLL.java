package dao;

import entities.CurrentProduct;
import entities.StockLevel;
import gateway.CurrentProductGetway;
import gateway.StockLevelGateway;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;

public class StockLevelBLL
{
    
    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();
    
    SQL sql = new SQL();
    StockLevelGateway stockLevelGateway = new StockLevelGateway();
    
    public void save(StockLevel stockLevel)
    {
        if (isUniqName(stockLevel))
        {
            stockLevelGateway.save(stockLevel);
        }
        
    }
    
    public void update(StockLevel stockLevel)
    {
        if (isNotNull(stockLevel))
        {
            if (isUpdate(stockLevel))
            {
                if (checkUpdateCondition(stockLevel))
                {
                    stockLevelGateway.update(stockLevel);
                } else if (isUniqName(stockLevel))
                {
                    stockLevelGateway.update(stockLevel);
                }
            }
        }
    }
    
    public boolean isUniqName(StockLevel stockLevel)
    {
        boolean isUniqname = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".StockLevel where product_id=?");
            pst.setString(1, stockLevel.productId);
            rs = pst.executeQuery();
            while (rs.next())
            {                
                return isUniqname;
            }
            isUniqname = true;
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isUniqname;
    }
    
    public boolean isUpdate(StockLevel stockLevel)
    {
        boolean isUpdate = false;
        try
        {
            pst = con.prepareStatement("select * from " + db + ".StockLevel where id=? and quantity_available=? and selling_price=? and product_id=? and purchase_price=? "
                    + "and last_modified_date=? and last_modified_by=?");
            pst.setString(1, stockLevel.id);
            pst.setInt(2, Integer.parseInt(stockLevel.quantityAvailable));
            pst.setDouble(3, Double.valueOf(stockLevel.sellingPrice));
            pst.setString(4, stockLevel.productId);
            pst.setDouble(5, Double.valueOf(stockLevel.purchasePrice));
            pst.setDate(8, new java.sql.Date(new Date().getTime()));
            pst.setString(9, stockLevel.lastModifiedBy);
            
            rs = pst.executeQuery();
            while (rs.next())
            {
                return isUpdate;
            }
            isUpdate = true;
        } catch (SQLException ex)
        {
            Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isUpdate;
    }
    
    public boolean checkUpdateCondition(StockLevel stockLevel)
    {
        boolean isTrueUpdate = false;
        if (isUpdate(stockLevel))
        {
            try
            {
                pst = con.prepareStatement("select * from " + db + ".StockLevel where id=? and product_id=?");
                pst.setString(1, stockLevel.id);
                pst.setString(2, stockLevel.productId);
                rs = pst.executeQuery();
                while (rs.next())
                {
                    
                    return isTrueUpdate = true;
                }
            } catch (SQLException ex)
            {
                Logger.getLogger(CurrentProduct.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return isTrueUpdate;
    }
    
    public boolean isNotNull(StockLevel stockLevel)
    {
        
        boolean isNotNull = false;
        if (stockLevel.productId.isEmpty() || stockLevel.sellingPrice.isEmpty() || stockLevel.quantityAvailable.isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sucess");
            alert.setHeaderText("ERROR : Null Found");
            alert.setContentText("Required fields missing");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
     
            return isNotNull;
        }
        return isNotNull = true;
    }
    
    public Object delete(StockLevel stockLevel)
    {
        if (stockLevelGateway.isSoled(stockLevel))
        {
            stockLevelGateway.delete(stockLevel);
        } else
        {
            //nothing
        }
        return stockLevel;
    }
    
    public void updateStockQuantity(StockLevel stockLevel)
    {
        try
        {
            if (isUniqName(stockLevel))
            {
                if (isNotNull(stockLevel))
                {
                    stockLevelGateway.save(stockLevel);
                }
            } else
            {
                //update stocklevel of product
                //select all from stock level where product is found
                pst = con.prepareStatement("select id, quantity_available from " + db + ".StockLevel where product_id=?");
                pst.setString(1, stockLevel.productId);
                rs = pst.executeQuery();
                while (rs.next())
                {
                    stockLevel.id = rs.getString(1);
                    int previousQty = rs.getInt(2);
                    int newQty = Integer.parseInt(stockLevel.quantityAvailable);
                    int updatedQty = previousQty + newQty;
                    
                    stockLevel.quantityAvailable = String.valueOf(updatedQty);
                    stockLevelGateway.update(stockLevel);
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
}
