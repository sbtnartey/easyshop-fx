/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.SellCart;
import gateway.SaleItemGateway;

/**
 *
 * @author ICSGH-BILLY
 */
public class SaleItemBLL
{
    SellCartBLL sellCartBLL = new SellCartBLL();
    SaleItemGateway saleItemGateway = new SaleItemGateway();
    
    public void save(SellCart sellCart)
    {
        sellCartBLL.updateCurrentQuantity(sellCart);
        saleItemGateway.save(sellCart);
    }
}
