/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

/**
 *
 * @author ICSGH-BILLY
 */
public class ReportFiles
{
    public static final String BASE_FOLDER = "/report/";
    public static final String sales_receipt = BASE_FOLDER + "sales_receipt.jasper";
    public static final String supplies_stock_report = BASE_FOLDER + "supplies_stock.jasper";
    public static final String top_products_report = BASE_FOLDER + "top_selling_products.jasper";
    public static final String products_report = BASE_FOLDER + "products_report.jasper";
    public static final String eod_sales_report = BASE_FOLDER + "daily_sales_report.jasper";
    public static final String stock_level_report = BASE_FOLDER + "stock_level_report.jasper";
}
