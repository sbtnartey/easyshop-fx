/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableCell;

/**
 *
 * @author ICSGH-BILLY
 */
public class SpinnerCell<S, T> extends TableCell<S, T>
{

    private Spinner<Integer> spinner;
    private ObjectProperty<Integer> integerField = new SimpleObjectProperty<>();
    private ObservableValue<T> ov;

    public SpinnerCell()
    {
        final int step = 1;
        spinner = new Spinner<>();
        spinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, step));
        setEditable(true);

    }

    @Override
    protected void updateItem(T item, boolean empty)
    {
        super.updateItem(item, empty);

        if (empty)
        {
            setText(null);
            setGraphic(null);
        } else
        {
            setText(null);
            if (isFocused() || isSelected() || isPressed())
            {
                setGraphic(this.spinner);

               
                if (this.ov instanceof Property)
                {
                    this.spinner.getValueFactory().valueProperty().unbindBidirectional((Property) this.ov);
                }
                this.ov = getTableColumn().getCellObservableValue(getIndex());

                if (this.ov instanceof Property)
                {
                    this.spinner.getValueFactory().valueProperty().bindBidirectional((Property) this.ov);
                }
            }

        }

    }
}
