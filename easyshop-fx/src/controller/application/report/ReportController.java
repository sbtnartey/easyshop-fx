/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.report;

import controller.application.settings.OrgSettingController;
import db.DBConnection;
import db.DBProperties;
import details.EodSales;
import details.SalesReciept;
import details.StockLevelDetail;
import gateway.ReportGateway;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import report.ReportFiles;

/**
 *
 * @author ICSGH-BILLY
 */
public class ReportController extends ReportManager implements Initializable {

    @FXML
    public AnchorPane rcAnchorPane;
    @FXML
    private Label lblHeader;
    @FXML
    private Button topProductsBtn;
    @FXML
    private Button suppliersBtn;
    @FXML
    private Button dailySalesBtn;
    @FXML
    private Button availableStockBtn;

    private EodSales eodSales = new EodSales();
    private StockLevelDetail stockLevelDetail = new StockLevelDetail();
    private ReportGateway reportGateway = new ReportGateway();
    private String shopName = "";
    private String shopPhoneNo = "";
    private String shopAddress = "";
    private String shopEmailAddress = "";

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    DBProperties dBProperties = new DBProperties();
    PreparedStatement pst;
    ResultSet rs;
    String db = dBProperties.loadPropertiesFile();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("SELECT shop_name,shop_contact_address,shop_contact_numbers FROM " + db + ".Shop ORDER BY id ASC LIMIT 1");
            rs = pst.executeQuery();
            while (rs.next()) {
                shopName = rs.getString(1);
                shopPhoneNo = rs.getString(3);
                shopAddress = rs.getString(2);
//                salesReciept.setShopEmailAddress(rs.getString(4));
                addParam("shopName", shopName);
                addParam("shopAddress", shopAddress);
                addParam("shopPhoneNo", shopPhoneNo);
                addParam("statementDate", new Date());
                System.out.println("ReportController::initialize():: shopName =" + shopName + ", shopPhoneNo =" + shopPhoneNo);
            }
            //addParam("shopEmailAddress", salesReciept.getShopEmailAddress());
        } catch (SQLException ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void generateTopProductsBtnAction(ActionEvent actionEvent) {

    }

    @FXML
    private void generateSuppliersBtnAction(ActionEvent actionEvent) {

    }

    @FXML
    private void generateDailySalesBtnAction(ActionEvent actionEvent) {

    }

    @FXML
    private void generateEndOfDayBtnAction(ActionEvent actionEvent) {
        try {
            addParam("reportTitle", "Daily Sales Report");
            reportGateway.getEodSales(eodSales);
            InputStream inputStream = this.getClass().getResourceAsStream(ReportFiles.eod_sales_report);
            showReport(eodSales.eodSalesLists, inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void generateStocksBtnAction(ActionEvent actionEvent) {
        try {
            addParam("reportTitle", "Stock Level Report");
            reportGateway.getStockLevel(stockLevelDetail);
            InputStream inputStream = this.getClass().getResourceAsStream(ReportFiles.stock_level_report);
            showReport(stockLevelDetail.stockLevelDetailLists, inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void generateSalesOverPeriodBtnAction(ActionEvent actionEvent) {

    }

    public void generateSalesReceipt(SalesReciept salesReciept) {
        try {

            addParam("billingDate", new Date());
            addParam("receiptNumber", salesReciept.getReceiptNumber());
            addParam("printedBy", salesReciept.getPrintedBy());
            addParam("cashPaid", salesReciept.getCashHanded());
            addParam("totalCost", salesReciept.getTotalCost());
            addParam("amountToPay", salesReciept.getTotalCost());
//            addParam("vat", "");
//            addParam("nhis", "");
//            addParam("discount", "");

            InputStream inputStream = this.getClass().getResourceAsStream(ReportFiles.sales_receipt);
            showReport(salesReciept.getSaleItemsList(), inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
