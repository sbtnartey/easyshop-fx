/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import gateway.SupplierGetway;
import custom.EffectUtility;
import db.SQL;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import media.userNameMedia;
import entities.Supplier;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author rifat
 */
public class AddSupplierController implements Initializable
{

    private String usrId;
    public String supplierId;

    private userNameMedia media;
    @FXML
    private TextField tfSupplierName;
    @FXML
    private TextArea taSupplierAddress;
    @FXML
    private TextArea taSupplierDescription;
    @FXML
    public Button btnSave;
    @FXML
    private TextArea taContactNumbers;
    @FXML
    public Button btnUpdate;
    @FXML
    public Button btnClose;
    @FXML
    public Label lblCaption;

    @FXML
    private DatePicker dpSupplyingSince;

    private Stage primaryStage;
    @FXML
    private AnchorPane apContent;
    
    SQL sql = new SQL();

    public userNameMedia getMedia()
    {
        return media;
    }

    public void setMedia(userNameMedia media)
    {
        usrId = media.getId();
        this.media = media;
    }

    Supplier oSupplier = new Supplier();
    SupplierGetway supplierGetway = new SupplierGetway();

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {

    }

    @FXML
    private void btnSaveOnAction(ActionEvent event)
    {
        if (isNotNull())
        {
            oSupplier.supplierName = tfSupplierName.getText();
            oSupplier.supplierContactNumber = taContactNumbers.getText();
            oSupplier.supplierAddress = taSupplierAddress.getText();
            oSupplier.supplierDescription = taSupplierDescription.getText();
            oSupplier.supplyingSince = dpSupplyingSince.getValue().toString();
            oSupplier.creatorId = usrId;
            supplierGetway.save(oSupplier);
            clrAll();
        }
    }

    public boolean isNotNull()
    {
        boolean isNotNull;
        if (tfSupplierName.getText().trim().isEmpty()
                || taContactNumbers.getText().trim().isEmpty()
                || taSupplierAddress.getText().trim().isEmpty()
                || dpSupplyingSince.getValue() == null)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error : Null Field Found");
            alert.setContentText("Please fill all require field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            isNotNull = false;

        } else
        {
            isNotNull = true;
        }
        return isNotNull;
    }

    private void clrAll()
    {
        tfSupplierName.clear();
        taContactNumbers.clear();
        taSupplierAddress.clear();
        taSupplierDescription.clear();
    }

    @FXML
    private void btnUpdateOnAction(ActionEvent event)
    {
        try
        {
            if (isNotNull())
            {
                oSupplier.id = supplierId;
                oSupplier.supplierName = tfSupplierName.getText().trim();
                oSupplier.supplierContactNumber = taContactNumbers.getText().trim();
                oSupplier.supplierAddress = taSupplierAddress.getText().trim();
                oSupplier.supplierDescription = taSupplierDescription.getText().trim();
                oSupplier.supplyingSince = dpSupplyingSince.getValue().toString();
                supplierGetway.updateNow(oSupplier);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @FXML
    private void btnCloseOnAction(ActionEvent event)
    {
        Stage stage = (Stage) btnUpdate.getScene().getWindow();
        stage.close();
        refreshStockList(event);
    }

    public void showDetails()
    {
        oSupplier.id = supplierId;
        supplierGetway.selectedView(oSupplier);
        tfSupplierName.setText(oSupplier.supplierName);
        taContactNumbers.setText(oSupplier.supplierContactNumber);
        taSupplierAddress.setText(oSupplier.supplierAddress);
        taSupplierDescription.setText(oSupplier.supplierDescription);
        dpSupplyingSince.setValue(sql.stringToLocalDate(oSupplier.supplyingSince));
    }

    public void addSupplierStage(Stage stage)
    {
        EffectUtility.makeDraggable(stage, apContent);
    }

    public void refreshStockList(ActionEvent actionEvent)
    {
        try
        {
            ViewSupplierController vsc = new ViewSupplierController();
            FXMLLoader fXMLLoader = new FXMLLoader();
            fXMLLoader.load(getClass().getResource("/view/application/stock/ViewSupplier.fxml").openStream());
            ViewSupplierController viewSupplierController = fXMLLoader.getController();
            viewSupplierController.btnRefreshOnAction(actionEvent);
        } catch (IOException ex)
        {
            Logger.getLogger(AddInvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
