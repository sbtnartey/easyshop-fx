/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import dao.CurrentProductBLL;
import dao.StockItemsBLL;
import dao.StockLevelBLL;
import entities.CurrentProduct;
import entities.Stock;
import entities.StockLevel;
import gateway.CurrentProductGetway;
import gateway.StockLevelGateway;
import gateway.StocksGateway;
import list.ListProduct;
import list.ListStock;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import entities.StockItem;
import gateway.StockItemsGateway;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import list.PreStockItemsList;
import media.userNameMedia;

/**
 * FXML Controller class
 *
 * @author rifat
 */
public class AddInvoiceItemController implements Initializable
{

    public Button btnAddProduct;
    private String usrId;
    private String stockId;
    private String productId;
    private userNameMedia nameMedia;

    CurrentProduct currentProduct = new CurrentProduct();
    CurrentProductBLL currentProductBLL = new CurrentProductBLL();
    CurrentProductGetway currentProductGetway = new CurrentProductGetway();

    Stock stock = new Stock();
    StockLevel stockLevel = new StockLevel();
    StockItem stockItem = new StockItem();

    StockLevelGateway stockLevelGateway = new StockLevelGateway();
    StockLevelBLL stockLevelBLL = new StockLevelBLL();

    StocksGateway stocksGateway = new StocksGateway();
    StockItemsGateway stockItemsGateway = new StockItemsGateway();
    StockItemsBLL stockItemsBLL = new StockItemsBLL();
    
    SQL sql = new SQL();
    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    @FXML
    private Button btnClose;
    @FXML
    private TextField tfProductQuantity;
    @FXML
    private TextField tfProductUnitPrice;
    @FXML
    private TextField tfProductSellPrice;
    @FXML
    private TextField tfProductCost;
    @FXML
    private TableView<ListStock> tblViewStock;
    @FXML
    private TableView<ListProduct> tblViewProduct;
    @FXML
    private TableColumn<Object, Object> tblClmInvoiceNumber;
    @FXML
    private TableColumn<Object, Object> tblClmInvoiceDate;
    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private DatePicker dpDate;
    @FXML
    public Button btnSave;
    @FXML
    public Button btnUpdate;
    @FXML
    private TextField tfInvoiceSearch;
    @FXML
    private TextField tfProductSearch;
    @FXML
    private TextField tfCost;
    @FXML
    private MenuButton mbtnStockInvoice;
    @FXML
    private MenuButton mbtnProduct;

    public String id;

    @FXML
    public Button btnRemoveSelected;
    @FXML
    public Button btnAddToList;
    @FXML
    public Label lblHeader;

    @FXML
    public TableView<PreStockItemsList> tblPreStockItems;

    @FXML
    public TableColumn<Object, Object> tblClmSIInvoiceNumber;
    @FXML
    public TableColumn<Object, Object> tblClmSIProductName;
    @FXML
    public TableColumn<Object, Object> tblClmSIQuantity;
    @FXML
    public TableColumn<Object, Object> tblClmSIPurchasePrice;
    @FXML
    public TableColumn<Object, Object> tblClmSISellPrice;
    @FXML
    public TableColumn<Object, Object> tblClmSIExpiration;

    ObservableList<PreStockItemsList> preStockItemsList = FXCollections.observableArrayList();

    public userNameMedia getNameMedia()
    {
        return nameMedia;
    }

    public void setNameMedia(userNameMedia nameMedia)
    {
        usrId = nameMedia.getId();
        this.nameMedia = nameMedia;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        //tfValue.setVisible(false);
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event)
    {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event)
    {
        try
        {

            if (!tblPreStockItems.getItems().isEmpty())
            {
                int loop = tblPreStockItems.getItems().size();
                for (int i = 0; i < loop; i++)
                {
                    tblPreStockItems.getSelectionModel().select(i);

                    stockItem.qty = tblPreStockItems.getSelectionModel().getSelectedItem().getItemQty();
                    stockItem.stockId = tblPreStockItems.getSelectionModel().getSelectedItem().getStockId();
                    stockItem.productId = tblPreStockItems.getSelectionModel().getSelectedItem().getProductId();
                    stockItem.purchasePrice = tblPreStockItems.getSelectionModel().getSelectedItem().getPurchasePrice();
                    stockItem.sellingPrice = tblPreStockItems.getSelectionModel().getSelectedItem().getSellingPrice();
                    //save stock items 

                    if (stockItemsGateway.save(stockItem))
                    {
                        //create stocklevel for each product
                        stockLevel.productId = stockItem.productId;
                        stockLevel.quantityAvailable = stockItem.qty;
                        stockLevel.purchasePrice = stockItem.purchasePrice;
                        stockLevel.sellingPrice = stockItem.sellingPrice;
                        stockLevel.latestStock = stockItem.stockId;
                        stockLevel.lastAddQty = stockItem.qty;
                        stockLevelBLL.updateStockQuantity(stockLevel);
                    }

                }
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Sucess");
                alert.setHeaderText("Sucess : save sucess");
                alert.setContentText("Product Level Updated successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sumStockItemsTotal()
    {

    }

    private boolean isNotNull()
    {
        boolean insNotNull = false;
        if (mbtnStockInvoice.getText().contains("Select Invoice")
                || tfProductSellPrice.getText().isEmpty()
                || tfProductUnitPrice.getText().isEmpty()
                || tfProductQuantity.getText().isEmpty())
        {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("error");
            alert.setHeaderText("ERROR : NULL FOUND");
            alert.setContentText("Please fill all require field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            insNotNull = false;
        } else
        {
            insNotNull = true;
        }
        return insNotNull;
    }

    @FXML
    public void btnAddProductOnAction(ActionEvent actionEvent)
    {
        AddProductController addProductController = new AddProductController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddProduct.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddProductController productController = fxmlLoader.getController();
            media.setId(usrId);
            productController.setNameMedia(media);
            productController.lblCaption.setText("Add Product");
            productController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            productController.addProductStage(nStage);
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @FXML
    private void tfInvoiceSearchOnKeyReleased()
    {
        stock.stockDetailsList.clear();

        stock.invoiceNumber = tfInvoiceSearch.getText().trim();

        tblViewStock.setItems(stock.stockDetailsList);
        tblClmInvoiceDate.setCellValueFactory(new PropertyValueFactory<>("invoiceDate"));
        tblClmInvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("invoiceNumber"));

        stocksGateway.searchView(stock);

    }

    @FXML
    private void tfProductSearchOnKeyReleased()
    {
        currentProduct.currentProductList.clear();

        currentProduct.productName = tfProductSearch.getText().trim();
        tblViewProduct.setItems(currentProduct.currentProductList);
        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));

        currentProductGetway.searchView(currentProduct);
    }

    @FXML
    private void mbtnStockInvoiceOnClick(MouseEvent event)
    {
        stock.invoiceNumber = tfInvoiceSearch.getText().trim();
        tblViewStock.setItems(stock.stockDetailsList);
        tblClmInvoiceDate.setCellValueFactory(new PropertyValueFactory<>("invoiceDate"));
        tblClmInvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("invoiceNumber"));

        stocksGateway.searchView(stock);
    }

    @FXML
    private void mbtnProductOnClick(MouseEvent event)
    {
        currentProduct.productName = tfProductSearch.getText();
        tblViewProduct.setItems(currentProduct.currentProductList);
        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));

        currentProductGetway.searchView(currentProduct);

    }

    @FXML
    private void tblStockInvoiceOnClick(MouseEvent event)
    {
        mbtnStockInvoice.setText(tblViewStock.getSelectionModel().getSelectedItem().getInvoiceNumber());
        stockId = tblViewStock.getSelectionModel().getSelectedItem().getId();
        System.out.println("stock id " + stockId);
    }

    @FXML
    private void tblProductOnClick(MouseEvent event)
    {
        mbtnProduct.setText(tblViewProduct.getSelectionModel().getSelectedItem().getProductName());
        productId = tblViewProduct.getSelectionModel().getSelectedItem().getId();
        System.out.println("product id " + productId);
    }

    @FXML
    private void btnAddToListAction(ActionEvent event)
    {
        if (isNotNull())
        {
            stockItem.qty = tfProductQuantity.getText().trim();
            stockItem.sellingPrice = tfProductSellPrice.getText().trim();
            stockItem.purchasePrice = tfProductUnitPrice.getText().trim();
            
            if(dpDate.getValue() == null){
                 stockItem.expirationDate = null;
            }else{
                 stockItem.expirationDate = dpDate.getValue().toString();
            }
           

            stockItem.invoiceNumber = sql.getName(stockId, stock.invoiceNumber, "Stock");
            stockItem.productName = sql.getName(productId, currentProduct.productName, "Products");
            stockItem.stockId = stockId;
            stockItem.productId = productId;
            System.out.println("stock.productName" + stockItem.productName);

            preStockItemsList.add(new PreStockItemsList(stockItem.qty, stockItem.productId, stockItem.stockId, stockItem.invoiceNumber, stockItem.productName, stockItem.sellingPrice,
                    stockItem.purchasePrice, stockItem.expirationDate));
            viewAll();
            clearForm();
        }
    }

    @FXML
    private void tfProductCostKeyReleased(KeyEvent event)
    {
        if (!tfCost.getText().isEmpty() && !tfProductQuantity.getText().isEmpty())
        {
            int qty = Integer.parseInt(tfProductQuantity.getText());
            double cost = Double.parseDouble(tfCost.getText());

            double unitPrice = (cost / (qty * 1.0));

            tfProductUnitPrice.setText(String.valueOf(unitPrice));
        }

    }

    @FXML
    private void tfProductQuantityKeyReleased(KeyEvent event)
    {
        if (!tfCost.getText().isEmpty() && !tfProductQuantity.getText().isEmpty())
        {
            int qty = Integer.parseInt(tfProductQuantity.getText());
            double cost = Double.parseDouble(tfCost.getText());

            double unitPrice = (cost / (qty * 1.0));

            tfProductUnitPrice.setText(String.valueOf(unitPrice));
        }
    }

    private void viewAll()
    {
        tblPreStockItems.setItems(preStockItemsList);

        tblClmSIExpiration.setCellValueFactory(new PropertyValueFactory<>("expirationDate"));
        tblClmSIProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmSIPurchasePrice.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        tblClmSISellPrice.setCellValueFactory(new PropertyValueFactory<>("sellingPrice"));
        tblClmSIQuantity.setCellValueFactory(new PropertyValueFactory<>("itemQty"));
//        tblClmSIInvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("invoiceNumber"));

    }

    @FXML
    private void btnClearAllOnAction(ActionEvent actionEvent)
    {
        clearAll();
    }

    private void clearForm()
    {
        tfProductQuantity.clear();
        tfProductSellPrice.clear();
        tfProductSellPrice.clear();

        mbtnProduct.setText("Select Product");
    }

    private void clearAll()
    {
        tfProductQuantity.clear();
        tfProductSellPrice.clear();
        tfProductSellPrice.clear();
        dpDate.setValue(null);
        preStockItemsList.clear();

        mbtnProduct.setText("Select Product");
        mbtnStockInvoice.setText("Select Invoice");
    }

    public void refreshProductList()
    {
        try
        {
            CurrentStoreController asc = new CurrentStoreController();
            FXMLLoader fXMLLoader = new FXMLLoader();
            fXMLLoader.load(getClass().getResource("/view/application/stock/CurrentStore.fxml").openStream());
            CurrentStoreController currentStoreController = fXMLLoader.getController();
            currentStoreController.viewDetails();
        } catch (IOException ex)
        {
            Logger.getLogger(AddInvoiceItemController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnClearSelectedOnAction(ActionEvent event)
    {
        if (tblPreStockItems.getItems().size() != 0)
        {
            tblPreStockItems.getItems().removeAll(tblPreStockItems.getSelectionModel().getSelectedItems());
        } else
        {
            System.out.println("EMPTY");
        }
    }

    public void showDetails()
    {
        stockItem.id = id;
        System.out.println("selected stock item id " + stockItem.id);
        stockItemsGateway.selectedView(stockItem);
        
        System.out.println("selected stock item qty " + stockItem.qty);
        mbtnStockInvoice.setText(stockItem.invoiceNumber);
        mbtnProduct.setText(stockItem.productName);
        tfCost.setText(stockItem.totalCost);
        tfProductQuantity.setText(stockItem.qty);
        tfProductSellPrice.setText(stockItem.sellingPrice);
        tfProductUnitPrice.setText(stockItem.purchasePrice);
    }
    
    @FXML
    private void btnUpdateOnAction(ActionEvent event)
    {
        if(isNotNull()){
            stockItem.qty = tfProductQuantity.getText().trim();
            stockItem.sellingPrice = tfProductSellPrice.getText().trim();
            stockItem.purchasePrice = tfProductUnitPrice.getText().trim();
            stockItemsBLL.updateStockItem(stockItem);
        }
    }
}
