/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.userNameMedia;
import controller.application.SettingsController;
import dao.StockItemsBLL;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import entities.StockItem;
import gateway.StockItemsGateway;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import list.ListStockItems;


public class ViewInvoiceItemController implements Initializable
{
    StockItem stockItem = new StockItem();
    StockItemsGateway stockItemsGateway = new StockItemsGateway();
    StockItemsBLL stockItemsBLL = new StockItemsBLL();
   

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    private String usrId;

    private userNameMedia media;
    @FXML
    public StackPane spProductContent;
    @FXML
    private TextField tfSearch;
    @FXML
    private TextField tfSearchProduct;
    @FXML
    private Button btnAddNew;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnDelete;
    @FXML
    private TableView<ListStockItems> tblViewCurrentStore;
    
    @FXML
    private TableColumn<Object, Object> tblClmInvoiceNumber;
    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private TableColumn<Object, Object> tblClmProductquantity;
    @FXML
    private TableColumn<Object, Object> tblClmProductUnitPrice;
    @FXML
    private TableColumn<Object, Object> tblClmProductSellPrice;
    @FXML
    private TableColumn<Object, Object> tblClmCreatedDate;
    @FXML
    private TableColumn<Object, Object> tblClmExpiryDate;
    @FXML
    private MenuItem miSellSelected;

    String suplyerId;
    String suplyerName;
    String brandId;
    String brandName;
    String catagoryId;
    String catagoryName;
    String rmaID;
    String rmaName;

    SQL sql = new SQL();
    @FXML
    private Button btnRefresh;
    @FXML
    public AnchorPane apCombobox;
    private Executor executor;

    public userNameMedia getMedia()
    {
        return media;
    }

    public void setMedia(userNameMedia media)
    {
        usrId = media.getId();
        this.media = media;
    }

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        try
        {
            executor = Executors.newCachedThreadPool(runnable -> {
                Thread thread = new Thread(runnable);
                thread.setDaemon(true);
                
                return thread;
            });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @FXML
    private void tfSearchOnKeyRelese(KeyEvent event)
    {
        stockItem.productName = tfSearch.getText();
        stockItem.invoiceNumber = tfSearch.getText();
        stockItemsGateway.searchView(stockItem);

    }


    @FXML
    private void btnAddNewOnAction(ActionEvent event)
    {
        AddInvoiceItemController apc = new AddInvoiceItemController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddInvoiceItem.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddInvoiceItemController addInvoiceItemController = fxmlLoader.getController();
            media.setId(usrId);
            addInvoiceItemController.setNameMedia(media);
            addInvoiceItemController.lblHeader.setText("Add Invoice Items");
            addInvoiceItemController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    @FXML
    private void btnDeleteOnAction(ActionEvent event)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Confirm");
        alert.setContentText("Are you sure to delete this item \n to Confirm click ok");
        alert.initStyle(StageStyle.UNDECORATED);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK)
        {
            String item = tblViewCurrentStore.getSelectionModel().getSelectedItem().getId();
            stockItem.id = item;
            stockItemsBLL.deleteStockItem(stockItem);
            btnRefreshOnACtion(event);
        }

    }

    @FXML
    private void tblViewCurrentStoreOnClick(MouseEvent event
    )
    {
        if (event.getClickCount() == 2)
        {
            if (!tblViewCurrentStore.getSelectionModel().isEmpty())
            {
                viewSelected();
            } else
            {
                System.out.println("EMPTY SELECTION");
            }
        } else
        {
            tblViewCurrentStore.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event)
                {
                    tblViewCurrentStore.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                }
            });

        }
    }

    public void viewDetails()
    {
        
        Task<List<ListStockItems>> task = new Task<List<ListStockItems>>()
        {
            @Override
            protected List<ListStockItems> call() throws Exception
            {
                stockItemsGateway.viewStockItems(stockItem);
                return stockItem.stockItemsDetailsList;
            }
        };
        
        task.setOnFailed(e -> task.getException().printStackTrace());
        task.setOnSucceeded(e -> tblViewCurrentStore.setItems((ObservableList<ListStockItems>) task.getValue()));
        executor.execute(task);
        
        tblClmInvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("invoiceNumber"));
        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmProductquantity.setCellValueFactory(new PropertyValueFactory<>("quantityAvailable"));
        tblClmProductSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellingPrice"));
        tblClmProductUnitPrice.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        tblClmCreatedDate.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        tblClmExpiryDate.setCellValueFactory(new PropertyValueFactory<>("expiryDate"));
        //stockItemsGateway.viewStockItems(stockItem);
    }

    private void viewSelected()
    {
        AddInvoiceItemController apc = new AddInvoiceItemController();
        userNameMedia userMedia = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddInvoiceItem.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddInvoiceItemController addInvoiceItemController = fxmlLoader.getController();
            userMedia.setId(usrId);
            addInvoiceItemController.setNameMedia(userMedia);
            addInvoiceItemController.lblHeader.setText("Invoice Item Details");
            addInvoiceItemController.btnUpdate.setVisible(true);
            addInvoiceItemController.btnSave.setVisible(false);
            addInvoiceItemController.id = tblViewCurrentStore.getSelectionModel().getSelectedItem().getId();
            addInvoiceItemController.showDetails();
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void settingPermission()
    {
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".UserPermission where id=?");
            pst.setString(1, usrId);
            rs = pst.executeQuery();
            while (rs.next())
            {
                if (rs.getInt(8) == 0)
                {
                    btnUpdate.setDisable(true);
                    btnDelete.setDisable(true);
                }
                if (rs.getInt(3) == 0)
                {
                    btnAddNew.setDisable(true);
                }
                if (rs.getInt("SellProduct") == 0)
                {
                    miSellSelected.setDisable(true);
                } else
                {

                }
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(SettingsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnRefreshOnACtion(ActionEvent event)
    {
        stockItem.stockItemsDetailsList.clear();
        tfSearch.clear();
        
        viewDetails();

//        tblViewCurrentStore.setItems(stockItem.stockItemsDetailsList);
//        //tblClmProductId.setCellValueFactory(new PropertyValueFactory<>("productId"));
//        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
//        tblClmProductquantity.setCellValueFactory(new PropertyValueFactory<>("quantityAvailable"));
//        tblClmProductUnitPrice.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
//        tblClmProductSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellingPrice"));
//        stockItemsGateway.viewStockItems(stockItem);

    }

    @FXML
    private void tblViewCurrentStoreOnScroll(ScrollEvent event)
    {
        if (event.isInertia())
        {
            System.out.println("ALT DOWN");
        } else
        {
            System.out.println("Noting");
        }
    }
    
    @FXML
    private void btnUpdateOnAction(ActionEvent event)
    {
        if (!tblViewCurrentStore.getSelectionModel().isEmpty())
        {
            viewSelected();
        } else
        {
            System.out.println("EMPTY SELECTION");
        }
    }
    
    @FXML
    private void miUpdate(ActionEvent actionEvent){
        btnUpdateOnAction(actionEvent);
    }
    
    @FXML
    private void miDelete(ActionEvent actionEvent){
        btnDeleteOnAction(actionEvent);
    }
    
    @FXML
    private void miAddNew(ActionEvent actionEvent){
        btnAddNewOnAction(actionEvent);
    }

}
