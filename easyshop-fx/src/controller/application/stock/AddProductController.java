/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import entities.CurrentProduct;
import gateway.SupplierGetway;
import custom.EffectUtility;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import media.userNameMedia;
import gateway.CurrentProductGetway;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import list.ListProduct;

public class AddProductController implements Initializable {

    private String usrId;
    public String supplierId;
    public String brandId;
    public String categoryId;
    public String id;
    public ComboBox<String> cmbBrand;
    public ComboBox<String> cmbCatagory;
    public ComboBox<String> cbSupplier;
    public Button btnAddSupplier;
    public Button btnAddBrand;
    public Button btnAddCatagory;
    public Button btnAddUnit;
    public Button btnAddRma;

    @FXML
    private TableView<ListProduct> tblViewProduct;
    @FXML
    private TableColumn<Object, Object> tblClmProductId;
    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private TableColumn<Object, Object> tblClmProductSupplier;
    @FXML
    private TableColumn<Object, Object> tblClmProductBrand;
    @FXML
    private TableColumn<Object, Object> tblClmProductCatagory;
    @FXML
    private TableColumn<Object, Object> tblClmProductdescription;

    private userNameMedia nameMedia;
    @FXML
    private TextField tfProductName;
    @FXML
    private TextArea taProductDescription;
    @FXML
    public Button btnSave;
    @FXML
    public Button btnUpdate;
    @FXML
    public Button btnClose;
    @FXML
    public Label lblCaption;
    @FXML
    public Label lblHeader;
    @FXML
    private Rectangle recProductImage;
    private File file;
    private BufferedImage bufferedImage;
    private Image image;
    private String imagePath;
    @FXML
    private Button btnAttachImage;

    private Stage primaryStage;
    @FXML
    private AnchorPane apContent;

    @FXML
    private ComboBox<String> cbUnit;
    @FXML
    private ComboBox<String> cbRMA;

    public userNameMedia getNameMedia() {
        return nameMedia;
    }

    public void setNameMedia(userNameMedia media) {
        usrId = media.getId();
        this.nameMedia = media;
    }

    SupplierGetway supplyerGetway = new SupplierGetway();

    CurrentProduct currentProduct = new CurrentProduct();
    CurrentProductGetway currentProductGetway = new CurrentProductGetway();
    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void btnSaveOnAction(ActionEvent event) {
        if (isNotNull()) {
            currentProduct.productName = tfProductName.getText();
            currentProduct.description = taProductDescription.getText();
            currentProduct.supplierId = supplierId;
            currentProduct.brandId = brandId;
            currentProduct.catagoryId = categoryId;
            currentProduct.createdBy = usrId;
            currentProduct.imagePath = imagePath;

            currentProductGetway.save(currentProduct);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("error");
            alert.setHeaderText("Sucess : save sucess ");
            alert.setContentText("Product added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            clrAll();
        }
    }

    public boolean isNotNull() {
        boolean isNotNull;
        if (tfProductName.getText().trim().isEmpty()
                && cbSupplier.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("error");
            alert.setHeaderText("ERROR : NULL FOUND");
            alert.setContentText("Please fill all required field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            isNotNull = false;

        } else {
            isNotNull = true;
        }
        return isNotNull;
    }

    private void clrAll() {
        tfProductName.clear();
        taProductDescription.clear();
    }

    @FXML
    private void btnUpdateOnAction(ActionEvent event) {
        if (isNotNull()) {
            currentProduct.productName = tfProductName.getText();
            currentProduct.description = taProductDescription.getText();
            currentProduct.supplierId = supplierId;
            currentProduct.brandId = brandId;
            currentProduct.catagoryId = categoryId;

            currentProduct.lastModifiedBy = usrId;

            currentProductGetway.update(currentProduct);

        }
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) btnUpdate.getScene().getWindow();
        stage.close();
        refreshProductList(event);
    }

    public void showDetails() {
        currentProduct.id = id;
        currentProductGetway.selectedView(currentProduct);
        tfProductName.setText(currentProduct.productName);
        taProductDescription.setText(currentProduct.description);
        supplierId = currentProduct.supplierId;
        brandId = currentProduct.brandId;
        categoryId = currentProduct.catagoryId;

        cbSupplier.setValue(currentProduct.supplierName);
        cmbBrand.setValue(currentProduct.brandName);
        cmbCatagory.setValue(currentProduct.catagoryName);
    }

    public void addProductStage(Stage stage) {
        EffectUtility.makeDraggable(stage, apContent);
    }

    @FXML
    private void cbSupplierOnClicked(MouseEvent event) {
        cbSupplier.getSelectionModel().clearSelection();
        cbSupplier.getItems().clear();
        cmbBrand.getSelectionModel().clearSelection();
        cmbBrand.getItems().clear();
        cmbBrand.getItems().removeAll();
        try {
            pst = con.prepareStatement("select supplier_name from " + db + ".Supplier");
            rs = pst.executeQuery();
            while (rs.next()) {
                cbSupplier.getItems().addAll(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void cbSupplierOnAction(ActionEvent event) {
        cbSupplier.getSelectionModel().getSelectedItem();
        try {
            pst = con.prepareStatement("select id from " + db + ".Supplier where supplier_name=?");
            pst.setString(1, cbSupplier.getSelectionModel().getSelectedItem());
            rs = pst.executeQuery();
            while (rs.next()) {
                supplierId = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void cmbBrandOnClick(Event event) {
        cmbBrand.getItems().clear();
        cmbCatagory.getItems().clear();
        cmbCatagory.getItems().removeAll();
        cmbCatagory.setPromptText(null);
        try {
            pst = con.prepareStatement("select brand_name from " + db + ".Brands where supplier_id=?");
            pst.setString(1, supplierId);
            rs = pst.executeQuery();
            while (rs.next()) {
                cmbBrand.getItems().addAll(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void cmbCatagoryOnClick(Event event) {
        cmbCatagory.getItems().clear();
        try {
            pst = con.prepareStatement("select category_name from " + db + ".Category");
            rs = pst.executeQuery();
            while (rs.next()) {
                cmbCatagory.getItems().addAll(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void cmbBrandOnAction(ActionEvent event) {
        cmbBrand.getSelectionModel().getSelectedItem();
        try {
            pst = con.prepareStatement("select id from " + db + ".Brands where brand_name=? and supplier_id=?");
            pst.setString(1, cmbBrand.getSelectionModel().getSelectedItem());
            pst.setString(2, supplierId);
            rs = pst.executeQuery();
            while (rs.next()) {
                brandId = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void cmbCatagoryOnAction(ActionEvent actionEvent) {
        cmbCatagory.getSelectionModel().getSelectedItem();
        try {
            pst = con.prepareStatement("select id from " + db + ".Category where category_name =?");
            pst.setString(1, cmbCatagory.getSelectionModel().getSelectedItem());
            rs = pst.executeQuery();
            while (rs.next()) {
                categoryId = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void btnAddSupplierOnAction(ActionEvent actionEvent) {
        AddSupplierController addSupplyerController = new AddSupplierController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddSupplier.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddSupplierController supplyerController = fxmlLoader.getController();
            media.setId(usrId);
            supplyerController.setMedia(media);
            supplyerController.lblCaption.setText("Add Supplier");
            supplyerController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            supplyerController.addSupplierStage(nStage);
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void btnAddBrandOnAction(ActionEvent actionEvent) {
        AddBrandController addSupplyerController = new AddBrandController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddBrand.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddBrandController supplyerController = fxmlLoader.getController();
            media.setId(usrId);
            supplyerController.setMedia(media);
            supplyerController.lblHeader.setText("Add Brand");
            supplyerController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void btnAddCatagoryOnAction(ActionEvent actionEvent) {
        AddCatagoryController addCatagoryController = new AddCatagoryController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddCategory.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddCatagoryController catagoryController = fxmlLoader.getController();
            media.setId(usrId);
            catagoryController.setMedia(media);
            catagoryController.lblHeaderContent.setText("Add Item");
            catagoryController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void refreshProductList(ActionEvent actionEvent) {
        try {
            ViewProductController asc = new ViewProductController();
            FXMLLoader fXMLLoader = new FXMLLoader();
            fXMLLoader.load(getClass().getResource("/view/application/stock/ViewProduct.fxml").openStream());
            ViewProductController viewProductController = fXMLLoader.getController();
            viewProductController.btnRefreshOnACtion(actionEvent);
        } catch (Exception ex) {
            Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void btnAttachImageOnAction(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG (Joint Photographic Group)", "*.jpg"),
                new FileChooser.ExtensionFilter("JPEG (Joint Photographic Experts Group)", "*.jpeg"),
                new FileChooser.ExtensionFilter("PNG (Portable Network Graphics)", "*.png")
        );

        fileChooser.setTitle("Choose Product Image");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            System.out.println(file);
            bufferedImage = ImageIO.read(file);
            image = SwingFXUtils.toFXImage(bufferedImage, null);
            recProductImage.setFill(new ImagePattern(image));
            imagePath = file.getAbsolutePath();
        }
    }

//    private void refreshProductsList()
//    {
//        currentProduct.currentProductList.clear();
//        tblViewProduct.setItems(currentProduct.currentProductList);
////        tblClmProductId.setCellValueFactory(new PropertyValueFactory<>("productId"));
//        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
//        tblClmProductdescription.setCellValueFactory(new PropertyValueFactory<>("description"));
//        tblClmProductSupplier.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
//        tblClmProductBrand.setCellValueFactory(new PropertyValueFactory<>("brand"));
//        tblClmProductCatagory.setCellValueFactory(new PropertyValueFactory<>("catagory"));
//        currentProductGetway.view(currentProduct);
//    }
}
