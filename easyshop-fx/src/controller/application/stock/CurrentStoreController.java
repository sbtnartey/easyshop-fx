/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import dao.StockLevelBLL;
import entities.StockLevel;
import gateway.StockLevelGateway;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.userNameMedia;
import list.ListStockLevel;
import controller.application.SettingsController;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author rifat
 */
public class CurrentStoreController implements Initializable
{

    StockLevel stockLevel = new StockLevel();
    StockLevelGateway stockLevelGateway = new StockLevelGateway();
    StockLevelBLL stockLevelBLL = new StockLevelBLL();

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    private String usrId;

    private userNameMedia media;
    @FXML
    public StackPane spProductContent;
    @FXML
    private TextField tfSearch;
    @FXML
    private TextField tfSearchProduct;
    @FXML
    private Button btnAddNew;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnDelete;
    @FXML
    private TableView<ListStockLevel> tblViewCurrentStore;
    @FXML
    private TableColumn<Object, Object> tblClmId;
    @FXML
    private TableColumn<Object, Object> tblClmProductId;
    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private TableColumn<Object, Object> tblClmProductquantity;
    @FXML
    private TableColumn<Object, Object> tblClmProductUnitPrice;
    @FXML
    private TableColumn<Object, Object> tblClmProductSellPrice;
    @FXML
    private MenuItem miSellSelected;

    String suplyerId;
    String suplyerName;
    String brandId;
    String brandName;
    String catagoryId;
    String catagoryName;
    String rmaID;
    String rmaName;

    SQL sql = new SQL();
    @FXML
    private Button btnRefresh;
    @FXML
    public AnchorPane apCombobox;

    public userNameMedia getMedia()
    {
        return media;
    }

    public void setMedia(userNameMedia media)
    {
        usrId = media.getId();
        this.media = media;
    }

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    private Executor executor;
    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        try
        {
            executor = Executors.newCachedThreadPool(runnable ->{
                Thread thread = new Thread(runnable);
                thread.setDaemon(true);
                
                return thread;
            });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @FXML
    private void tfSearchOnKeyRelese(KeyEvent event)
    {
        stockLevel.productId = tfSearch.getText();
        stockLevel.productName = tfSearch.getText();
        
        viewDetails();
        stockLevelGateway.searchView(stockLevel);

    }


    @FXML
    private void btnAddNewOnAction(ActionEvent event)
    {
        AddStockLevelController apc = new AddStockLevelController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddStockLevel.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddStockLevelController addStockLevelController = fxmlLoader.getController();
            media.setId(usrId);
            addStockLevelController.setNameMedia(media);
            addStockLevelController.lblHeader.setText("Add Stock Items");
            //addStockLevelController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
//            addProductController.addSupplyerStage(nStage);
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    

    @FXML
    private void btnDeleteOnAction(ActionEvent event)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Confirm");
        alert.setContentText("Are you sure to delete this item \n to Confirm click ok");
        alert.initStyle(StageStyle.UNDECORATED);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK)
        {
            String item = tblViewCurrentStore.getSelectionModel().getSelectedItem().getId();
            System.out.println("Product id" + item);
            stockLevel.id = item;
            stockLevelBLL.delete(stockLevel);
            btnRefreshOnACtion(event);
        }

    }

    @FXML
    private void tblViewCurrentStoreOnClick(MouseEvent event
    )
    {
        if (event.getClickCount() == 2)
        {
            if (!tblViewCurrentStore.getSelectionModel().isEmpty())
            {
                viewSelected();
            } else
            {
                System.out.println("EMPTY SELECTION");
            }
        } else
        {
            tblViewCurrentStore.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event)
                {
                    tblViewCurrentStore.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                }
            });

        }
    }

    public void viewDetails()
    {
        
        Task<List<ListStockLevel>> task = new Task<List<ListStockLevel>>()
        {
            @Override
            protected List<ListStockLevel> call() throws Exception
            {
                    stockLevelGateway.viewFistTen(stockLevel);
                    return stockLevel.stockLevelDetails;
            }
        };
        
        task.setOnFailed(e -> task.getException().printStackTrace());
        task.setOnSucceeded(e -> tblViewCurrentStore.setItems((ObservableList<ListStockLevel>) task.getValue()));
        
        executor.execute(task);
        tblViewCurrentStore.setItems(stockLevel.stockLevelDetails);
        //tblClmProductId.setCellValueFactory(new PropertyValueFactory<>("productId"));
        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmProductquantity.setCellValueFactory(new PropertyValueFactory<>("quantityAvailable"));
        tblClmProductUnitPrice.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        tblClmProductSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellingPrice"));
    }

    private void viewSelected()
    {
        AddStockLevelController apc = new AddStockLevelController();
        userNameMedia userMedia = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddStockLevel.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddStockLevelController addStockLevelController = fxmlLoader.getController();
            userMedia.setId(usrId);
            addStockLevelController.setNameMedia(userMedia);
            addStockLevelController.lblHeader.setText("Stock Item Details");
            addStockLevelController.btnUpdate.setVisible(true);
            addStockLevelController.btnSave.setVisible(false);
            addStockLevelController.id = tblViewCurrentStore.getSelectionModel().getSelectedItem().getId();
            addStockLevelController.showDetails();
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

//    @FXML
//    private void miSellSelectedOnAction(ActionEvent event)
//    {
//        if (tblViewCurrentStore.getSelectionModel().getSelectedItem() != null)
//        {
//            String item = tblViewCurrentStore.getSelectionModel().getSelectedItem().getProductId();
//            NewSellController acc = new NewSellController();
//            userNameMedia media = new userNameMedia();
//            FXMLLoader fXMLLoader = new FXMLLoader();
//            fXMLLoader.setLocation(getClass().getResource("/view/application/sell/NewSell.fxml"));
//            try
//            {
//                fXMLLoader.load();
//                Parent parent = fXMLLoader.getRoot();
//                Scene scene = new Scene(parent);
//                scene.setFill(new Color(0, 0, 0, 0));
//                NewSellController newSellController = fXMLLoader.getController();
//                newSellController.tfProductId.setText(item);
//                newSellController.tfProductIdOnAction(event);
//                media.setId(usrId);
//                newSellController.setNameMedia(media);
//                newSellController.genarateSellID();
//                Stage stage = new Stage();
//                stage.setScene(scene);
//                stage.initModality(Modality.APPLICATION_MODAL);
//                stage.initStyle(StageStyle.TRANSPARENT);
//                stage.show();
//            } catch (IOException ex)
//            {
//                Logger.getLogger(ViewCustomerController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else
//        {
//
//        }
//    }

    public void settingPermission()
    {
        con = dbCon.getConnection();
        try
        {
            pst = con.prepareStatement("select * from " + db + ".UserPermission where id=?");
            pst.setString(1, usrId);
            rs = pst.executeQuery();
            while (rs.next())
            {
                if (rs.getInt(8) == 0)
                {
                    btnUpdate.setDisable(true);
                    btnDelete.setDisable(true);
                }
                if (rs.getInt(3) == 0)
                {
                    btnAddNew.setDisable(true);
                }
                if (rs.getInt("SellProduct") == 0)
                {
                    miSellSelected.setDisable(true);
                } else
                {

                }
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(SettingsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnRefreshOnACtion(ActionEvent event)
    {
        
        tfSearch.clear();
        viewDetails();

    }

    @FXML
    private void tblViewCurrentStoreOnScroll(ScrollEvent event)
    {
        if (event.isInertia())
        {
            System.out.println("ALT DOWN");
        } else
        {
            System.out.println("Noting");
        }
    }

}
