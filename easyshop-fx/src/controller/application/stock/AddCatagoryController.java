/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import dao.CategoryBLL;
import db.DBConnection;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import db.SQL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.userNameMedia;
import entities.Category;
import gateway.CategoryGetway;
import db.DBProperties;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import list.ListCategory;

public class AddCatagoryController implements Initializable
{

    private String userId;
    private String brandId;
    private String brnadName;
    public String supplyerId;
    public String supplyerName;
    public String catagoryId;

    Category catagory = new Category();
    CategoryGetway catagoryGetway = new CategoryGetway();
    CategoryBLL catagoryBLL = new CategoryBLL();
    SQL sql = new SQL();

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    private userNameMedia media;
    @FXML
    private ComboBox<String> cbBrandName;
    @FXML
    private TextField tfCatagoryName;
    @FXML
    private TextArea taCatagoryDescription;
    @FXML
    public Button btnSave;
    @FXML
    private ComboBox<String> cbSupplierName;
    @FXML
    private Button btnAddSupplier;
    @FXML
    private Button btnAddBrand;
    @FXML
    public Button btnUpdate;
    @FXML
    public Label lblHeaderContent;
    @FXML
    public Button btnClose;
    
    @FXML
    private TableView<ListCategory> tblCatagory;
    @FXML
    private TableColumn<Object, Object> clmCatagoryId;
    @FXML
    private TableColumn<Object, Object> clmCatagoryName;
    @FXML
    private TableColumn<Object, Object> clmCatagoryDescription;

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;

    public userNameMedia getMedia()
    {
        return media;
    }

    public void setMedia(userNameMedia media)
    {
        userId = media.getId();
        this.media = media;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {

    }

    @FXML
    private void btnSaveCatagory(ActionEvent event)
    {
        if (isNotNull())
        {
            catagory.catagoryName = tfCatagoryName.getText().trim();
            catagory.catagoryDescription = taCatagoryDescription.getText().trim();
            catagory.createdBy = userId;
            catagoryBLL.save(catagory);
        }
    }

    @FXML
    private void btnUpdateOnAction(ActionEvent event)
    {
        try
        {
            if (isNotNull())
            {
                catagory.id = catagoryId;
                catagory.catagoryName = tfCatagoryName.getText().trim();
                catagory.catagoryDescription = taCatagoryDescription.getText().trim();
                catagoryBLL.update(catagory);
                btnCloseOnAction(event);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void btnCloseOnAction(ActionEvent actionEvent)
    {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
        refreshCategoriesList(actionEvent);
    }

    public boolean isNotNull()
    {
        boolean isNotNull = false;
        if (tfCatagoryName.getText().trim().isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("error");
            alert.setHeaderText("Error : null found ");
            alert.setContentText("Please fill required field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            return isNotNull;
        } else
        {
            return isNotNull = true;
        }
        
    }

    public void showDetails()
    {
        catagory.id = catagoryId;
        catagoryGetway.selectedView(catagory);
        tfCatagoryName.setText(catagory.catagoryName);
        taCatagoryDescription.setText(catagory.catagoryDescription);
    }
    
    private void viewCategoriesList()
    {
        catagory.catagoryDetails.clear();
        tblCatagory.setItems(catagory.catagoryDetails);
        clmCatagoryId.setCellValueFactory(new PropertyValueFactory<>("id"));
        clmCatagoryName.setCellValueFactory(new PropertyValueFactory<>("catagoryName"));
        clmCatagoryDescription.setCellValueFactory(new PropertyValueFactory<>("catagoryDescription"));
        catagoryGetway.view(catagory);
    }

    private void refreshCategoriesList(ActionEvent event)
    {
        try
        {
            ViewCatagoryController vsc = new ViewCatagoryController();
            FXMLLoader fXMLLoader = new FXMLLoader();
            fXMLLoader.load(getClass().getResource("/view/application/stock/ViewCategory.fxml").openStream());
            ViewCatagoryController viewCategoryController = fXMLLoader.getController();
            viewCategoryController.btnRefreshOnAction(event);
        } catch (IOException ex)
        {
            Logger.getLogger(AddInvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
