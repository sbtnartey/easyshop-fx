/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import dao.SupplierBLL;
import gateway.SupplierGetway;
import custom.History;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import db.SQL;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import list.ListSupplier;
import entities.Supplier;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import media.userNameMedia;

public class ViewSupplierController implements Initializable
{

    @FXML
    public AnchorPane acContent;
    SQL sql = new SQL();
    Supplier supplier = new Supplier();
    SupplierGetway supplierGetway = new SupplierGetway();
    SupplierBLL supplierBLL = new SupplierBLL();
    History history = new History();

    private String usrId;
    private String creatorName;
    private String creatorId;
    private String supplyerId;
    private String userName;

    private userNameMedia media;

    @FXML
    private TableView<ListSupplier> tblSupplier;
    @FXML
    private TableColumn<Object, Object> clmSupplierId;
    @FXML
    private TableColumn<Object, Object> clmSupplierName;
    @FXML
    private TableColumn<Object, Object> clmSupplierPhoneNumber;
    @FXML
    private TableColumn<Object, Object> clmSupplierAddress;
    @FXML
    private TableColumn<Object, Object> clmSupplierJoining;
    @FXML
    private TableColumn<Object, Object> clmSupplierDescription;

    private final static int dataSize = 10_023;
    private final static int rowsPerPage = 1000;
    @FXML
    private Button btnAdditems;
    @FXML
    private Button btnUpdate;
    @FXML
    private TextField tfSearch;
    private Text text;
    @FXML
    private MenuItem mbSearch;
    @FXML
    private Button btnRefresh;
    private Executor executor;

    public userNameMedia getMedia()
    {
        return media;
    }

    public void setMedia(userNameMedia media)
    {
        usrId = media.getId();
        this.media = media;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        try
        {
            executor = Executors.newCachedThreadPool(runnable -> {
               Thread t = new Thread(runnable);
               t.setDaemon(true);
               return t;
            });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @FXML
    private void tblSupplierOnClick(MouseEvent event)
    {
        int click = event.getClickCount();
        if (click == 2)
        {
            viewDetails();
        }

    }

    @FXML
    private void tblSupplierOnKeyPress(KeyEvent event)
    {

    }

    @FXML
    public void tfSearchOnType(Event event)
    {
        supplier.supplierDetails.removeAll();
        supplier.supplierName = tfSearch.getText().trim();

        tblSupplier.setItems(supplier.supplierDetails);
        clmSupplierId.setCellValueFactory(new PropertyValueFactory<>("supplierId"));
        clmSupplierName.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
        clmSupplierPhoneNumber.setCellValueFactory(new PropertyValueFactory<>("supplierPhoneNumber"));
        clmSupplierAddress.setCellValueFactory(new PropertyValueFactory<>("supplierAddress"));
//        clmSupplierDescription.setCellValueFactory(new PropertyValueFactory<>("supplierDescription"));
        clmSupplierJoining.setCellValueFactory(new PropertyValueFactory<>("supplyingSince"));
        supplierGetway.searchView(supplier);
    }

    public void showDetails()
    {
        Task<List<ListSupplier>> task = new Task<List<ListSupplier>>()
        {
            @Override
            protected List<ListSupplier> call() throws Exception
            {
                supplierGetway.view(supplier);
                return supplier.supplierDetails;
            }
        };
        
        task.setOnFailed(e -> task.getException().printStackTrace());
        task.setOnSucceeded(e-> tblSupplier.setItems((ObservableList<ListSupplier>) task.getValue()));
        
        executor.execute(task);
        tblSupplier.setItems(supplier.supplierDetails);
        clmSupplierId.setCellValueFactory(new PropertyValueFactory<>("supplierId"));
        clmSupplierName.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
        clmSupplierPhoneNumber.setCellValueFactory(new PropertyValueFactory<>("supplierPhoneNumber"));
        clmSupplierAddress.setCellValueFactory(new PropertyValueFactory<>("supplierAddress"));
        clmSupplierJoining.setCellValueFactory(new PropertyValueFactory<>("supplyingSince"));

    }

    @FXML
    private void btnAdditemsOnAction(ActionEvent event)
    {
        AddSupplierController addSupplierController = new AddSupplierController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddSupplier.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddSupplierController supplyerController = fxmlLoader.getController();
            media.setId(usrId);
            supplyerController.setMedia(media);
            supplyerController.lblCaption.setText("Add Item");
            supplyerController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            supplyerController.addSupplierStage(nStage);
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        tfSearchOnType(event);

    }

    @FXML
    private void btnUpdateOnAction(Event event)
    {
        if (tblSupplier.getSelectionModel().getSelectedItem() != null)
        {
            viewDetails();
        } else
        {
            System.out.println("EMPTY SELECTION");
        }

    }

    private void viewDetails()
    {
        if (!tblSupplier.getSelectionModel().isEmpty())
        {
            ListSupplier selectedSupplier = tblSupplier.getSelectionModel().getSelectedItem();
            String items = selectedSupplier.getSupplierId();
            if (!items.equals(null))
            {
                AddSupplierController addSupplierController = new AddSupplierController();
                userNameMedia media = new userNameMedia();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddSupplier.fxml"));
                try
                {
                    fxmlLoader.load();
                    Parent parent = fxmlLoader.getRoot();
                    Scene scene = new Scene(parent);
                    scene.setFill(new Color(0, 0, 0, 0));
                    AddSupplierController supplyerController = fxmlLoader.getController();
                    media.setId(usrId);
                    supplyerController.setMedia(media);
                    supplyerController.lblCaption.setText("Supplier Details");
                    supplyerController.btnUpdate.setVisible(true);
                    supplyerController.btnSave.setVisible(false);
                    supplyerController.supplierId = selectedSupplier.getSupplierId();
                    supplyerController.showDetails();
                    Stage nStage = new Stage();
                    nStage.setScene(scene);
                    nStage.initModality(Modality.APPLICATION_MODAL);
                    nStage.initStyle(StageStyle.TRANSPARENT);
                    nStage.show();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        } else
        {
            System.out.println("empty Selection");
        }

    }

    @FXML
    private void mbView(ActionEvent event)
    {
        btnUpdateOnAction(event);
    }

    @FXML
    private void mbViewHistory(ActionEvent event)
    {
    }

    @FXML
    private void mbAddNew(ActionEvent event)
    {
        btnAdditemsOnAction(event);
    }

    @FXML
    private void mbDeleteItem(ActionEvent event)
    {
        System.out.println("clicked to delete");
        acContent.setOpacity(0.5);
        ListSupplier selectedSupplier = tblSupplier.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Login Now");
        alert.setHeaderText("Confirm");
        alert.setContentText("Are you sure to delete this item \n to Confirm click ok");
        alert.initStyle(StageStyle.UNDECORATED);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK)
        {
            supplier.id = selectedSupplier.getSupplierId();
            System.out.println(supplier.id + "On hear");
            supplierBLL.delete(supplier);
            tblSupplier.getItems().clear();
            showDetails();
        } else
        {

        }

    }

    @FXML
    private void mbEdit(ActionEvent event)
    {
        btnUpdateOnAction(event);
        tfSearchOnType(event);
    }

    @FXML
    private void mbSearch(ActionEvent event)
    {
        tblSupplier.getSelectionModel().clearSelection();
        tfSearch.requestFocus();

    }

    @FXML
    public void btnDeleteOnAction(ActionEvent event)
    {
        mbDeleteItem(event);
    }

    @FXML
    public void btnRefreshOnAction(ActionEvent event)
    {
        showDetails();
    }

}
