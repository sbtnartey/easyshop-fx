/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import entities.Stock;
import gateway.StocksGateway;
import db.DBConnection;
import db.DBProperties;
import db.SQL;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.userNameMedia;

public class AddInvoiceController implements Initializable
{

    public Button btnAddProduct;
    private String usrId;
    private userNameMedia nameMedia;
    public String invoiceId;
    
    Stock stock = new Stock();
   
    StocksGateway stocksGateway = new StocksGateway();
    SQL sql = new SQL();

    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    PreparedStatement pst;
    ResultSet rs;

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    @FXML
    private Button btnClose;
    @FXML
    private TextField tfInvoiceNumber;
    @FXML
    private TextField tfTotalCost;
    @FXML
    private TextField tfTotalItems;
    @FXML
    public Button btnSave;

    public String id;
  
    @FXML
    public Button btnUpdate;
    @FXML
    public Label lblHeader;
    @FXML
    public DatePicker datePicker;
    
    String pattern = "dd/MM/yyyy";


    public userNameMedia getNameMedia()
    {
        return nameMedia;
    }

    public void setNameMedia(userNameMedia nameMedia)
    {
        usrId = nameMedia.getId();
        this.nameMedia = nameMedia;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event)
    {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
        refreshStockList(event);
    }

    @FXML
    private void btnSaveOnAction(ActionEvent event)
    {
        System.out.println("Presesd");
        if (isNotNull())
        {
                stock.invoiceNumber = tfInvoiceNumber.getText().trim();
                stock.totalItems = tfTotalItems.getText().trim();
                stock.invoiceTotalCost = tfTotalCost.getText().trim();
                stock.invoiceDate = datePicker.getValue().toString();
                
                stocksGateway.save(stock);
                  
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("error");
                alert.setHeaderText("Sucess : save sucess ");
                alert.setContentText("Invoice added successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
            }
        
    }

    @FXML
    private void btnAddNewOnAction(ActionEvent event)
    {
        
    }

    private boolean isNotNull()
    {
        boolean insNotNull = false;
        if (tfInvoiceNumber.getText().isEmpty() || datePicker.getValue() == null)
        {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("error");
            alert.setHeaderText("ERROR : NULL FOUND");
            alert.setContentText("Please fill all require field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            insNotNull = false;
        } else
        {
            insNotNull = true;
        }
        return insNotNull;
    }

    @FXML
    private void btnUpdateOnAction(ActionEvent event)
    {
        if (isNotNull())
        {
            stock.invoiceNumber = tfInvoiceNumber.getText().trim();
            stock.totalItems = tfTotalItems.getText().trim();
            stock.invoiceDate = datePicker.getValue().toString();
            stock.invoiceTotalCost = tfTotalCost.getText().trim();
            stock.lastModifiedBy = usrId;
            stock.id = id;
            
            stocksGateway.update(stock);
        }

    }

    public void viewSelected()
    {
        stock.id = id;
        stocksGateway.selectedView(stock);
        tfInvoiceNumber.setText(stock.invoiceNumber);
        tfTotalCost.setText(stock.invoiceTotalCost);
        tfTotalItems.setText(stock.totalItems);
        datePicker.setValue(sql.stringToLocalDate(stock.invoiceDate));
        
    }

    
    public void refreshStockList(ActionEvent event)
    {
        try
        {
            ViewInvoiceController vsc = new ViewInvoiceController();
            FXMLLoader fXMLLoader = new FXMLLoader();
            fXMLLoader.load(getClass().getResource("/view/application/stock/ViewInvoice.fxml").openStream());
            vsc = fXMLLoader.getController();
            vsc.btnRefreshOnAction(event);
        } catch (IOException ex)
        {
            Logger.getLogger(AddInvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
