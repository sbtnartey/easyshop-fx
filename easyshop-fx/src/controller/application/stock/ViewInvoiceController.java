/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.stock;

import dao.StockBLL;
import db.DBConnection;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import db.SQL;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import media.userNameMedia;
import entities.Stock;
import gateway.StocksGateway;
import list.ListStock;
import java.util.Optional;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author rifat
 */
public class ViewInvoiceController implements Initializable
{

    private String usrId;
    private String usrName;

    SQL sql = new SQL();
    Stock stock = new Stock();
    StocksGateway stockGateway = new StocksGateway();
    StockBLL stockBLL = new StockBLL();

    private userNameMedia media;

    DBConnection dbCon = new DBConnection();
    Connection con;
    PreparedStatement pst;
    ResultSet rs;

    @FXML
    private TableView<ListStock> tblStock;
    @FXML
    private TableColumn<Object, Object> tblClmId;
    @FXML
    private TableColumn<Object, Object> tblClmInvoiceNumber;
    @FXML
    private TableColumn<Object, Object> tblClmInvoiceDate;
    @FXML
    private TableColumn<Object, Object> tblClmInvoiceTotalCost;
    @FXML
    private TableColumn<Object, Object> tblClmNoOfItems;
    @FXML
    private TextField tfSearch;
    @FXML
    private Button btnAddStock;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnRefresh;
    
    @FXML
    public AnchorPane apCombobox;

    public userNameMedia getMedia()
    {
        return media;
    }

    public void setMedia(userNameMedia media)
    {
        usrId = media.getId();
        usrName = media.getUsrName();
        this.media = media;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {

    }

    @FXML
    public void tblBrandOnClick(MouseEvent event)
    {
        int click = event.getClickCount();
        if (click == 2)
        {
            viewDetails();
        }

    }

    @FXML
    private void tfSearchOnKeyPress(Event event)
    {
        System.out.println(usrId);
        stock.stockDetailsList.clear();
        stock.invoiceNumber = tfSearch.getText();
        tblStock.setItems(stock.stockDetailsList);
        tblClmId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tblClmInvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("invoiceNumber"));
        tblClmInvoiceDate.setCellValueFactory(new PropertyValueFactory<>("invoiceDate"));
        tblClmInvoiceTotalCost.setCellValueFactory(new PropertyValueFactory<>("invoiceTotalCost"));
        tblClmNoOfItems.setCellValueFactory(new PropertyValueFactory<>("totalItems"));
//        tblClmInvoiceCreator.setCellValueFactory(new PropertyValueFactory<>("createdBy"));
        stockGateway.searchView(stock);

    }

    @FXML
    private void btnAddNewOnAction(ActionEvent event)
    {
        AddInvoiceController asc = new AddInvoiceController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddInvoice.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddInvoiceController addStockController = fxmlLoader.getController();
            media.setId(usrId);
            addStockController.setNameMedia(media);
            addStockController.lblHeader.setText("Add Stock");
            addStockController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        tfSearchOnAction(event);
    }

    @FXML
    private void tfSearchOnKeyRelease()
    {
        stock.invoiceNumber = tfSearch.getText().trim();
        stockGateway.searchView(stock);
    }

    @FXML
    private void btnUpdateOnAction(Event event)
    {
        if (tblStock.getSelectionModel().getSelectedItem() != null)
        {
            viewDetails();
        } else
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("error");
            alert.setHeaderText("ERROR");
            alert.setContentText("Please select Invoice to update");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        }

    }

    @FXML
    private void btnDeleteOnAction(Event event)
    {
        if (tblStock.getSelectionModel().getSelectedItem() != null)
        {
            ListStock selectedStock = tblStock.getSelectionModel().getSelectedItem();
            //check if selected invoice has stocklevel
            
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirm Now");
            alert.setHeaderText("Confirm");
            alert.setContentText("Are you sure to delete this item \n to Confirm click ok");
            alert.initStyle(StageStyle.UNDECORATED);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK)
            {
                stock.id = selectedStock.getId();
                stockBLL.delete(stock);
                tblStock.getItems().clear();
                showDetails();
            }
        }else
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("error");
            alert.setHeaderText("ERROR");
            alert.setContentText("Please select Invoice to delete");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        }

    }

    public void showDetails()
    {
        tblStock.setItems(stock.stockDetailsList);
        tblClmId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tblClmInvoiceNumber.setCellValueFactory(new PropertyValueFactory<>("invoiceNumber"));
        tblClmInvoiceDate.setCellValueFactory(new PropertyValueFactory<>("invoiceDate"));
        tblClmInvoiceTotalCost.setCellValueFactory(new PropertyValueFactory<>("invoiceTotalCost"));
        tblClmNoOfItems.setCellValueFactory(new PropertyValueFactory<>("totalItems"));
//        tblClmInvoiceCreator.setCellValueFactory(new PropertyValueFactory<>("createdBy"));
        stockGateway.view(stock);
    }

    Callback<TableColumn<Object, Object>, TableCell<Object, Object>> callback = new Callback<TableColumn<Object, Object>, TableCell<Object, Object>>()
    {
        @Override
        public TableCell<Object, Object> call(TableColumn<Object, Object> param)
        {
            final TableCell cell = new TableCell()
            {

                public void updateItem(Object item, boolean empty)
                {

                    super.updateItem(item, empty);
                    Text text = new Text();
                    if (!isEmpty())
                    {
                        text = new Text(item.toString());
                        text.setWrappingWidth(200);
                        text.setFill(Color.BLACK);
                        setGraphic(text);
                    } else if (isEmpty())
                    {
                        text.setText(null);
                        setGraphic(null);
                    }
                }
            };
            return cell;
        }
    };

    @FXML
    public void tfSearchOnAction(ActionEvent event)
    {

    }

    private void viewDetails()
    {
        if (!tblStock.getSelectionModel().isEmpty())
        {
            ListStock selectedStock = tblStock.getSelectionModel().getSelectedItem();
            String items = selectedStock.getId();
            if (!items.equals(null))
            {
                AddInvoiceController abc = new AddInvoiceController();
                userNameMedia media = new userNameMedia();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddInvoice.fxml"));
                try
                {
                    fxmlLoader.load();
                    Parent parent = fxmlLoader.getRoot();
                    Scene scene = new Scene(parent);
                    scene.setFill(new Color(0, 0, 0, 0));
                    AddInvoiceController addStockController = fxmlLoader.getController();
                    media.setId(usrId);
                    addStockController.setNameMedia(media);
                    addStockController.lblHeader.setText("Invoice Details");
                    addStockController.btnUpdate.setVisible(true);
                    addStockController.id = selectedStock.id;
                    addStockController.viewSelected();
                    Stage nStage = new Stage();
                    nStage.setScene(scene);
                    nStage.initModality(Modality.APPLICATION_MODAL);
                    nStage.initStyle(StageStyle.TRANSPARENT);
                    nStage.show();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

    }

    @FXML
    private void btnAddStockOnAction(ActionEvent event)
    {
        AddInvoiceController asc = new AddInvoiceController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/view/application/stock/AddInvoice.fxml"));
        try
        {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddInvoiceController addStockController = fxmlLoader.getController();
            media.setId(usrId);
            addStockController.setNameMedia(media);
            addStockController.lblHeader.setText("Add Invoice");
            addStockController.btnUpdate.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        tfSearchOnAction(event);
    }

    @FXML
    private void miSearch(ActionEvent event)
    {
        tblStock.getSelectionModel().clearSelection();
        tfSearch.requestFocus();
    }

    @FXML
    private void miUpdate(Event event)
    {
        btnUpdateOnAction(event);
    }

    @FXML
    private void miDelete(ActionEvent event)
    {
        btnDeleteOnAction(event);
    }

    @FXML
    private void miAddNew(ActionEvent event)
    {
        btnAddStockOnAction(event);
    }

    @FXML
    private void miView(ActionEvent event)
    {
        if (tblStock.getSelectionModel().getSelectedItem() != null)
        {
            btnUpdateOnAction(event);
        }
    }

    @FXML
    public  void btnRefreshOnAction(ActionEvent event)
    {
        stock.stockDetailsList.clear();
        showDetails();
    }
}
