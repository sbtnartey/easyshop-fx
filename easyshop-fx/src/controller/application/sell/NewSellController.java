/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.sell;

import dao.SellCartBLL;
import entities.CurrentProduct;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import gateway.CurrentProductGetway;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.userNameMedia;
import entities.Customer;
import entities.SellCart;
import entities.StockLevel;
import gateway.SellCartGetway;
import list.ListPreSell;
import list.ListProduct;
import controller.application.stock.AddInvoiceController;
import custom.CustomTf;
import custom.QuantityEditCell;
import custom.RandomIdGenarator;
import dao.SaleItemBLL;
import db.DBConnection;
import db.SQL;
import gateway.SaleItemGateway;
import gateway.StockLevelGateway;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Objects;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;

/**
 *
 * @author rifat
 */
public class NewSellController implements Initializable {

    public Button btnAddCustomer;
    userNameMedia nameMedia;

    String userId;
    String productId;
    String productName;
    String savedSale;
    String receiptNo;
    int quantity;
    int quanityAvailable;
    int oldQuantity;
    String totalItems;

    @FXML
    private MenuButton mbtnProduct;
    @FXML
    private TableView<ListProduct> tblProductSortView;
    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private TableColumn<Object, Object> tblClmCartProductName;
    @FXML
    private TextField tfSearchProduct;
    @FXML
    private Button btnClose;

    String customerId;
    @FXML
    public TextField tfProductId;
    @FXML
    private TableView<ListPreSell> tblSellPreList;
    @FXML
    private TableColumn<Object, Object> tblClmSellId;
    @FXML
    private TableColumn<Object, Object> tblClmProductId;
    @FXML
    private TableColumn<Object, Object> tblClmSellPrice;
    @FXML
    private TableColumn<Object, Object> tblClmCustomer;
    @FXML
    private TableColumn<Object, Object> tblClmSoledBy;
    @FXML
    private TableColumn<Object, Object> tblClmWarrentyVoidDate;
    @FXML
    private TableColumn<Object, Object> tblClmQuantity;
    @FXML
    private TableColumn<Object, Object> tblClmTotalPrice;
    @FXML
    private Label lblTotal;

    @FXML
    private Button btnAddToCart;
    @FXML
    private Button btnSell;

    @FXML
    private Button btnClearAll;
    @FXML
    private Button btnClearSelected;
    @FXML
    private Label lblSellId;
    @FXML
    private ComboBox<String> cbProducts;

    //shopping cart table 
    @FXML
    private TableView<ListPreSell> tblViewSellCart;
    @FXML
    private TableColumn<Object, String> tblClmCartQty;
    @FXML
    private TableColumn<Object, Object> tblClmCartProductId;
    @FXML
    private TableColumn<Object, Object> tblClmCartPrice;
    @FXML
    private TableColumn<Object, Object> tblClmCartQtyAvailable;


    public void setNameMedia(userNameMedia nameMedia) {
        userId = nameMedia.getId();
        sellCart.sellerID = userId;
        this.nameMedia = nameMedia;
    }

    Customer customer = new Customer();
    CurrentProduct product = new CurrentProduct();
    StockLevel currrentProduct = new StockLevel();

    StockLevelGateway stockLevelGateway = new StockLevelGateway();
    CurrentProductGetway currentProductGetway = new CurrentProductGetway();

    SellCart sellCart = new SellCart();
    SellCartGetway sellCartGerway = new SellCartGetway();

    SaleItemGateway saleItemGateway = new SaleItemGateway();

    SellCartBLL scbll = new SellCartBLL();
    SaleItemBLL saleItemBLL = new SaleItemBLL();
    CustomTf ctf = new CustomTf();

    SQL sql = new SQL();
    DBConnection db = new DBConnection();
    Connection con = db.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    ListPreSell listPreSell = new ListPreSell();

    ObservableList<ListPreSell> preList = FXCollections.observableArrayList();

    Callback<TableColumn<Object, String>, TableCell<Object, String>> cellFactory
            = (TableColumn<Object, String> param) -> new QuantityEditCell();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        preList.clear();
        sellCart.carts.clear();
        //tblViewSellCart.setEditable(true);

//        clearAll();
    }

    @FXML
    private void tblProductOnClick(MouseEvent event) {
        productId = tblViewSellCart.getSelectionModel().getSelectedItem().getProductId();
        sellCart.oldQuantity = tblViewSellCart.getSelectionModel().getSelectedItem().getOldQuantity();
        currrentProduct.productId = productId;
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
        refreshSalesList();
    }

    @FXML
    private void tfProductSearchOnKeyReleased(KeyEvent event) {
        sellCart.carts.clear();

        sellCart.productName = tfSearchProduct.getText().trim();
        tblViewSellCart.setItems(sellCart.carts);
        tblViewSellCart.setEditable(true);

        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmCartPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
        tblClmCartQtyAvailable.setCellValueFactory(new PropertyValueFactory<>("oldQuantity"));

        tblClmCartQty.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tblClmCartQty.setCellFactory(cellFactory);
        tblClmCartQty.setOnEditCommit(
                (TableColumn.CellEditEvent<Object, String> t)
                -> {
            ((ListPreSell) t.getTableView().getItems()
                    .get(t.getTablePosition().getRow()))
                    .setQuantity(t.getNewValue());

            quantity = Integer.parseInt(t.getNewValue());
        });

        sellCartGerway.searchProductForCart(sellCart);
    }

    @FXML
    private void btnAddToCartOnAction(ActionEvent event) {
        if (inNotNull()) {
            currrentProduct.productName = sql.getName(currrentProduct.productId, currrentProduct.productName, "Products");
            sellCart.quantity = String.valueOf(quantity);
            sellCart.sellPrice = tblViewSellCart.getSelectionModel().getSelectedItem().getSellPrice();
            double netCost = Double.valueOf(StringUtils.replace(sellCart.sellPrice, ",", "")) * Integer.valueOf(sellCart.quantity);
            String itemCost = String.valueOf(netCost);

            listPreSell.setId(currrentProduct.id);
            listPreSell.setProductId(currrentProduct.productId);
            listPreSell.setProductName(currrentProduct.productName);
            listPreSell.setCustomerId(customerId);
            listPreSell.setUnitPrice(currrentProduct.purchasePrice);
            listPreSell.setSellPrice(sellCart.sellPrice);
            listPreSell.setQuantity(sellCart.quantity);
            listPreSell.setOldQuantity(sellCart.oldQuantity);
            listPreSell.setTotalPrice(itemCost);

            addItem(listPreSell);
            viewAll();
            sumTotalCost();
            //clearAll();
        }

    }

    private void sumTotalCost() {
        tblSellPreList.getSelectionModel().selectFirst();
        float sum = 0;
        int items = tblSellPreList.getItems().size();

        int itemsInCart = 0;

        for (int i = 0; i < items; i++) {
            tblSellPreList.getSelectionModel().select(i);
            String selectedItem = tblSellPreList.getSelectionModel().getSelectedItem().getTotalPrice();
            int selectedQty = Integer.parseInt(tblSellPreList.getSelectionModel().getSelectedItem().getQuantity());
            float newFloat = Float.parseFloat(selectedItem);
            System.out.println("total price " + newFloat);
            sum += newFloat;
            itemsInCart += selectedQty;
        }
        String totalCost = String.valueOf(sum);
        lblTotal.setText(totalCost);
        System.out.println("Total:" + sum);
        totalItems = String.valueOf(itemsInCart);
        //lblTotalItems.setText(totalItem);
    }

    public void viewAll() {
        tblSellPreList.setItems(preList);
        tblClmCartProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tblClmSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
        tblClmTotalPrice.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));
    }

    @FXML
    private void btnSellOnAction(ActionEvent event) {
        if (!tblSellPreList.getItems().isEmpty()) {
            sellCart.sellId = lblSellId.getText();
            sellCart.totalItems = totalItems;
            sellCart.totalCost = lblTotal.getText();
            //create sale 
            savedSale = scbll.sell(sellCart);
            sellCart.Id = savedSale;
            System.out.println("saved sale id " + savedSale);
            int indexs = tblSellPreList.getItems().size();
            for (int i = 0; i < indexs; i++) {
                tblSellPreList.getSelectionModel().select(i);
//                sellCart.Id = tblSellPreList.getSelectionModel().getSelectedItem().getId();
                sellCart.productId = tblSellPreList.getSelectionModel().getSelectedItem().getProductId();
                sellCart.sellPrice = tblSellPreList.getSelectionModel().getSelectedItem().getSellPrice();
                sellCart.quantity = tblSellPreList.getSelectionModel().getSelectedItem().getQuantity();
                sellCart.oldQuantity = tblSellPreList.getSelectionModel().getSelectedItem().getOldQuantity();

                sellCart.totalPrice = tblSellPreList.getSelectionModel().getSelectedItem().getTotalPrice();
//                sellCart.warrentyVoidDate = tblSellPreList.getSelectionModel().getSelectedItem().getWarrentyVoidDate();
//                sellCart.sellerID = userId;

                if (null != savedSale || "" == savedSale) {
                    //save each sale item
                    saleItemBLL.save(sellCart);
                }
            }

            tblSellPreList.getItems().clear();
            btnCloseOnAction(event);
            toPrintReceipt(event);
            lblTotal.setText(null);
        } else {
            System.out.println("EMPTY");
        }

    }

    @FXML
    private void toPrintReceipt(ActionEvent event) {
        PrintReceiptController prc = new PrintReceiptController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fXMLLoader = new FXMLLoader();
        fXMLLoader.setLocation(getClass().getResource("/view/application/sell/PrintReceipt.fxml"));
        try {
            fXMLLoader.load();
            Parent parent = fXMLLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            PrintReceiptController printReceiptController = fXMLLoader.getController();
            media.setId(userId);
            printReceiptController.setNameMedia(media);
            printReceiptController.receiptNo = lblSellId.getText();
            printReceiptController.lblSellId.setText(lblSellId.getText());
            printReceiptController.saleId = sellCart.Id;
            printReceiptController.lblTotalCost.setText(lblTotal.getText());

            printReceiptController.viewDetails();

            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(ViewCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnClearAllOnAction(ActionEvent event) {

    }

    @FXML
    private void btnClearSelectedOnAction(ActionEvent event) {
        if (!tblSellPreList.getItems().isEmpty()) {
            System.out.println("Clicked");
            tblSellPreList.getItems().removeAll(tblSellPreList.getSelectionModel().getSelectedItems());
            sumTotalCost();
        } else {
            System.out.println("EMPTY");
        }

    }

    public void genarateSellID() {
        String id = RandomIdGenarator.randomstring();
        if (id.matches("001215")) {
            String nId = RandomIdGenarator.randomstring();
            lblSellId.setText(nId);
        } else {
            lblSellId.setText(id);
        }

    }

    public boolean inNotNull() {
        boolean isNotNull = false;
        System.out.println("checking if table for sell cart is selected");
        if (tblViewSellCart.getSelectionModel().getSelectedItem().getQuantity() != null) {
            System.out.println("table item is selected and quantity is " + quantity);
            isNotNull = true;

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("ERROR");
            alert.setContentText("Please fill all required field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        }

        System.out.println("is not null " + isNotNull);
        return isNotNull;
    }

    private void refreshSalesList() {
        try {
            ViewSellController vsc = new ViewSellController();
            FXMLLoader fXMLLoader = new FXMLLoader();
            fXMLLoader.load(getClass().getResource("/view/application/sell/ViewSell.fxml").openStream());
            ViewSellController viewSellController = fXMLLoader.getController();
            viewSellController.viewDetails();
        } catch (IOException ex) {
            Logger.getLogger(AddInvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void addItem(ListPreSell cartItem) {
        boolean newItem = true;
        try {

            int index = 0;
            for (ListPreSell item : preList) {
                if (Objects.equals(item.getProductId(), cartItem.getProductId())) {
                    newItem = false;
                    int oldQty = Integer.parseInt(item.getQuantity());
                    int newQty = Integer.parseInt(cartItem.getQuantity());
                    int sumQty = oldQty + newQty;
                    item.setQuantity(String.valueOf(sumQty));

                    preList.set(index, item);
                }
                index++;
            }
            if (newItem) {
                ListPreSell newLineItem = new ListPreSell(cartItem.getId(), cartItem.getProductId(), cartItem.getProductName(), cartItem.getCustomerId(),
                        cartItem.getUnitPrice(), cartItem.getSellPrice(), cartItem.getQuantity(), cartItem.getOldQuantity(), cartItem.getTotalPrice());
                preList.add(newLineItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
