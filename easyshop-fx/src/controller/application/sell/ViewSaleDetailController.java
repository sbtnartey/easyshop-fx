/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.sell;

import entities.SaleItem;
import gateway.SaleItemGateway;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import list.ListSaleItem;

/**
 *
 * @author ICSGH-BILLY
 */
public class ViewSaleDetailController implements Initializable {

    public String saleId;
    public String receiptNo;
    SaleItem saleItem = new SaleItem();
    SaleItemGateway saleItemGateway = new SaleItemGateway();

    @FXML
    private TableView<ListSaleItem> tblSaleItemView;
    @FXML
    private TableColumn<Object, Object> clmProductName;
    @FXML
    private TableColumn<Object, Object> clmQuantity;
    @FXML
    private TableColumn<Object, Object> clmSellPrice;
    @FXML
    private TableColumn<Object, Object> clmTotalPrice;

    @FXML
    private Button btnClose;
    @FXML
    private Label lblSellId;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void viewSaleDetails(ActionEvent event) {
        try {

            lblSellId.setText(receiptNo);
            saleItem.saleId = saleId;
            tblSaleItemView.setItems(saleItem.saleItemDetails);
            clmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
            clmQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
            clmSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
            clmTotalPrice.setCellValueFactory(new PropertyValueFactory<>("amount"));
            saleItemGateway.viewSaleDetail(saleItem);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }
    
    

}
