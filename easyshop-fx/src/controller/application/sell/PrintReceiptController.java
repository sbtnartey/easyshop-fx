/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.sell;

import com.latlab.common.formating.ObjectValue;
import controller.application.report.ReportController;
import controller.application.settings.OrgSettingController;
import dao.SellCartBLL;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import details.SalesDetail;
import details.SalesReciept;
import entities.Sale;
import entities.SaleItem;
import gateway.SaleItemGateway;
import gateway.SellCartGetway;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import list.ListSaleItem;
import media.userNameMedia;

/**
 *
 * @author ICSGH-BILLY
 */
public class PrintReceiptController implements Initializable {

    public String receiptNo;
    private userNameMedia nameMedia;
    public String userId;
    public String usrName;
    public String saleId;
    public String shopName;
    public String shopPhoneNo;
    public String shopAddress;

    @FXML
    private TableView<ListSaleItem> tblViewSaleItem;

    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private TableColumn<Object, Object> tblClmSellPrice;
    @FXML
    private TableColumn<Object, Object> tblClmAmount;
    @FXML
    private TableColumn<Object, Object> tblClmQty;

    @FXML
    private TextField tfCashHanded;
    @FXML
    private Label lblBalance;
    @FXML
    public Label lblTotalCost;
    @FXML
    public Label lblSellId;
    @FXML
    private Button btnPrintReceipt;
    @FXML
    private Button btnClose;

    Sale sale = new Sale();
    SaleItem saleItem = new SaleItem();
    SaleItemGateway saleItemGateway = new SaleItemGateway();
    SalesReciept salesReciept = new SalesReciept();
    SellCartBLL scbll = new SellCartBLL();
    SellCartGetway sellCartGerway = new SellCartGetway();

    SQL sql = new SQL();
    DBConnection dbCon = new DBConnection();
    Connection con = dbCon.getConnection();
    DBProperties dBProperties = new DBProperties();
    PreparedStatement pst;
    ResultSet rs;
    String db = dBProperties.loadPropertiesFile();
    ReportController reportController;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            reportController = new ReportController();
            reportController.initialize(location, resources);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnPrintReceiptOnAction(ActionEvent event) {
        if (isNotNull()) {
            
            salesReciept.setCashHanded(Double.parseDouble(tfCashHanded.getText()));
            salesReciept.setSaleItemsList(SalesDetail.createSalesReceipt(saleItem.saleItemDetails));

            reportController.generateSalesReceipt(salesReciept);

            //close print dialog
            btnCloseOnAction(event);
            sale.amountPaid = String.valueOf(salesReciept.getCashHanded());
            sale.id = saleId;
            sellCartGerway.updateSalePayment(sale);
        }
    }

    private boolean isNotNull() {
        if (tfCashHanded.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("ERROR : ");
            alert.setContentText("Enter amount paid to print");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            return false;
        }
        return true;

    }

    @FXML
    private void tfCashHandedKeyEvent(KeyEvent keyEvent) {
        double balance = ObjectValue.get_doubleValue(tfCashHanded.getText()) - ObjectValue.get_doubleValue(lblTotalCost.getText());
        lblBalance.setText(ObjectValue.getStringValue(balance));
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    public void viewDetails() {
        double totalCost = 0.0;
        try {

            tblViewSaleItem.setItems(saleItem.saleItemDetails);

            tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
            tblClmQty.setCellValueFactory(new PropertyValueFactory<>("quantity"));
            tblClmSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
            tblClmAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));

            saleItem.saleId = saleId;
            salesReciept.setReceiptNumber(receiptNo);
            totalCost = Double.parseDouble(lblTotalCost.getText());
            salesReciept.setTotalCost(totalCost);
            salesReciept.setPrintedBy(sql.getName(userId, sale.createdBy, "User"));
            salesReciept.setAmountToPay(totalCost);
            saleItemGateway.viewSaleDetail(saleItem);

        } catch (Exception ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public userNameMedia getNameMedia() {

        return nameMedia;
    }

    public void setNameMedia(userNameMedia nameMedia) {
        this.nameMedia = nameMedia;
        userId = nameMedia.getId();
        usrName = nameMedia.getUsrName();
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public SalesReciept getSalesReciept() {
        return salesReciept;
    }

    public void setSalesReciept(SalesReciept salesReciept) {
        this.salesReciept = salesReciept;
    }

}
