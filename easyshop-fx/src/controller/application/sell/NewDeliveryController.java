/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.sell;

import dao.SellCartBLL;
import entities.CurrentProduct;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import gateway.CurrentProductGetway;
import gateway.CustomerGetway;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import media.userNameMedia;
import entities.Customer;
import entities.SellCart;
import entities.StockLevel;
import gateway.SellCartGetway;
import list.ListCustomer;
import list.ListPreSell;
import custom.CustomTf;
import custom.QuantityEditCell;
import custom.RandomIdGenarator;
import dao.SaleItemBLL;
import db.DBConnection;
import db.SQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Objects;
import javafx.util.Callback;

/**
 *
 * @author rifat
 */
public class NewDeliveryController implements Initializable {

    public Button btnAddCustomer;
    userNameMedia nameMedia;
    String userId;
    String productId;
    int oldQuantity;
    String customerId;
    int quantity;
    String totalItems;
    String savedSale;

    @FXML
    private MenuButton mbtnCustomer;
    @FXML
    private TableView<ListCustomer> tblCustomerSortView;
    @FXML
    private TableColumn<Object, Object> tblClmCustomerName;
    @FXML
    private TableColumn<Object, Object> tblClmCustomerPhoneNo;
    @FXML
    private TextField tfCustomerSearch;
    @FXML
    private TextField tfProductSearch;
    @FXML
    private Button btnClose;
    @FXML
    public TextField tfProductId;
    @FXML
    private TableView<ListPreSell> tblSellPreList;
    @FXML
    private TableColumn<Object, Object> tblClmProductName;
    @FXML
    private TableColumn<Object, Object> tblClmSellPrice;
    @FXML
    private TableColumn<Object, Object> tblClmQuantity;
    @FXML
    private TableColumn<Object, Object> tblClmTotalPrice;
    @FXML
    private Label lblTotal;
    @FXML
    private Label lblNetCost;
    @FXML
    private Button btnAddToChart;
    @FXML
    private Button btnSell;
    @FXML
    private Button btnClearAll;
    @FXML
    private Button btnClearSelected;
    @FXML
    private Label lblSellId;
    @FXML
    private TableView<ListPreSell> tblViewSellCart;
    @FXML
    private TableColumn<Object, String> tblClmCartQty;
    @FXML
    private TableColumn<Object, Object> tblClmCartPrice;
    @FXML
    private TableColumn<Object, Object> tblClmCartQtyAvailable;
    @FXML
    private TableColumn<Object, Object> tblClmCartProductName;

    Callback<TableColumn<Object, String>, TableCell<Object, String>> cellFactory
            = (TableColumn<Object, String> param) -> new QuantityEditCell();

    Customer customer = new Customer();
    CurrentProduct product = new CurrentProduct();
    CustomerGetway customerGetway = new CustomerGetway();
    StockLevel currrentProduct = new StockLevel();
    CurrentProductGetway currentProductGetway = new CurrentProductGetway();
    SellCart sellCart = new SellCart();
    SellCartGetway sellCartGerway = new SellCartGetway();
    SellCartBLL scbll = new SellCartBLL();
    SaleItemBLL saleItemBLL = new SaleItemBLL();
    CustomTf ctf = new CustomTf();

    SQL sql = new SQL();
    DBConnection db = new DBConnection();
    Connection con = db.getConnection();
    PreparedStatement pst;
    ResultSet rs;
    ListPreSell listPreSell = new ListPreSell();
    ObservableList<ListPreSell> preList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearAll();
    }

    @FXML
    private void tblCustomerOnClick(MouseEvent event) {
        mbtnCustomer.setText(tblCustomerSortView.getSelectionModel().getSelectedItem().getCustomerName());
        customerId = tblCustomerSortView.getSelectionModel().getSelectedItem().getId();
        sellCart.customerId = customerId;
    }

    @FXML
    private void mbtnCustomerOnClicked(MouseEvent event) {
        customer.customerName = tfCustomerSearch.getText().trim();
        tblCustomerSortView.setItems(customer.customerList);
        tblClmCustomerName.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        tblClmCustomerPhoneNo.setCellValueFactory(new PropertyValueFactory<>("customerContNo"));
        customerGetway.searchView(customer);
    }

    @FXML
    private void tblProductOnClick(MouseEvent event) {
        productId = tblViewSellCart.getSelectionModel().getSelectedItem().getProductId();
        sellCart.oldQuantity = tblViewSellCart.getSelectionModel().getSelectedItem().getOldQuantity();
        currrentProduct.productId = productId;
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void tfCustomerSearchOnKeyReleased(KeyEvent event) {
        customer.customerName = tfCustomerSearch.getText().trim();
        customer.customerConNo = tfCustomerSearch.getText().trim();
        tblCustomerSortView.setItems(customer.customerList);
        tblClmCustomerName.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        tblClmCustomerPhoneNo.setCellValueFactory(new PropertyValueFactory<>("customerContNo"));
        customerGetway.searchView(customer);
    }

    @FXML
    private void tfProductSearchOnKeyReleased(KeyEvent event) {
        sellCart.carts.clear();

        sellCart.productName = tfProductSearch.getText().trim();
        tblViewSellCart.setItems(sellCart.carts);
        tblViewSellCart.setEditable(true);

        tblClmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmCartPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
        tblClmCartQtyAvailable.setCellValueFactory(new PropertyValueFactory<>("oldQuantity"));

        tblClmCartQty.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tblClmCartQty.setCellFactory(cellFactory);
        tblClmCartQty.setOnEditCommit(
                (TableColumn.CellEditEvent<Object, String> t)
                -> {
            ((ListPreSell) t.getTableView().getItems()
                    .get(t.getTablePosition().getRow()))
                    .setQuantity(t.getNewValue());

            quantity = Integer.parseInt(t.getNewValue());
        });

        sellCartGerway.searchProductForCart(sellCart);
    }

    @FXML
    private void btnAddToCartOnAction(ActionEvent event) {
        if (inNotNull()) {

            currrentProduct.productName = sql.getName(currrentProduct.productId, currrentProduct.productName, "Products");
            sellCart.quantity = String.valueOf(quantity);
            sellCart.sellPrice = tblViewSellCart.getSelectionModel().getSelectedItem().getSellPrice();
            double netCost = Double.valueOf(sellCart.sellPrice) * Integer.valueOf(sellCart.quantity);
            String itemCost = String.valueOf(netCost);

            listPreSell.setId(currrentProduct.id);
            listPreSell.setProductId(currrentProduct.productId);
            listPreSell.setProductName(currrentProduct.productName);
            listPreSell.setCustomerId(customerId);
            listPreSell.setUnitPrice(currrentProduct.purchasePrice);
            listPreSell.setSellPrice(sellCart.sellPrice);
            listPreSell.setQuantity(sellCart.quantity);
            listPreSell.setOldQuantity(sellCart.oldQuantity);
            listPreSell.setTotalPrice(itemCost);

            addItem(listPreSell);
            viewAll();
            sumTotalCost();
        }

    }

    private void sumTotalCost() {
        tblSellPreList.getSelectionModel().selectFirst();
        float sum = 0;
        int items = tblSellPreList.getItems().size();

        int itemsInCart = 0;

        for (int i = 0; i < items; i++) {
            tblSellPreList.getSelectionModel().select(i);
            String selectedItem = tblSellPreList.getSelectionModel().getSelectedItem().getTotalPrice();
            int selectedQty = Integer.parseInt(tblSellPreList.getSelectionModel().getSelectedItem().getQuantity());
            float newFloat = Float.parseFloat(selectedItem);
            System.out.println("total price " + newFloat);
            sum += newFloat;
            itemsInCart += selectedQty;
        }
        String totalCost = String.valueOf(sum);
        lblTotal.setText(totalCost);
        System.out.println("Total:" + sum);
        totalItems = String.valueOf(itemsInCart);
    }

    public void viewAll() {
        tblSellPreList.setItems(preList);
        tblClmCartProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
        tblClmQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tblClmSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
        tblClmTotalPrice.setCellValueFactory(new PropertyValueFactory<>("totalPrice"));
    }

    @FXML
    private void btnSellOnAction(ActionEvent event) {
        if (!tblSellPreList.getItems().isEmpty()) {
            sellCart.sellId = lblSellId.getText();
            sellCart.totalItems = totalItems;
            sellCart.totalCost = lblTotal.getText();
            sellCart.customerId = customerId;
            //create sale 
            savedSale = scbll.sell(sellCart);
            sellCart.Id = savedSale;
            System.out.println("saved sale id " + savedSale);
            int indexs = tblSellPreList.getItems().size();
            for (int i = 0; i < indexs; i++) {
                tblSellPreList.getSelectionModel().select(i);
//                sellCart.Id = tblSellPreList.getSelectionModel().getSelectedItem().getId();
                sellCart.productId = tblSellPreList.getSelectionModel().getSelectedItem().getProductId();
                sellCart.sellPrice = tblSellPreList.getSelectionModel().getSelectedItem().getSellPrice();
                sellCart.quantity = tblSellPreList.getSelectionModel().getSelectedItem().getQuantity();
                sellCart.oldQuantity = tblSellPreList.getSelectionModel().getSelectedItem().getOldQuantity();

                sellCart.totalPrice = tblSellPreList.getSelectionModel().getSelectedItem().getTotalPrice();
//                sellCart.warrentyVoidDate = tblSellPreList.getSelectionModel().getSelectedItem().getWarrentyVoidDate();
                //sellCart.sellerID = userId;

                if (null != savedSale || "" == savedSale) {
                    //save each sale item
                    saleItemBLL.save(sellCart);
                }
            }

            tblSellPreList.getItems().clear();
            btnCloseOnAction(event);
            toPrintReceipt(event);
            lblTotal.setText(null);
        } else {
            System.out.println("EMPTY");
        }

    }

    @FXML
    private void toPrintReceipt(ActionEvent event) {
        PrintReceiptController prc = new PrintReceiptController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fXMLLoader = new FXMLLoader();
        fXMLLoader.setLocation(getClass().getResource("/view/application/sell/PrintReceipt.fxml"));
        try {
            fXMLLoader.load();
            Parent parent = fXMLLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            PrintReceiptController printReceiptController = fXMLLoader.getController();
            media.setId(userId);
            printReceiptController.setNameMedia(media);
            printReceiptController.receiptNo = lblSellId.getText();
            printReceiptController.lblSellId.setText(lblSellId.getText());
            printReceiptController.saleId = sellCart.Id;
            printReceiptController.lblTotalCost.setText(lblTotal.getText());

            printReceiptController.viewDetails();

            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(ViewCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void clearAll() {

    }

    @FXML
    public void btnAddCustomerOnAction(ActionEvent actionEvent) {
        System.out.println(userId);
        AddCustomerController acc = new AddCustomerController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fXMLLoader = new FXMLLoader();
        fXMLLoader.setLocation(getClass().getResource("/view/application/sell/AddCustomer.fxml"));
        try {
            fXMLLoader.load();
            Parent parent = fXMLLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AddCustomerController addCustomerController = fXMLLoader.getController();
            media.setId(userId);
            addCustomerController.setNameMedia(nameMedia);
            addCustomerController.lblCustomerContent.setText("ADD CUSTOMER");
            addCustomerController.btnUpdate.setVisible(false);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(ViewCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnClearAllOnAction(ActionEvent event) {

    }

    @FXML
    private void btnClearSelectedOnAction(ActionEvent event) {
        if (!tblSellPreList.getItems().isEmpty()) {
            tblSellPreList.getItems().removeAll(tblSellPreList.getSelectionModel().getSelectedItems());
            sumTotalCost();
        } else {
            System.out.println("EMPTY");
        }

    }

    public void genarateSellID() {
        String id = RandomIdGenarator.randomstring();
        if (id.matches("001215")) {
            String nId = RandomIdGenarator.randomstring();
            lblSellId.setText(nId);
        } else {
            lblSellId.setText(id);
        }

    }

    public boolean inNotNull() {
        boolean isNotNull = false;
        if (tblViewSellCart.getSelectionModel().getSelectedItem().getQuantity() != null || customerId != null) {
            System.out.println("table item is selected and quantity is " + quantity);
            isNotNull = true;

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("ERROR");
            alert.setContentText("Please fill all required field");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        }

        System.out.println("is not null " + isNotNull);
        return isNotNull;
    }

    public void setNameMedia(userNameMedia nameMedia) {
        userId = nameMedia.getId();
        sellCart.sellerID = userId;
        this.nameMedia = nameMedia;
    }

    private synchronized void addItem(ListPreSell cartItem) {
        boolean newItem = true;
        try {

            int index = 0;
            for (ListPreSell item : preList) {
                if (Objects.equals(item.getProductId(), cartItem.getProductId())) {
                    newItem = false;
                    int oldQty = Integer.parseInt(item.getQuantity());
                    int newQty = Integer.parseInt(cartItem.getQuantity());
                    int sumQty = oldQty + newQty;
                    item.setQuantity(String.valueOf(sumQty));

                    preList.set(index, item);
                }
                index++;
            }
            if (newItem) {
                ListPreSell newLineItem = new ListPreSell(cartItem.getId(), cartItem.getProductId(), cartItem.getProductName(), cartItem.getCustomerId(),
                        cartItem.getUnitPrice(), cartItem.getSellPrice(), cartItem.getQuantity(), cartItem.getOldQuantity(), cartItem.getTotalPrice());
                preList.add(newLineItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
