/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.sell;

import entities.Sale;
import entities.SaleItem;
import entities.SellCart;
import gateway.SaleItemGateway;
import gateway.SellCartGetway;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import list.ListSale;
import list.ListSaleItem;
import media.userNameMedia;

public class ViewSellController implements Initializable {

    userNameMedia nameMedia;

    SellCart sellCart = new SellCart();
    Sale sale = new Sale();
    SellCartGetway sellCartGerway = new SellCartGetway();
    SaleItem saleItem = new SaleItem();
    SaleItemGateway saleItemGateway = new SaleItemGateway();

    String userId;
    @FXML
    private Button btnSellOrder;
    @FXML
    private TableView<ListSale> tblSellView;
    @FXML
    private TableColumn<Object, Object> tblClmSellId;
    @FXML
    private TableColumn<Object, Object> tblClmSoldDate;
    @FXML
    private TableColumn<Object, Object> tblClmQuantity;
    @FXML
    private TableColumn<Object, Object> tblClmTotalAmount;
    @FXML
    private TableColumn<Object, Object> tblClmReceiptNo;
    @FXML
    private TableColumn<Object, Object> tblClmSoldBy;
    @FXML
    public TextField tfSearch;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button btnViewDetails;

    public void setNameMedia(userNameMedia nameMedia) {
        userId = nameMedia.getId();
        this.nameMedia = nameMedia;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void btnSellOrderOnAction(ActionEvent event) {
        System.out.println(userId);
        NewSellController acc = new NewSellController();
        userNameMedia media = new userNameMedia();
        FXMLLoader fXMLLoader = new FXMLLoader();
        fXMLLoader.setLocation(getClass().getResource("/view/application/sell/NewSell.fxml"));
        try {
            fXMLLoader.load();
            Parent parent = fXMLLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            NewSellController newSellController = fXMLLoader.getController();
            media.setId(userId);
            newSellController.setNameMedia(nameMedia);
            newSellController.genarateSellID();
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(ViewCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void viewDetails() {
        tblSellView.setItems(sale.saleDetails);

        tblClmSoldDate.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        tblClmQuantity.setCellValueFactory(new PropertyValueFactory<>("totalItems"));
        tblClmTotalAmount.setCellValueFactory(new PropertyValueFactory<>("totalAmount"));
        tblClmReceiptNo.setCellValueFactory(new PropertyValueFactory<>("receiptNo"));
        tblClmSoldBy.setCellValueFactory(new PropertyValueFactory<>("sellerName"));
        sellCartGerway.view(sale);
    }

    @FXML
    private void btnViewSaleDetailsOnAction(ActionEvent actionEvent) {
        if (tblSellView.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("ERROR : ");
            alert.setContentText("Select a sale to view sale items");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        } else if (!tblSellView.getSelectionModel().isEmpty()) {
            try {
                String saleId = tblSellView.getSelectionModel().getSelectedItem().getId();
                String receiptNo = tblSellView.getSelectionModel().getSelectedItem().getReceiptNo();

                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/application/sell/ViewSaleDetail.fxml"));

                if (saleId != null) {

                    fxmlLoader.load();
                    Parent parent = fxmlLoader.getRoot();
                    Scene scene = new Scene(parent);
                    scene.setFill(new Color(0, 0, 0, 0));

                    ViewSaleDetailController viewSaleDetailController = fxmlLoader.getController();
                    viewSaleDetailController.saleId = saleId;
                    viewSaleDetailController.receiptNo = receiptNo;
                    System.out.println("selected sale id " + saleId);
                    viewSaleDetailController.viewSaleDetails(actionEvent);

                    Stage nStage = new Stage();
                    nStage.setScene(scene);
                    nStage.initModality(Modality.APPLICATION_MODAL);
                    nStage.initStyle(StageStyle.TRANSPARENT);
                    nStage.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @FXML
    private void btnReturnedSaleDetailOnAction(ActionEvent actionEvent) {
        if (tblSellView.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("ERROR : ");
            alert.setContentText("Select a sale to view sale items");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        } else if (!tblSellView.getSelectionModel().isEmpty()) {
            try {
                String saleId = tblSellView.getSelectionModel().getSelectedItem().getId();
                String receiptNo = tblSellView.getSelectionModel().getSelectedItem().getReceiptNo();

                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/application/sell/ProcessReturnSales.fxml"));

                if (saleId != null) {

                    fxmlLoader.load();
                    Parent parent = fxmlLoader.getRoot();
                    Scene scene = new Scene(parent);
                    scene.setFill(new Color(0, 0, 0, 0));

                    ProcessReturnedSalesController viewSaleDetailController = fxmlLoader.getController();
                    viewSaleDetailController.saleId = saleId;
                    viewSaleDetailController.receiptNo = receiptNo;
                    viewSaleDetailController.viewSaleDetails(actionEvent);

                    Stage nStage = new Stage();
                    nStage.setScene(scene);
                    nStage.initModality(Modality.APPLICATION_MODAL);
                    nStage.initStyle(StageStyle.TRANSPARENT);
                    nStage.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @FXML
    public void tfSearchOnKeyReleased(KeyEvent event) {
        tblSellView.getItems().clear();
        sale.id = tfSearch.getText();
        sale.receiptNo = tfSearch.getText();
        sellCartGerway.searchView(sale);
    }

    @FXML
    private void btnRefreshOnAction(ActionEvent event) {
        tblSellView.getItems().clear();
        tfSearch.clear();
        tblSellView.setItems(sale.saleDetails);

        tblClmSoldDate.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        tblClmQuantity.setCellValueFactory(new PropertyValueFactory<>("totalItems"));
        tblClmTotalAmount.setCellValueFactory(new PropertyValueFactory<>("totalAmount"));
        tblClmReceiptNo.setCellValueFactory(new PropertyValueFactory<>("receiptNo"));
        tblClmSoldBy.setCellValueFactory(new PropertyValueFactory<>("sellerName"));
        sellCartGerway.view(sale);
    }

}
