/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.application.sell;

import entities.SaleItem;
import gateway.SaleItemGateway;
import gateway.SalesGateway;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import list.ListSaleItem;
import media.userNameMedia;

public class ProcessReturnedSalesController implements Initializable {

    public String saleId;
    public String receiptNo;
    userNameMedia nameMedia;
    SaleItem saleItem = new SaleItem();
    SaleItemGateway saleItemGateway = new SaleItemGateway();
    SalesGateway salesGateway = new SalesGateway();

    String userId;
    @FXML
    private TableView<ListSaleItem> tblSaleItemView;
    @FXML
    private TableColumn<Object, Object> clmProductName;
    @FXML
    private TableColumn<Object, Object> clmQuantity;
    @FXML
    private TableColumn<Object, Object> clmSellPrice;
    @FXML
    private TableColumn<Object, Object> clmTotalPrice;

    @FXML
    private Button btnClose;
    @FXML
    private Label lblSellId;

    public void setNameMedia(userNameMedia nameMedia) {
        userId = nameMedia.getId();
        this.nameMedia = nameMedia;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void viewSaleDetails(ActionEvent event) {
        try {

            lblSellId.setText(receiptNo);
            saleItem.saleId = saleId;
            tblSaleItemView.setItems(saleItem.saleItemDetails);
            clmProductName.setCellValueFactory(new PropertyValueFactory<>("productName"));
            clmQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
            clmSellPrice.setCellValueFactory(new PropertyValueFactory<>("sellPrice"));
            clmTotalPrice.setCellValueFactory(new PropertyValueFactory<>("amount"));
            saleItemGateway.viewSaleDetail(saleItem);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void btnReturnAllOnAction(ActionEvent event) {
        int count = 0;
        try {
            if (!tblSaleItemView.getItems().isEmpty()) {
                //set receipt as returned

                //loop over items in list and updated stock levels
                int listSize = tblSaleItemView.getItems().size();
                for (int i = 0; i < listSize; i++) {
                    tblSaleItemView.getSelectionModel().select(i);

                    //fetch productId and quantity
                    saleItem.productId = tblSaleItemView.getSelectionModel().getSelectedItem().getProductId();
                    saleItem.quantity = tblSaleItemView.getSelectionModel().getSelectedItem().getQuantity();
                    saleItem.amount = tblSaleItemView.getSelectionModel().getSelectedItem().getAmount();
                    saleItem.saleId = saleId;

                    int isUpdated = saleItemGateway.returnSaleItem(saleItem);
                    if (isUpdated == 1) {
                        salesGateway.updateReturnedSale(saleItem);
                        saleItemGateway.updateReturnedGoods(saleItem);
                        count++;
                    }

                }

                if (count == listSize) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Success");
                    alert.setContentText("Return process successful and stock levels updated");
                    alert.initStyle(StageStyle.UNDECORATED);
                    alert.showAndWait();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnReturnSelected(ActionEvent event) {
        try {

            if (tblSaleItemView.getSelectionModel().getSelectedItem() == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("ERROR");
                alert.setContentText("Select an item to remove");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
            } else {
                saleItem.productId = tblSaleItemView.getSelectionModel().getSelectedItem().getProductId();
                saleItem.quantity = tblSaleItemView.getSelectionModel().getSelectedItem().getQuantity();

                int isUpdated = saleItemGateway.returnSaleItem(saleItem);
                if (isUpdated == 1) {
                    salesGateway.updateReturnedSale(saleItem);
                    saleItemGateway.updateReturnedGoods(saleItem);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Success");
                    alert.setContentText("Return process successful and stock levels updated");
                    alert.initStyle(StageStyle.UNDECORATED);
                    alert.showAndWait();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }
}
