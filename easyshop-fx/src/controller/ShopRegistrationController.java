/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.latlab.common.utils.DateTimeUtils;
import controller.application.settings.OrgSettingController;
import db.DBConnection;
import db.DBProperties;
import db.SQL;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;

/**
 *
 * @author seth
 */
public class ShopRegistrationController implements Initializable {

    //shop registration 
    @FXML
    private TextField tfWebSite;
    @FXML
    private TextArea taContactNumber;
    @FXML
    private TextArea taAdddress;
    @FXML
    private TextField tfVat;
    @FXML
    private TextField tfOrganizeName;
    @FXML
    private Rectangle retOrgLogo;
    @FXML
    private Button btnAttechLogo;
    @FXML
    private Button btnSaveOrganize;
    private File file;
    private BufferedImage bufferedImage;
    private Image image;
    private String userId;
    private String imagePath;

    private PreparedStatement pst;
    private Connection con;
    private ResultSet rs;
    DBConnection dbCon = new DBConnection();

    DBProperties dBProperties = new DBProperties();
    String db = dBProperties.loadPropertiesFile();

    @FXML
    private Hyperlink hlLogin;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    //<editor-fold defaultstate="collapsed" desc="Shop Registration Logic">
    @FXML
    private void hlLogin(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/Login.fxml"));
        Scene scene = new Scene(root);
        Stage nStage = new Stage();
        nStage.setScene(scene);
        nStage.setMaximized(true);
        nStage.setTitle("Welcome to EasyShop -Login");
        nStage.getIcons().add(new Image("/image/easyshop-login.png"));
        nStage.show();
    }

    @FXML
    private void btnSaveOrganizeOnClick(ActionEvent event) {
        if (isFoundData()) {
            //update
            if (imagePath != null) {
                updateOrganizeWithImage();
            } else {
                updateOrganizeWithOutImage();
            }

        } else {
            //insert
            insertOrganizeWithImage(event);

        }
    }

    @FXML
    private void btnAttechLogoOnAction(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG (Joint Photographic Group)", "*.jpg"),
                new FileChooser.ExtensionFilter("JPEG (Joint Photographic Experts Group)", "*.jpeg"),
                new FileChooser.ExtensionFilter("PNG (Portable Network Graphics)", "*.png")
        );

        fileChooser.setTitle("Choise a Image File");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            System.out.println(file);
            bufferedImage = ImageIO.read(file);
            image = SwingFXUtils.toFXImage(bufferedImage, null);
            retOrgLogo.setFill(new ImagePattern(image));
            imagePath = file.getAbsolutePath();
        }
    }

    /*
    
     */
    public void showDetails() {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("select * from " + db + ".Shop where id=?");
            pst.setString(1, "1");
            rs = pst.executeQuery();
            while (rs.next()) {
                tfOrganizeName.setText(rs.getString(2));
                tfWebSite.setText(rs.getString(3));
                taContactNumber.setText(rs.getString(4));
                taAdddress.setText(rs.getString(5));

                Blob blob = rs.getBlob(6);
                if (blob != null) {
                    ByteArrayInputStream in = new ByteArrayInputStream(blob.getBytes(1, (int) blob.length()));
                    image = new Image(in);
                    retOrgLogo.setFill(new ImagePattern(image));
                } else {

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    
     */
    private boolean isFoundData() {
        boolean dataFound = true;
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("select * from " + db + ".Shop ORDER BY id ASC LIMIT 1");
            rs = pst.executeQuery();
            while (rs.next()) {
                System.out.println("Data Found");
                return dataFound;
            }
            System.out.println("Data not found");
            dataFound = false;

        } catch (SQLException ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dataFound;
    }

    /*
    
     */
    private void updateOrganizeWithImage() {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("Update " + db + ".Shop set shop_name=?,shop_web_address=?,shop_contact_numbers=?,shop_address=?,shop_logo=?,vat=? where id=1");

            pst.setString(1, tfOrganizeName.getText());
            pst.setString(2, tfWebSite.getText());
            pst.setString(3, taContactNumber.getText());
            pst.setString(4, taAdddress.getText());
            if (imagePath != null) {
                try {
                    InputStream is = new FileInputStream(new File(imagePath));
                    pst.setBlob(5, is);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                pst.setBlob(5, (Blob) null);
            }
            pst.setDouble(6, Double.parseDouble(tfVat.getText()));
            pst.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Update");
            alert.setHeaderText("Update ");
            alert.setContentText("Update Sucessfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
        } catch (SQLException ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void insertOrganizeWithImage(ActionEvent event) {
        SQL sql = new SQL();
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("insert into " + db + ".Shop(id,shop_name,shop_web_address,shop_contact_numbers,shop_contact_address,shop_logo,user_id,start_date,end_date) values(?,?,?,?,?,?,?,?,?)");
            pst.setString(1, "1");
            pst.setString(2, tfOrganizeName.getText());
            pst.setString(3, tfWebSite.getText());
            pst.setString(4, taContactNumber.getText());
            pst.setString(5, taAdddress.getText());
            if (imagePath != null) {
                try {
                    InputStream is = new FileInputStream(new File(imagePath));
                    pst.setBlob(6, is);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                pst.setBlob(6, (Blob) null);
            }
            pst.setString(7, userId);
            Date startDate = new Date();
            Date endDate = DateTimeUtils.increaseDate(startDate, 30);
            pst.setDate(8, sql.getDate(DateTimeUtils.formatDate(startDate, "yyyy-MM-dd")));
            pst.setDate(9, sql.getDate(DateTimeUtils.formatDate(endDate, "yyyy-MM-dd")));
            pst.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Login");
            alert.setHeaderText("Sucess");
            alert.setContentText("Shop information added successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                try {
                    Stage hlLoginStage = (Stage) btnSaveOrganize.getScene().getWindow();
                    hlLoginStage.close();
                    hlLogin(event);
                } catch (IOException ex) {
                    Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    
     */
    private void updateOrganizeWithOutImage() {
        con = dbCon.getConnection();
        try {
            pst = con.prepareStatement("Update " + db + ".Shop set shop_name=?,shop_web_address=?,shop_contact_numbers=?,shop_contact_address=? where id=1");

            pst.setString(1, tfOrganizeName.getText());
            pst.setString(2, tfWebSite.getText());
            pst.setString(3, taContactNumber.getText());
            pst.setString(4, taAdddress.getText());

            pst.executeUpdate();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Update");
            alert.setHeaderText("Sucess ");
            alert.setContentText("Update sucessfuly");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

        } catch (SQLException ex) {
            Logger.getLogger(OrgSettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//</editor-fold>

}
